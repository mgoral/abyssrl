# Abyssal Plain

Abyssal Plain is a roguelike game.

## System requirements

* OS: GNU/Linux (probably should work on Windows and Mac OS)
* Computer with a graphical mode
* Minimal screen resolution: 800x600

## Dependencies

* Python (supported versions: 3.5, 3.6)
* libsdl2-dev
* libffi-dev
* pip3

## Development notes

### Palette

This game uses DB32 color palette (see:
http://pixeljoint.com/forum/forum_posts.asp?TID=16247). Color definitions for
Gimp are kept in `other/db32-gimp-palette`.
