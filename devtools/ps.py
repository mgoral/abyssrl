#!/usr/bin/env python3

import pstats

ps = pstats.Stats('prof')
ps.sort_stats('cumtime')
ps.print_stats(25)
