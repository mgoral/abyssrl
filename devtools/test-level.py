#!/usr/bin/env python3

import abyssrl.fs.jsonloader as jsonloader
from abyssrl.level.factory import Factory, LevelType
from abyssrl.level.img import save, save_partial_layout, partial_finished

FILE = 'map.png'

f = Factory()
f._depths = {'1': ['caves']}

partial_finished.connect(save_partial_layout)
generated = f(1,
              width=140,
              height=80)
save(FILE, generated, f.bases)
