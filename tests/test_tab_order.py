from abyssrl.gui.boxes import *
from abyssrl.gui.widgets import *

def test_basic_tab_order():
    '''Checks tab order after adding several widgets'''
    top = VBox()
    assert top.tab_order == []

    top.add(Button('1'))
    top.add(Button('2'))
    top.add(Button('3'))

    assert top.tab_order == top.children
    for ch in top.children:
        assert ch.tab_order is top.tab_order


def test_tab_order_focusable():
    '''Adds non-focusable widget, which should be excluded from tab order. After
    changing its focusable property, it should be added to tab_order and not
    removed from it after changing `focusable` to false again.'''
    sep = Separator('sep', focusable=False)

    top = VBox()
    top.add(Button('1'))
    top.add(sep)
    assert sep  not in top.tab_order

    sep.focusable = True
    assert sep == top.tab_order[-1]

    sep.focusable = False
    assert sep == top.tab_order[-1]


def test_remove_and_add_widgets():
    '''Checks tab order of individual widgets after removing a subtree and
    adding it back.'''
    top = VBox()
    btn = Button('checked')

    top.add(Button('1'))
    top.add(btn)  # added somewhere in the middle
    top.add(Button('3'))
    assert top.tab_order == [top.children[0], btn, top.children[2]]

    top.remove(btn)
    assert btn not in top.tab_order
    assert btn in btn.tab_order

    top.add(btn)
    assert top.tab_order == [top.children[0], top.children[1], btn]
