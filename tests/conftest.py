import pytest

import abyssrl.screen as screen
import abyssrl.ecs.entity as entity
from abyssrl.gui._widget import Widget


@pytest.fixture(autouse=True)
def fake_screen(monkeypatch):
    def _font_spacing_mp(name=None):
        if name == 'tileset':
            return (2, 1)
        return (1, 1)

    def _window_size_mp():
        return 200, 200

    monkeypatch.setattr(screen, 'font_spacing', _font_spacing_mp)
    monkeypatch.setattr(screen, 'window_size', _window_size_mp)


@pytest.fixture
def test_entity():
    def _make(cmp_dct):
        dct = {
            'otid': 'test_entity',
            'components': cmp_dct
        }
        return entity.Entity.FromJson(dct)
    return _make
