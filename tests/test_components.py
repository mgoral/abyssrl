from unittest.mock import Mock

from abyssrl.ecs.components import *
from abyssrl.ecs.stats import Stats
from abyssrl.mechanics import Dice

def test_position():
    '''Check Position() attributes setting and validation'''
    pos = Position(2, 5)

    assert pos + pos == Position(4, 10)
    assert pos - pos == Position(0, 0)


def test_stats_encounter_level():
    s = Stats()
    assert s.encounter_level == 1

    s.dices = [Dice.S('1d6'), Dice.S('1d6')]
    assert s.encounter_level == 2
