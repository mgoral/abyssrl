from abyssrl.mechanics import Dice


def assert_rolls(d, minroll, maxroll):
    assert d.minroll == minroll
    assert d.maxroll == maxroll


def test_add():
    assert_rolls(Dice(1, 10) + Dice(1, 10), 2, 20)
    assert_rolls(Dice(1, 8) + Dice(1, 3), 2, 11)
    assert_rolls(Dice(3, 6) + Dice(1, 3), 4, 21)

    assert_rolls(Dice(1, 10) + 5, 6, 15)
    assert_rolls(3 + Dice(1, 10), 4, 13)
