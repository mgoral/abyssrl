import pytest
import attr

import abyssrl.api as api


@pytest.fixture
def node(test_entity):
    return test_entity({'stats': {'hp': 100, 'max_hp': 100}})


def test_regenerate(node):
    # basic usage: regenerate a given amount
    node.stats.hp = 10
    assert api.regenerate(node, 'hp', 2) == 2
    assert node.stats.hp == 12

    # basic usage: regenerate to the max value of hp
    assert api.regenerate(node, 'hp') == (100 - 12)
    assert node.stats.hp == 100

    # ask for regeneration of amount higher than max value of hp.
    # regenerate() should regenerate only to the max_hp
    node.stats.hp -= 10
    assert api.regenerate(node, 'hp', 20) == 10
    assert node.stats.hp == 100

    # regenerate when stat == max_hp
    assert api.regenerate(node, 'hp', 20) == 0
    assert node.stats.hp == 100

    # regenerate the last point to the max_hp (a common case for HP and O2
    # regen)
    node.stats.hp -= 1
    assert api.regenerate(node, 'hp', 1) == 1
    assert node.stats.hp == 100

    # don't change anything when hp > max_hp (e.g. due to equipment change)
    node.stats.max_hp = 50
    assert api.regenerate(node, 'hp', 20) == 0
    assert node.stats.hp == 100
    assert api.regenerate(node, 'hp') == 0
    assert node.stats.hp == 100
