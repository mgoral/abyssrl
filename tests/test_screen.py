import pytest

import abyssrl.screen as screen
import abyssrl.utils as utils

@pytest.fixture
def fake_terminal():
    screen.start()
    screen.refresh()
    yield
    screen.stop()

def test_layer_changing(fake_terminal):
    assert screen.current_layer() == 0
    with screen.layer(1):
        assert screen.current_layer() == 1

        with screen.next_layer():
            assert screen.current_layer() == 2

        assert screen.current_layer() == 1
    assert screen.current_layer() == 0

def test_layer_group(fake_terminal):
    assert screen.min_layer_in_group() == 0
    assert screen.max_layer_in_group() > screen.min_layer_in_group()

    # checks whether setting these layers won't raise
    with screen.layer(screen.min_layer_in_group()):
        assert screen.current_layer() == screen.min_layer_in_group()
    with screen.layer(screen.max_layer_in_group()):
        assert screen.current_layer() == screen.max_layer_in_group()

    with screen.next_layer_group():
        assert screen.current_layer() == screen.min_layer_in_group()

        assert screen.min_layer_in_group() > 0
        assert screen.max_layer_in_group() > screen.min_layer_in_group()

        # checks whether setting these layers won't raise
        with screen.layer(screen.min_layer_in_group()):
            assert screen.current_layer() == screen.min_layer_in_group()
        with screen.layer(screen.max_layer_in_group()):
            assert screen.current_layer() == screen.max_layer_in_group()

        with pytest.raises(utils.GameError):
            with screen.layer(screen.min_layer_in_group() - 1): pass

        with pytest.raises(utils.GameError):
            with screen.layer(screen.max_layer_in_group() + 1):pass

    assert screen.min_layer_in_group() == 0


def test_assert_layer_width():
    # test of a hardcode...
    assert screen.layer_width() >= 2
