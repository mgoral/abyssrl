import os
import subprocess

from setuptools import setup, find_packages, Command
import distutils.command.build as distutils_build
from distutils import log as dist_log

basepath = os.path.dirname(__file__)


class NoOptsCmd(Command):
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass

class BuildJsonStrings(NoOptsCmd):
    """Generates translatable strings from json files"""
    user_options = []

    def run(self):
        script = os.path.join(basepath, 'tools', 'gen-strings')
        subprocess.run(script)


class AbyssBuild(distutils_build.build):
    """Overrides a default install_data"""
    user_options = []

    def run(self):
        self.run_command('build_jsonstrings')
        self.run_command('compile_catalog')
        super().run()
        self.clean()

    def clean(self):
        dist_log.info('running build cleanup')
        # establish a path to build/lib, where build artifacts (before creating
        # wheels or bdist) are copied. We'll use distutils_build.build.build_lib
        # property for that.
        build_dir = os.path.join(basepath, self.build_lib)

        # file names relative to build/lib/abyssrl directory
        to_clean = [
            # e.g. 'abyssrl/somefile.txt'
        ]

        for expr in to_clean:
            for file_ in glob.glob(os.path.join(build_dir, expr)):
                dist_log.info('removing %s' % file_)
                os.unlink(file_)


def main():
    install_reqs = ['attrs==17.4.0',
                    'mgcomm==0.2.0',
                    'tdl==4.1.1',
                    'bearlibterminal==0.15.7',
                    'numpy',
                    'pytmx']

    setup(name='abyssrl',
          description='Abyssal Plain is a roguelike game',
          use_scm_version={'write_to': '.version.txt'},
          license='GPLv3+',
          author='Michał Góral',
          author_email='dev@mgoral.org',
          url='https://gitlab.com/mgoral/abyssrl',
          platforms=['linux'],
          setup_requires=['setuptools_scm', 'babel'],
          install_requires=install_reqs,

          # https://pypi.python.org/pypi?%3Aaction=list_classifiers
          classifiers=['Development Status :: 1 - Planning',
                       'Environment :: X11 Applications',
                       'Intended Audience :: End Users/Desktop',
                       'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
                       'Natural Language :: English',
                       'Operating System :: POSIX',
                       'Programming Language :: Python :: 3 :: Only',
                       'Programming Language :: Python :: 3.5',
                       'Programming Language :: Python :: 3.6',
                       'Topic :: Games/Entertainment',
                       'Topic :: Games/Entertainment :: Role-Playing',
                       ],

          packages=find_packages('src'),
          package_dir={'': 'src'},

          # package_data is bdist specific
          package_data={'': ['assets/**/*',
                             'locale/*/LC_MESSAGES/*.mo']},

          entry_points={
              'console_scripts': ['abyssrl=abyssrl.main:main'],
          },

          cmdclass={
              'build': AbyssBuild,
              'build_jsonstrings': BuildJsonStrings,
          }
    )

if __name__ == '__main__':
    main()

