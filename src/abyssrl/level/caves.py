# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import tcod.map
import tcod.path
import random

import abyssrl.mechanics as mech
import abyssrl.utils as utils

from abyssrl.level.defs import Level, Tile, Properties
from abyssrl.level.img import partial_finished
from abyssrl.level.helpers import (find_rooms, iter_neighbours, filter_fillers,
                                   fillers_probs)

def _random(width, height, prob=44):
    return (np.random.randint(100, size=(height, width)) < prob).astype(np.int8)


def _neighbours(data):
    # we skip the border (calculate only middle of array) because they'd be
    # zeroed anyway
    n = np.zeros(data.shape)
    n[1:-1, 1:-1] = (data[0:-2, 0:-2] + data[0:-2, 1:-1] + data[0:-2, 2:] +
                     data[1:-1, 0:-2] +                  + data[1:-1, 2:] +
                     data[2:  , 0:-2] + data[2:  , 1:-1] + data[2:  , 2:])
    return n


def _simulate(data, steps=16, b=6, s=3):
    # default ruleset: B678/S345678
    for _ in range(steps):
        n = _neighbours(data)
        born = ((n >= b) & (data == 0))
        survive = ((n >= s) & (data == 1))

        # Skip if arrays are equal. It usually happens after 9-10 iterations so
        # we save half of computations.
        nd = (survive | born).astype(int)
        if np.array_equal(nd, data):
            break

        data[:] = nd
        partial_finished.emit('caves', data)

    # border
    data[0,:] = 0
    data[:,0] = 0
    data[-1,:] = 0
    data[:,-1] = 0


def _remove_small_rooms(data, rooms, threshold=10):
    def _rm(room):
        if len(room) > threshold:
            return True  # returns whether room stays
        for x, y in room:
            data[y, x] = 0
        return False

    rooms[:] = (room for room in rooms if _rm(room))
    partial_finished.emit('caves', data)


def _connect_rooms(data, rooms, tunnel_cost=6):
    if len(rooms) < 2:
        return

    # pathfinding prefers going through existing tiles, unless destination is
    # far, far away
    walk_map = np.ones(data.shape, dtype=np.int8) \
        + tunnel_cost - (data * tunnel_cost)
    astar = tcod.path.AStar(walk_map, diagonal=0)

    # connect all rooms. Rooms are connected to the next one on the list, so
    # this way we'll always have a path to each one. They also have 50% chance
    # of being connected to random other room. They'll keep connecting to random
    # rooms, so we'll have a little variation in number of exits.
    for i, src in enumerate(rooms):
        # circle buffer
        if i < len(rooms) - 1:
            j = i + 1
        else:
            j = 0

        conn = set([j])

        while mech.chance():
            j = random.choice(range(len(rooms)))
            if j != i:
                conn.add(j)

        for j in conn:
            dst = rooms[j]

            sx, sy = src[0]
            dx, dy = dst[1]
            path = astar.get_path(sx, sy, dx, dy)
            for x, y in path:
                data[y, x] = 1
                walk_map[y, x] = 1

        # Update astar only once in a while. This is not a light operation and
        # it can affect performance significantly
        astar = tcod.path.AStar(walk_map, diagonal=0)
    partial_finished.emit('caves', data)


def _place_tile(otid, x, y,  level, taken):
    if (x, y) in taken:
        return False
    level.tiles.append(Tile(x, y, otid))
    taken.add((x, y))
    return True


def _random_tiles(fillers_or_otids, dice_or_int, walkable, level, taken):
    assert fillers_or_otids, 'empty otid list'

    if isinstance(dice_or_int, int):
        no = dice_or_int
    else:
        no = dice_or_int.roll()

    if isinstance(fillers_or_otids[0], str):
        otids = fillers_or_otids
        p = None
    else:
        otids = [f.otid for f in fillers_or_otids]
        p = fillers_probs(fillers_or_otids)

    while no > 0 and no < (len(walkable) + len(taken)):
        otid = np.random.choice(otids, p=p)
        y, x = random.choice(walkable)
        if _place_tile(otid, x, y, level, taken):
            no -= 1


def _fill(level, data, rooms, fill_data):
    height, width = data.shape

    level.tiles = []

    walkable = np.argwhere(data == 1)

    for y, x in walkable:
        level.tiles.append(Tile(x, y, 'ocean_floor'))

    for y, x in np.argwhere(data == 0):
        if np.any(data[max(0, y-1):min(height, y+2),
                       max(0, x-1):min(width, x+2)]):
            level.tiles.append(Tile(x, y, 'ocean_floor'))
            level.tiles.append(Tile(x, y, 'wall'))

    taken_tiles = set()
    pc_y, pc_x = random.choice(walkable)
    if not _place_tile('player', pc_x, pc_y, level, taken_tiles):
        raise utils.GameError('Cannot place player at %d, %d' % (pc_x, pc_y))

    _random_tiles(['stairs_down'], max(1, round(len(walkable) / 2000)),
                  walkable, level, taken_tiles)

    items = filter_fillers(fill_data.item_list, level.properties.depth)
    _random_tiles(items, mech.Dice.S('5d3+1'),
                  walkable, level, taken_tiles)
    _random_tiles(['oxygen_bulbs'], mech.Dice.S('1d2'),
                  walkable, level, taken_tiles)
    _random_tiles(['refreshing_drink'], mech.Dice.S('1d2'),
                  walkable, level, taken_tiles)

    monster_no=mech.Dice.S('%dd2+2' % round(len(walkable) / 100))
    monsters = filter_fillers(fill_data.monster_list, level.properties.depth)
    _random_tiles(monsters, monster_no, walkable, level, taken_tiles)

def caves(width, height, encounters, properties):
    '''This algorithm tries to generate believable cave system. For this purpose
    it uses cellular automata with ruleset B678/S345678 (birth > 5,
    survive > 2).'''
    if properties is None:
        properties = {}

    data = _random(width, height)
    partial_finished.emit('caves', data)

    _simulate(data)
    _simulate(data, steps=3, b=5, s=4)  # smooths

    rooms = find_rooms(data, diagonal=False)
    _remove_small_rooms(data, rooms)

    _connect_rooms(data, rooms)

    level = Level(width, height, properties=properties)
    _fill(level, data, rooms, encounters)

    return level
