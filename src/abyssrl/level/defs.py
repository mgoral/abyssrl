# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import enum
import attr

class LevelType(enum.Enum):
    prefab = 0
    caves = 1


@attr.s
class Properties:
    depth = attr.ib(0)
    danger = attr.ib(0)
    breathable = attr.ib(False)
    discovered = attr.ib(False)


@attr.s
class Tile:
    x = attr.ib()
    y = attr.ib()
    otid = attr.ib()


@attr.s
class Level:
    width = attr.ib(0)
    height = attr.ib(0)
    tiles = attr.ib(None)
    objects = attr.ib(attr.Factory(dict))
    properties = attr.ib(attr.Factory(Properties))
