# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import os
import itertools

import abyssrl.resources as resources
import abyssrl.fs.tiled as tiled

from .defs import Level, Properties


def _set_properties(level_properties, out):
    for propname in level_properties:
        try:
            ct = type(getattr(out, propname))
        except AttributeError:
            continue

        val = level_properties[propname]
        if ct != type(None):
            setattr(out, propname, ct(val))
        else:
            setattr(out, propname, val)


def prefab(mapname, bases, properties):
    '''Generates a map from a prefabricated text file.'''
    import abyssrl.fs.jsonloader as jsonloader

    data = tiled.load(mapname)
    level = Level()
    level.width = data.level.width
    level.height = data.level.height
    level.tiles = itertools.chain(*data.layers)

    descrf = os.path.splitext(mapname)[0] + '.json'
    try:
        level.objects = jsonloader.loadr(descrf, bases=bases)
    except FileNotFoundError:
        pass

    level.properties = properties
    _set_properties(data.level.properties, level.properties)

    return level
