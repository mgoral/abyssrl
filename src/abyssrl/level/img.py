# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import os
import numpy

import abyssrl.signals as signals
import abyssrl.colors as colors

partial_finished = signals.signal('level-partial-finished')

def _search_otid(otid, defs):
    if isinstance(defs, list) or isinstance(defs, tuple):
        for d in defs:
            obj = d.get(otid)
            if obj:
                return obj
    return defs.get(otid)


def save(path, level, defs, scale=6):
    # local imports, used only when this function is used!
    from PIL import Image

    data = numpy.zeros((level.height, level.width, 3), dtype=numpy.uint8)
    for t in level.tiles:
        obj = _search_otid(t.otid, defs)
        assert obj, 'unknown otid: %s' % t.otid

        try:
            icon = obj['components']['icon']  # normal entities
        except KeyError:
            icon = obj['icon']  # terrains

        if len(icon) > 1:
            fg = icon[1]
        elif t.otid == 'player':
            fg = (255, 255, 255)
        else:
            fg = (255, 0, 255)  # magenta eye killer
        data[t.y, t.x] = fg

    img = Image.fromarray(data)
    img = img.resize((level.width * scale, level.height * scale))
    img.save(path)


def save_partial_layout(name, data, scale=6):
    # local imports, used only when this function is used!
    from PIL import Image

    height, width = data.shape
    img = Image.fromarray((data * 255).astype('int8'), 'L')
    img = img.resize((width * scale, height * scale))

    # do-while
    i = 0
    while True:
        path = '{}-{:03d}.png'.format(name, i)
        if not os.path.exists(path):
            break
        i += 1
    img.save(path)
