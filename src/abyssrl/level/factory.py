# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import copy
import attr
import collections
import json
import numpy as np

import abyssrl.fs.jsonloader as jsonloader
import abyssrl.resources as resources

from abyssrl.level.prefab import prefab as _prefab
from abyssrl.level.caves import caves as _caves
from abyssrl.level.defs import Properties, LevelType

@attr.s
class _FillData:
    @attr.s
    class _Filler:
        otid = attr.ib()
        rarity = attr.ib(10)
        depth_start = attr.ib(1)
        depth_end = attr.ib(None)

    @attr.s
    class _MapFillers:
        # naming is important: %s_list, where %s must match entity type as
        # written in json
        item_list = attr.ib(attr.Factory(list))
        monster_list = attr.ib(attr.Factory(list))

    caves = attr.ib(attr.Factory(_MapFillers))


@attr.s
class Factory:
    bases = attr.ib()
    encounters = attr.ib()

    # default properties copied for each level builder
    _props = attr.ib(attr.Factory(Properties))

    _depths = attr.ib()
    _procedural_names = attr.ib()

    def __call__(self, depth=None, **kwargs):
        if depth is None:
            self._props.depth += 1
        else:
            self._props.depth = depth

        # List of available levels can contain either a list of enum names
        # (except 'prefab'), or prefab file name (without path or extension).
        # Factory tries to be smart about it.
        levels = self._depths.get(str(depth), self._procedural_names)
        name = np.random.choice(levels)
        maptype = LevelType.__members__.get(name, LevelType.prefab)

        if maptype is LevelType.caves:
            builder = _caves
            self._set_dimensions(kwargs)
            kwargs['encounters'] = self.encounters.caves
        elif maptype is LevelType.prefab:
            builder = _prefab
            kwargs['mapname'] = 'maps/%s.tmx' % name
            kwargs['bases'] = self.bases

        kwargs['properties'] = copy.deepcopy(self._props)

        return builder(**kwargs)

    @bases.default
    def default_bases(self):
        objs = jsonloader.loadr('objects/base_entities.json')
        objs.update(jsonloader.loadr('objects/base_terrains.json'))
        objs.update(jsonloader.loadr('objects/base_items.json'))
        objs.update(jsonloader.loadr('objects/base_monsters.json'))

        objs.update(jsonloader.loadr('objects/base_blocks.json', bases=objs))
        return objs

    @encounters.default
    def default_encounters(self):
        encounters = _FillData()

        all_places  = tuple(a.name for a in _FillData.__attrs_attrs__)
        allowed_types = tuple(a.name for a in _FillData._MapFillers.__attrs_attrs__)

        for otid in self.bases:
            obj = self.bases[otid]

            fill_list_name = str(obj.get('type')) + '_list'
            if fill_list_name not in allowed_types:
                continue

            places = obj.get('encountered', all_places)
            rarity = obj.get('rarity', 10)
            ds = obj.get('depth_start', 1)
            de = obj.get('depth_end', None)

            for place in places:
                fillers = getattr(encounters, place)  # e.g. encounters.cave
                otid_list = getattr(fillers, fill_list_name)  # e.cave.item_list
                otid_list.append(_FillData._Filler(otid, rarity, ds, de))

        return encounters

    @_depths.default
    def _default_depths(self):
        s = resources.string('maps/_depths.json').decode('utf-8')
        return json.loads(s)

    @_procedural_names.default
    def _default_procedural_names(self):
        return [lt.name for lt in LevelType if lt is not LevelType.prefab]

    def _set_dimensions(self, kwargs):
        kwargs.setdefault('width', 60)
        kwargs.setdefault('height', 60)
