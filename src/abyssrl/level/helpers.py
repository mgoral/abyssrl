# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import itertools
from collections import deque

import numpy as np

def iter_neighbours(x, y, w, h, diagonal=True):
    '''Iterates all negibours'''
    for nx, ny in itertools.product((x, x+1, x-1), (y, y+1, y-1)):
        if (not diagonal) and (x != nx and y != ny):
            continue
        if x == nx and y == ny:
            continue
        if nx < 0 or nx >= w or ny < 0 or ny >= h:
            continue
        yield nx, ny


def find_rooms(data, *, diagonal=True):
    '''Lists all disjoint rooms.'''
    walked = set()
    rooms = []
    height, width = data.shape
    for y, x in np.argwhere(data == 1):
        if (x, y) in walked:
            continue

        room = []
        stack = deque([(x, y)])
        while len(stack) > 0:
            node = stack.pop()
            room.append(node)
            walked.add(node)

            nx, ny = node
            for cx, cy in iter_neighbours(nx, ny, width, height, diagonal):
                chn = (cx, cy)
                if data[cy, cx] and chn not in walked and chn not in stack:
                    stack.append(chn)

        rooms.append(room)
    return rooms


def filter_fillers(fillers, depth):
    return [f for f in fillers
            if f.depth_start <= depth
                and (f.depth_end is None or f.depth_end >= depth)]


def fillers_probs(fillers):
    arr = np.array([f.rarity for f in fillers])
    s = np.sum(arr)
    return arr / s
