# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import os
import pkg_resources

import abyssrl.version as version

pkg = version.__appname__.lower()


def _fix_path(name):
    if os.path.sep != '/':
        name = name.replace('/', os.path.sep)
    return os.path.join('assets', name)


def stream(name):
    name = _fix_path(name)
    return pkg_resources.resource_stream(pkg, name)


def string(name):
    name = _fix_path(name)
    return pkg_resources.resource_string(pkg, name)


def filename(name):
    name = _fix_path(name)
    return pkg_resources.resource_filename(pkg, name)
