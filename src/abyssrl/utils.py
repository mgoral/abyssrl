# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import time
import decimal
import contextlib

import attr

class GameError(Exception):
    pass

class Stack:
    def __init__(self):
        self._s = []

    def push(self, v):
        self._s.append(v)

    def pop(self):
        return self._s.pop()

    def clear(self):
        self._s.clear()

    def __len__(self):
        return len(self._s)


def expect(b, msg='', etype=GameError):
    if not bool(b):
        raise etype(msg)

def cast(type_):
    '''Implicit conversion of arguments to a type'''
    def _f(args):
        if args is None:
            return None
        if isinstance(args, type_):
            return args
        elif isinstance(args, dict):
            return type_(**args)
        elif isinstance(args, tuple) or isinstance(args, list):
            return type_(*args)
        return type_(args)
    return _f

class Nothing:
    pass

def capital(s):
    '''Capitalizes only the first letter of a string.'''
    return s[0].upper() + s[1:]

@attr.s
class Measurements:
    _s = attr.ib(0)
    _fps = attr.ib(0)

    @contextlib.contextmanager
    def measure(self):
        try:
            tstart = time.time()
            yield
        finally:
            self._s = (time.time() - tstart)
            self._fps = round(1 / self._s)


    @property
    def ms(self):
        decimal.getcontext().prec = 3
        return decimal.Decimal(self._s * 1000).normalize()

    @property
    def fps(self):
        return self._fps

    def __str__(self):
        return '%s ms (%s FPS)' % (self.ms, self.fps)
