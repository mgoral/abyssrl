# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

'''This module is a wrapper for currently used rendering library.'''

from enum import Enum
import time
import contextlib
import attr
from bearlibterminal import terminal

import attr

import abyssrl.colors as colors
import abyssrl.utils as utils

fps = 0
frame_time = 0

_current_font = ''
_current_size = (1, 1)
_fonts = {}

_blocked =  True
_pause_sem = 0

_layer_group = 0

_event_names = {
    terminal.TK_MOUSE_LEFT: 'mouse-left',
    terminal.TK_MOUSE_RIGHT: 'mouse-right',
    terminal.TK_MOUSE_MIDDLE: 'mouse-middle',
    terminal.TK_MOUSE_LEFT|terminal.TK_KEY_RELEASED: 'mouse-left-released',
    terminal.TK_MOUSE_RIGHT|terminal.TK_KEY_RELEASED: 'mouse-right-released',
    terminal.TK_MOUSE_MIDDLE|terminal.TK_KEY_RELEASED: 'mouse-middle-released',
    terminal.TK_MOUSE_MOVE: 'mouse-move',
    terminal.TK_MOUSE_SCROLL: 'mouse-scroll',

    terminal.TK_CLOSE: 'close',
    terminal.TK_RESIZED: 'resize',

    terminal.TK_ENTER: 'enter',
    terminal.TK_RETURN: 'return',
    terminal.TK_ESCAPE: 'escape',
    terminal.TK_TAB: 'tab',
    terminal.TK_BACKSPACE: 'backspace',
    terminal.TK_DELETE: 'delete',

    terminal.TK_LEFT: 'left',
    terminal.TK_RIGHT: 'right',
    terminal.TK_UP: 'up',
    terminal.TK_DOWN: 'down',

    terminal.TK_KP_1: 'keypad1',
    terminal.TK_KP_2: 'keypad2',
    terminal.TK_KP_3: 'keypad3',
    terminal.TK_KP_4: 'keypad4',
    terminal.TK_KP_5: 'keypad5',
    terminal.TK_KP_6: 'keypad6',
    terminal.TK_KP_7: 'keypad7',
    terminal.TK_KP_8: 'keypad8',
    terminal.TK_KP_9: 'keypad9',
    terminal.TK_KP_0: 'keypad0',
    terminal.TK_KP_ENTER: 'enter',

    terminal.TK_HOME: 'home',
    terminal.TK_END: 'end',

}

@attr.s
class MousePos:
    x = attr.ib()
    y = attr.ib()

class Align(Enum):
    default = terminal.TK_ALIGN_DEFAULT
    left = terminal.TK_ALIGN_LEFT
    right = terminal.TK_ALIGN_RIGHT
    center = terminal.TK_ALIGN_CENTER
    top = terminal.TK_ALIGN_TOP
    bottom = terminal.TK_ALIGN_BOTTOM
    middle = terminal.TK_ALIGN_MIDDLE


def start():
    terminal.open()
    reset_colors()


def stop():
    terminal.close()


def pause():
    global _blocked
    _blocked = True


def resume():
    global _blocked
    _blocked = False


def autopause_lock(val=1):
    global _pause_sem
    _pause_sem += val
    resume()


def autopause_unlock(val=1):
    global _pause_sem
    _pause_sem -= val
    if _pause_sem < 0:
        pause_sem = 0


def dispatch_events():
    '''Main loop'''
    global fps
    global _blocked

    event = None
    ttime = 0
    frames = 0

    # HACK. This effectively creates a do-while loop, so it allows some updates,
    # setting screen options, refreshing it etc. before the first input fetch.
    # Thanks to this screen shows to the player, even in blocking mode, game
    # doesn't quit (because it will be set up and refreshed properly) etc., etc.
    yield None

    while True:
        if not _blocked and not terminal.has_input():
            yield None
            continue
        event = terminal.read()
        yield event


def event_char(ev):
    if ev is None:
        return ''
    codepoint = terminal.state(terminal.TK_WCHAR)
    if codepoint == 0:
        return ''
    return chr(codepoint)


def event_name(ev):
    return _event_names.get(ev)


def event_repr(ev):
    ret = []

    # ifs in alphabetical order!
    if terminal.check(terminal.TK_ALT):
        ret.append('a')
    if terminal.check(terminal.TK_CONTROL):
        ret.append('c')
    if terminal.check(terminal.TK_SHIFT):
        ret.append('s')

    ret.append(event_name(ev) or event_char(ev))
    return '-'.join(ret)


def refresh():
    terminal.refresh()
    if not _pause_sem:
        pause()


def clear():
    terminal.clear()


def change_options(options):
    return terminal.set(options)


def configure_font(name, path, *, size, spacing=None, offset=None):
    global _fonts
    if spacing:
        w, _, h = spacing.partition('x')
        w = int(w)
        h = int(h)
    else:
        w, h = 1, 1

    if offset:
        fname = '0x%x' % offset
    else:
        fname = '%s font' % name

    _fonts[name] = (w, h)
    opts = '%s: %s, size=%s, spacing=%dx%d;' % (fname, path, size, w, h)
    return change_options(opts)


def font_spacing(name=None):
    if name is None:
        name = _current_font
    return _fonts[name]


@contextlib.contextmanager
def font(name):
    try:
        global _current_font
        global _current_size
        prev = _current_font
        terminal.font(name)
        _current_font = name
        _current_size = font_spacing()
        yield
    finally:
        terminal.font(prev)
        _current_font = prev
        _current_size = font_spacing()


@attr.s
class Surface:
    '''Layer- and color-agnostic workaround for lack of blitting in
    bearlibterminal. It's a rectangle with local coordinates which can be used
    to place characters/tiles in their correct positions.'''
    x = attr.ib(0)
    y = attr.ib(0)
    w = attr.ib(0)
    h = attr.ib(0)

    def coords_inside(self, x, y):
        '''Returns whether given absolute coordinates are located inside this
        surface.'''
        return (x >= self.x and x < self.x + self.w and
                y >= self.y and y < self.y + self.h)

    def abs2loc(self, x, y):
        '''Converts absolute coordinates to surface local ones.'''
        return (int(x / _current_size[0]) - self.x, 
                int(y / _current_size[1]) - self.y)

    # TODO: for put() and fill() change argument order: glyph should be the
    # first one, so one doesn't have to write e.g. put(glyph='g').
    # TODO:x, y in put() should have default values: 0
    def put(self, x, y, glyph):
        terminal.put(
            self.x + x * _current_size[0],
            self.y + y * _current_size[1],
            glyph)

    def fill(self, x=0, y=0, w=None, h=None, glyph='█'):
        if w is None:
            w = self.w
        if h is None:
            h = self.h

        sw, sh = font_spacing()
        for xx in range(x, x + w):
            for yy in range(y, y + h):
                terminal.put(self.x + xx, self.y + yy, glyph)

    def print(self, x, y, s, *args, **kwargs):
        terminal.print_(self.x + x, self.y + y, s, *args, **kwargs)
        reset_colors()

    def clear(self):
        terminal.clear_area(self.x, self.y, self.w, self.h)


def window_size():
    w, _, h = terminal.get('window.size').partition('x')
    return int(w), int(h)


def screen():
    w, h = window_size()
    return Surface(0, 0, w, h)


def center(scale=0.7, maxw=None, maxh=None):
    '''A helper function which returns a centered surface'''
    w, h = window_size()
    sw, sh = round(w * scale), round(h * scale)
    if maxw:
        sw = min(maxw, sw)
    if maxh:
        sh = min(maxh, sh)
    left_offset = round((w - sw) / 2)
    top_offset = round((h - sh) / 2)
    return Surface(left_offset, top_offset, sw, sh)


def clear_layer_group():
    start = min_layer_in_group()
    end = max_layer_in_group()

    w, h = window_size()
    curr = current_layer()
    for i in range(start, end):
        terminal.layer(i)
        terminal.clear_area(0, 0, w, h)
    terminal.layer(curr)


def reset_colors():
    terminal.color(colors.name(colors.white))
    terminal.bkcolor(colors.name(colors.black))


def color(fg=None, bg=None):
    if fg:
        terminal.color(terminal.color_from_argb(255, *fg))
    if bg:
        terminal.bkcolor(terminal.color_from_argb(255, *bg))


@contextlib.contextmanager
def colorize(fg=None, bg=None):
    currfg, currbg = None, None

    try:
        if fg:
            currfg = terminal.state(terminal.TK_COLOR)
            terminal.color(terminal.color_from_argb(255, *fg))
        if bg:
            currbg = terminal.state(terminal.TK_BKCOLOR)
            terminal.bkcolor(terminal.color_from_argb(255, *bg))
        yield
    finally:
        if currfg is not None:
            terminal.color(currfg)
        if currbg is not None:
            terminal.color(currbg)


@contextlib.contextmanager
def layer(no):
    lo = min_layer_in_group()
    hi = max_layer_in_group()
    if no < lo or no > hi:
        raise utils.GameError(
            'Incorrect layer: %d; should be in range [%d, %d)' % (no, lo, hi))

    try:
        curr = current_layer()
        terminal.layer(no)
        yield
    finally:
        terminal.layer(curr)


@contextlib.contextmanager
def next_layer():
    curr = current_layer()
    with layer(curr + 1):
        yield


@contextlib.contextmanager
def next_layer_group():
    global _layer_group
    try:
        _layer_group += 1
        with layer(min_layer_in_group()):
            yield
    finally:
        _layer_group -= 1


def layer_width():
    # arbitrary number; "virtual" layers shouldn't need more layers than that
    return 4


def min_layer_in_group():
    return _layer_group * layer_width()


def max_layer_in_group():
    return _layer_group * layer_width() + layer_width() - 1


def current_layer():
    return terminal.state(terminal.TK_LAYER)


def measure_text(text, *a, **kw):
    return terminal.measure(text, *a, **kw)


def mouse_pos():
    return MousePos(terminal.state(terminal.TK_MOUSE_X),
                    terminal.state(terminal.TK_MOUSE_Y))

def mouse_clicks():
    return terminal.state(terminal.TK_MOUSE_CLICKS)
