# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License

from functools import partial
import math

import numpy

import abyssrl.effect as _e
import abyssrl.mechanics as mech

from abyssrl.defs import ItemKind, Rarity
from abyssrl.locale import _

_armor_effects = [
    partial(_e.RegenerateHealth, amount=1, chance=0.1,
            trigger=_e.Effect.Trigger.equipped, end=math.inf),
    partial(_e.RegenerateOxygen, amount=1, chance=0.1,
            trigger=_e.Effect.Trigger.equipped, end=math.inf),
    partial(_e.OxygenTank, storage=mech.Dice(1, 20, 5),
            trigger=_e.Effect.Trigger.equipped, end=math.inf),
    partial(_e.Armor, ac=1),
            
]

_weapon_effects = [
    partial(_e.DamageSetter, dmg=mech.Dice(1,3)),
    partial(_e.Accuracy, acc=mech.Dice(1,2)),
]

_gen_effects = {
    # TODO: other item kinds
    ItemKind.weapon: _weapon_effects,
    ItemKind.armor: _armor_effects,
}

def generate_effects(entity):
    item = entity.item
    descr = entity.descr

    if item.money:
        return

    effect_slots = _count_effect_slots(entity.randomize.rarity)
    if not effect_slots:
        return

    available_effs = _gen_effects.get(item.kind)
    if not available_effs:
        return

    effect_slots = min(effect_slots, len(available_effs))

    prefix = None
    suffix = None
    choices = numpy.random.choice(available_effs, effect_slots, replace=False)

    for eff_cls in choices:
        eff = eff_cls()
        if not prefix and eff.item_prefix:
            prefix = eff.item_prefix
        elif not suffix and eff.item_suffix:
            suffix = eff.item_suffix

        for prev_eff in item.effects:
            if prev_eff.merge(eff):
                break
        else:
            item.effects.append(eff)

    if len(choices) >= 4:
        if prefix:
            prefix = _('Legendary %s') % prefix
        else:
            prefix = _('Legendary')

    if descr:
        if prefix:
            descr.prefix = prefix
        if suffix:
            descr.suffix = suffix


def _count_effect_slots(rarity):
    if rarity >= Rarity.uncommon  and rarity < Rarity.rare:
        return 1
    elif rarity >= Rarity.rare and rarity < Rarity.unique:
        return 2
    elif rarity >= Rarity.unique and rarity < Rarity.legendary:
        return mech.roll(1, 2, mod=1)  # 2-3
    elif rarity >= Rarity.legendary and rarity < Rarity.PAST_THE_END:
        return 4

    return 0
