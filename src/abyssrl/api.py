# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

'''High-level public interface for interacting with a game. Use it whenever it's
possible.'''

import logging
from copy import deepcopy, copy

import abyssrl.mechanics as mech
import abyssrl.config as config
import abyssrl.version as version
from abyssrl.locale import _

_ctl = None
_world = None
_factions = {}

log = logging.getLogger('%s.%s' % (version.__appname__, __name__))

def init(ctl, world):
    '''Initialize Scripts API.'''
    global _ctl
    global _world
    global _factions

    # first reset globals from previous runs
    _ctl = None
    _world = None
    _factions = {}

    _ctl = ctl
    _world = world

    set_hostility('player', 'npc', 0)
    set_hostility('player', 'wild_fishes', -100)


def quit():
    _ctl.stop()


def entity(id_):
    '''Returns a real entity by its id'''
    return _ctl.objects.get(id_)


def system_node(id_, system):
    '''Returns a node by its id, as currently seen by a given system. It's
    useful because the global list of entities might get updated later than when
    this function is called (when it's called e.g. during system's update()
    method. See system.py for explanation).'''
    s = getattr(_ctl, system)
    return s.node(id_)


def schedule_change_level(*args, **kwargs):
    '''Schedules change of level as soon as possible (on next update).'''
    _ctl.leveladmin.request_level_change(*args, **kwargs)


def hostility(lhs, rhs):
    '''Returns a hostility value between the two nodes. Hostility is an integer
    in range [-100, 100], where -100 means that two factions are mortal enemies,
    0 means that they're neutral to each other and 100 means that they're loving
    each other'''
    ret = _factions.get(lhs.descr.faction, {}).get(rhs.descr.faction)
    if ret is None:
        return 0
    return ret


def set_hostility(lhs, rhs, hostility):
    _factions.setdefault(lhs, {})
    _factions.setdefault(rhs, {})
    _factions[lhs][rhs] = hostility
    _factions[rhs][lhs] = hostility


def destroy(node):
    '''Destroys an object represented by a node. Removes that object from a
    world.'''
    ent = _real(node)
    if not ent:
        return

    if ent.actions:
        ent.actions.queue.clear()
    if ent.effects:
        ent.effects.active.clear()
        ent.effects.pending.clear()

    if not is_pc(node):
        # Game over is handled by an appropriate subsystem
        _ctl.remove_object(node)


def remove_from_world(node):
    '''Remove an object represented by a node from a world. Object isn't
    destroyed. Returns whether removal was successful.'''
    cell = _world.atpos(node.pos)
    return cell.remove(node.id)


def redraw_cell(node):
    '''Marks a cell at which node exists as redrawable. This typically occurs
    when node changes its appearence.'''
    try:
        cell = _world.atpos(node.pos)
        cell.needs_update = True
    except AttributeError:
        pass


def update_real_entity(node, comp_name, val):
    '''Sets specified component of a real entity (which node is a view for) to a
    given value.'''
    setattr(_real(node), comp_name, val)


def is_pc(node):
    '''Returns whether a given node is player'''
    return node.id == _ctl.player.pc.id


def pc():
    '''Returns player entity'''
    return _ctl.player.pc


def recompute_fov(node):
    '''Requests field of view to be recomputed as soon as possible for a given
    node'''
    if is_pc(node):  # currently only player has FOV
        _ctl.player.recompute_fov()


def items(pos):
    '''Returns all items found on a given position on the map.'''
    if not _world.valid(pos):
        return []
    cell = _world.atpos(pos)
    return [o for o in cell.objects if o.item]


def move(node, newpos, o2=0):
    '''Moves a given node to a new position. Returns whether move was
    performed.'''
    if not _world.valid(newpos):
        return False

    oldcell = _world.atpos(node.pos)
    nc = _world.atpos(newpos)

    if (nc.terrain and not triggerable(newpos) and nc.move_cost > 0):
        node.actions.cooldown = node.props.move_cost * nc.move_cost
        ent = _real(node)
        ent.pos = newpos
        nc.add(ent)
        oldcell.remove(node.id)
        take_o2(node, o2)
        return True
    return False


def neighbours_pos(lhs, rhs):
    '''Returns a boolean telling whether two positions are located on a
    neighbour cells.'''
    diff = lhs - rhs
    return abs(diff.x) <= 1 and abs(diff.y) <= 1


def neighbours(lhs, rhs):
    '''Returns a boolean telling whether two nodes are located on a neighbour
    cells.'''
    return neighbours_pos(lhs.pos, rhs.pos)


def triggerable(pos):
    '''Tells whether a cell at position `pos` is triggerable, i.e. whether it
    has actor or any triggerable feature.'''
    if not _world.valid(pos):
        return False

    cell = _world.atpos(pos)
    return cell.actor or (cell.feature and cell.feature.props.destroyable)


def trigger(pos, node, o2=0):
    '''Triggers a triggerable object located at a given position by a given
    node. Returns whether triggering was successfull.'''
    if not _world.valid(pos):
        return False

    cell = _world.atpos(pos)

    if cell.actor and cell.actor.id != node.id:
        return trigger_obj(cell.actor, node, o2)
    elif cell.feature and cell.feature.props.destroyable:
        return trigger_obj(cell.feature, node, o2)
    return False


def usable(pos):
    '''Tells whether there's a usable feature at position `pos`.'''
    if not _world.valid(pos):
        return False
    cell = _world.atpos(pos)
    return cell.feature and cell.feature.props.usable


def hostile(node, pos):
    '''Tells whether there's a hostile at position `pos`.'''
    if not _world.valid(pos):
        return False
    cell = _world.atpos(pos)
    return cell.actor and hostility(node, cell.actor) < 0


def use(pos, node, o2=0):
    '''Use a feature (by triggering it)'''
    cell = _world.atpos(pos)
    if cell.feature and cell.feature.props.usable:
        return trigger_obj(cell.feature, node, o2)
    return False


def trigger_obj(node, by_who, o2=0):
    '''Triggers some node by a another node.'''
    se = _ctl.scriptexecutor
    se.trigger_triggered(node, by_who)
    take_o2(by_who, o2)
    return True


def path(node, pos):
    '''Returns path from a node to a given position. First element of path
    will be always node's position (this simplifies path caching as node can
    always search its position inside a path and start from it)'''
    ret = [node.pos]
    ret.extend(_world.path(node.pos, pos))
    return ret


def swap_items(lhs, rhs, slots):
    '''Swaps slots of two items, without equipping or unequipping any of them'''
    clhs = lhs.item
    crhs = rhs.item

    # Both items are equipped
    if not clhs.slot or not crhs.slot:
        return False

    # Items are interchangable, i.e. fit each other slots
    if clhs.slot not in crhs.fits or crhs.slot not in clhs.fits:
        return False

    # Items are in the same inventory
    if clhs not in slots or crhs not in slots:
        return False

    tmp = crhs.slot
    crhs.slot = clhs.slot
    clhs.slot = tmp
    return True


def move_item(item, slot):
    '''Moves item to a different slot, withouth re-equipping it'''
    citem = item.item

    # Item is equipped
    if not citem.slot:
        return False

    # Item fits a new slot
    if slot not in citem.fits:
        return False

    citem.slot = slot
    return True


def equip(node, item, replace=True, slot=None):
    '''Equips an item on a given node. Item is equipped in a given slot or, if
    it isn't set, a correct slot is automatically determined. If selected slot
    is occupied by any item and `replace` is true, the old item is automatically
    unequipped.'''
    def _equipped():
        owner = item.item.owner
        for eff in item.item.effects:
            if eff.trigger == eff.Trigger.equipped:
                activate(eff, owner)


    inv = actor(node).inventory

    # convienient access; we store entities/nodes in inventory
    citem = item.item

    if citem.stackable:
        return False

    # First determine the correct usable slot.
    if slot and (slot not in citem.fits or (inv[slot.value] and not replace)):
        return False
    elif not slot:
        for fit in citem.fits:
            if not inv.equipped(fit) or replace:
                slot = fit
                break
        else:
            return False

    if replace and inv.slots[slot.value]:
        other = inv[slot.value]
        if citem.slot:
            # both items are already equipped. Just swap them and don't touch
            # any equipped() / unequipped() effects.
            return swap_items(item, other, inv.slots)
        else:
            # Free a chosen slot
            unequip(node, slot)

    if not inv.equipped(slot):
        if citem.slot:
            return move_item(item, slot)

        # item isn't equipped, but it still may be not in a backpack (e.g. when
        # auto-equipping item laying on the ground)
        inv.slots[slot.value] = item
        citem.slot = slot
        _equipped()
        _remove_node(inv.backpack, item)
        return True
    return False


def unequip(node, slot):
    '''Frees (unequips) an item from a given inventory node. Item is placed in a
    backpack.'''
    def _unequipped(item):
        owner = item.item.owner
        for eff in item.item.effects:
            if eff.trigger == eff.Trigger.equipped:
                deactivate(owner, eff)

    inv = actor(node).inventory
    curr = inv.slots[slot.value]
    if curr:
        _unequipped(curr)
        curr.item.slot = None
        inv.backpack.append(curr)
        inv.slots[slot.value] = None
        return True
    return False


def find_stack(item, backpack):
    '''Finds a stack of items matching a given item'''
    if not item.item.stackable:
        return None

    lhs = (item.descr.name, item.descr.prefix, item.descr.suffix, item.item)
    for ib in backpack:
        if ib.item.stackable:
            rhs = (ib.descr.name, ib.descr.prefix, ib.descr.suffix, ib.item)
            if lhs == rhs:
                return ib
    return None


def pick_item(item, node):
    '''Pick an item by a node'''
    if not remove_from_world(item):
        return False

    if item.item.money:
        node.actor.inventory.money += item.item.money
    else:
        stack = find_stack(item, node.actor.inventory.backpack)
        if not stack:
            node.actor.inventory.backpack.append(item)
            node.actor.inventory.weight += item.item.weight
            item.item.owner = node
            trigger_obj(item, node)  # TODO: should this be called for stacks?
        else:
            stack.item.amount += 1
            destroy(item)
    return True


def discard_item(item):
    '''Remove item from its owner inventory inventory and then destroy it'''
    citem = item.item

    if citem.owner:
        inv = actor(citem.owner).inventory
        if citem.slot:
            unequip(citem.owner, citem.slot)
            del inv.backpack[-1]
        else:
            _remove_node(inv.backpack, item)
        inv.weight -= citem.weight
        citem.owner = None

    destroy(item)


def use_item(item, target, filter_=None):
    '''Uses an item (activates all effects from it). Optionally accepts a filter
    which selects effects by their trigger type.'''

    citem = item.item
    eactiv = False

    ent = _real(target)
    if not ent.effects:
        return False

    for eff in citem.effects:
        if filter_ and eff.trigger != filter_:
            continue

        # stackable items are one-shot items, so discharging them would destroy
        # them immediately. Instead we discharge only the last item from the
        # stack (i.e. when amount <= 1)
        should_discharge = (not citem.stackable or citem.amount <= 1)
        eactiv = activate(eff, target, should_discharge) or eactiv

    if citem.stackable and eactiv:
        citem.amount -= 1

    if citem.autodiscard and not has_effects(item):
        discard_item(item)

    return eactiv


def activate(effect, target, discharge=True):
    '''Queues activation of an effect on a given target. Returns whether request
    was enqueued.'''
    ent = _real(target)
    if not ent.effects:
        return False

    if effect and effect.charges > 0:
        ent.effects.pending.push(effect)
        if discharge:
            effect.charges -= 1
        return True
    return False


def deactivate(target, effect):
    '''Immediately deactivates active effect'''
    ent = _real(target)
    effect.undo(ent)
    del ent.effects.active[effect]


def has_effects(item):
    '''Returns whether an item has effects which can be activated.'''
    citem = item.item
    return any(eff.charges > 0 for eff in citem.effects)


def actor(node):
    '''Returns actor component of node, i.e. pc or ai. It's a workaround for a
    fact that nodes with different components describing actors might be
    given.'''
    ent = _real(node)
    return ent.pc or ent.ai


def regenerate(node, stat_name, amount=None):
    '''Regenerates a given resource. If amount is not given, it is regenerated
    to the maximum value. Returns regenerated amount.'''
    stat = getattr(node.stats, stat_name)
    max_stat = getattr(node.stats, 'max_%s' % stat_name)

    if amount is None:
        amount = max_stat
    points = max(min(max_stat - stat, amount), 0)
    setattr(node.stats, stat_name, stat + points)
    return points


def attack(attacker, defender):
    '''Attack defender by the attacker. Queues appropriate effects. Returns
    whether attack was queued (i.e. whether attack hits or misses).'''

    def _dmg(effect, rolled):
        att = rolled + attacker.stats.accuracy

        log.debug('ATT: %s [c=red]%d[/c]=%d%+d vs %s [c=grey]%d[/c]' %
            (attacker.descr.fullname, att, rolled, att - rolled,
             defender.descr.fullname, defender.stats.ac))

        # XXX: HACK. Most effects have their owners set, but e.g. natural damage
        # doesn't.
        effect.owner = attacker

        if rolled == 1:  # critical miss
            return False
        elif rolled == 20:  # critical hit
            critical_effect = copy(effect)
            critical_effect.dmg = effect.dmg.maxroll
            defender.effects.pending.push(critical_effect)
            return True
        elif att > defender.stats.ac:
            defender.effects.pending.push(effect)
            return True
        else:
            return False


    inv = actor(attacker).inventory
    weapons = inv.equipped_weapons

    # attacker is bare handed
    if not weapons:
        rolled = mech.roll(1, 20)
        return _dmg(attacker.stats.natural_damage, rolled)

    queued = False

    # try to use damage of any weapon
    for item in weapons:
        if not item:
            continue
        rolled = mech.roll(1, 20)

        for eff in item.item.effects:
            if eff.trigger is eff.Trigger.attacking and hasattr(eff, 'dmg'):
                queued = _dmg(eff, rolled) or queued

    return queued


def take_o2(node, val):
    '''Substracts oxygen. If given amount is higher than the current node's
    oxygen, its HP is taken instead.'''
    # Oxygen is player-centric mechanic. We don't want to accidentaly kill our
    # monsters
    if _world.properties.breathable or not is_pc(node):
        return

    o2_taken = min(val, node.stats.o2)
    hp_taken = val - o2_taken

    node.stats.o2 -= o2_taken
    node.stats.hp -= hp_taken


def move_cost(node, distance=1):
    '''Returns how much oxygen will be used for traversing a given distance.'''
    inv = actor(node).inventory
    return int(inv.force() / 10 * distance)


def turn():
    '''Returns a turn number'''
    return _ctl.turn


def increase_threat(node, threat):
    '''Increases threat level (or ordinary level) of a given node.'''
    if threat < 1:
        return

    if node.stats:
        node.stats.increase_threat(threat)
    if node.item:
        for eff in node.item.effects:
            eff.increase_level(threat)


def iter_visible_actors(node):
    '''Iterates visible actors.'''
    for c in _world.visible_cells():
        if c.actor and c.actor.id != node.id:
            yield c.actor


def iter_hostiles(node):
    '''Iterates visible hostiles.'''
    for a in iter_visible_actors(node):
        if hostility(node, a) < 0:
            yield a


def _real(node):
    '''Returns a real entity for a given node.

    Don't expose this function in a public interface.'''
    return _ctl.objects.get(node.id)


def _remove_node(sequence, node):
    '''Removes any node with a given id from a list. It's useful when lists
    store nodes of a different type than a given node (in which case
    list.remove() fails because nodes pointers are different).'''
    sequence[:] = [n for n in sequence if n.id != node.id]
