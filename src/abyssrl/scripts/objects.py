# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import string

import abyssrl.api as api
import abyssrl.gui.dialogs as dialogs
from abyssrl.locale import _

def stairs_down(stairs, by_who):
    if not api.is_pc(by_who):
        return

    pc = api.entity(by_who.id)
    api.schedule_change_level(pc)


def signpost(post, by_who):
    title = post.descr.fullname
    if title.islower():
        title = string.capwords(title)

    box = dialogs.ChooseBox(
        title=title,
        text=post.descr.descr,
        choices=[(None, _('Close'))])
    box.run()
