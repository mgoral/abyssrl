# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import math
import logging

import abyssrl.api as api
import abyssrl.mechanics as mech
import abyssrl.config as config

import abyssrl.version as version
from abyssrl.utils import capital
from abyssrl.locale import _

log = logging.getLogger('%s.%s' % (version.__appname__, __name__))

TURNS_TO_FINISH_ENCOUNTER = 9

def created(player):
    player.stats.max_hp += player.stats.base_strength
    player.stats.hp = player.stats.max_hp


def every_turn(player):
    # Breathe
    if api.turn() % 5 == 0:
        api.take_o2(player, 1)

    player.pc.turns_since_attacked += 1


def triggered(player, who):
    if api.hostility(player, who) >= 0:
        return

    if player.stats.hp <= 0:
        return

    player.pc.turns_since_attacked = 0

    hits = api.attack(who, player)
    if not hits:
        log.info(_('%(att)s attack misses you.') %
                 dict(att=capital(who.descr.fullname)))
