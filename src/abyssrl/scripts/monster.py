# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import math
import logging
from enum import Enum

import abyssrl.api as api
import abyssrl.actions as actions
import abyssrl.colors as colors
import abyssrl.version as version
from abyssrl.utils import capital
from abyssrl.locale import _

log = logging.getLogger('%s.%s' % (version.__appname__, __name__))

class State(Enum):
    inactive = 0
    active = 1


def _attacks(target, monster):
    queue = monster.actions.queue
    return (
        queue and
        isinstance(queue[-1], actions.AttackMelee) and
        queue[-1].target_id == target.id)


def created(monster):
    monster.props.state = State.inactive


def destroyed(monster):
    pass


def every_turn(monster):
    if monster.props.state is State.inactive:
        return

    queue = monster.actions.queue

    dest = api.pc()

    if dest is None:
        queue.clear()
    elif not _attacks(dest, monster):
        queue.append(actions.AttackMelee(dest.id, repeat=math.inf))


def triggered(monster, who):
    if api.hostility(monster, who) >= 0:
        return

    hits = api.attack(who, monster)

    if not hits and api.is_pc(who):
        log.info(_('You miss %s.') % monster.descr.fullname)


def seen(monster, by_who):
    monster.props.state = State.active
