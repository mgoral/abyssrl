# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import logging

import abyssrl.config as config
import abyssrl.version as version

from abyssrl.scenes import SceneManager, MainMenu

log = logging.getLogger(version.__appname__)

def main():
    if config.get('dev.debug'):
        log.setLevel(logging.getLevelName('DEBUG'))
        log.addHandler(logging.StreamHandler())
    else:
        log.setLevel(logging.getLevelName('INFO'))

    with SceneManager() as sm:
        try:
            sm.push(MainMenu(sm))
            sm.run()
        except:
            if config.get('dev.debug'):
                import pdb; pdb.post_mortem()
            else:
                raise
