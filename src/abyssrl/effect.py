# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License

import contextlib
import weakref
import logging
import math
from enum import Enum

import attr

import abyssrl.defs as defs
import abyssrl.api as api
import abyssrl.mechanics as mech
import abyssrl.utils as utils
import abyssrl.config as config
import abyssrl.version as version

from abyssrl.utils import cast, capital
from abyssrl.locale import _, P_

log = logging.getLogger('%s.%s' % (version.__appname__, __name__))


def _describe(effect, descr, annotations=None):
    if annotations is None:
        annotations = []

    charges = effect.charges
    if charges != math.inf:
        annotations.append(P_('%d charge', '%d charges', charges) % charges)

    if annotations:
        descr += ' (%s)' % ', '.join(annotations)

    return descr


@attr.s(cmp=False)
class Effect:
    # XXX: DON'T EVER CHANGE PER-INSTANCE'S Trigger class!
    # It's a nested enum, available publicly to allow effect users (like api) to
    # use it wthout importing effect. This is important, because api preferably
    # shouldn't import anything from abyssrl as it'd cause circular imports!
    class Trigger(Enum):
        activated = 0
        equipped = 1
        attacking = 2
        attacked = 3
        auto = 4

    name = attr.ib(None)
    level = attr.ib(1)

    _owner_id = attr.ib(None, repr=False)

    trigger = attr.ib(None, converter=cast(Trigger))
    end = attr.ib(math.inf)
    charges = attr.ib(math.inf)

    @property
    def description(self):
        return ''

    def do(self, target):
        # By default effects trigger only before adding it to active effects (so
        # it will not trigger every turn). This is a correct behavior for most
        # one-shot effects. If do() should be performed every turn, simply do
        # not check super().do().
        return target.effects and self not in target.effects.active

    def undo(self, target):
        pass

    @property
    def owner(self):
        return api.entity(self._owner_id)

    @owner.setter
    def owner(self, node):
        if node is None:
            self._owner_id = node
        else:
            self._owner_id = node.id

    def applicable(cls, node):
        '''Should effect be auto applied by EffectsResolution system?'''
        return False

    def merge(self, other):
        '''Add other effect to the current one. Returns whether effects were
        successfully merged.'''
        return False

    def increase_level(self, lvl):
        self.level += lvl


@attr.s(cmp=False)
class DualWield(Effect):
    trigger = attr.ib(Effect.Trigger.auto)

    def __attrs_post_init__(self):
        self.name = _('Dual Wielding')

    @property
    def description(self):
        return _('Dual Wielding')

    def do(self, target):
        if super().do(target):
            target.stats.accu_bonus += defs.DUAL_WIELD_PENALTY
            return True
        return False

    def undo(self, target):
        target.stats.accu_bonus -= defs.DUAL_WIELD_PENALTY

    def applicable(cls, node):
        return len(api.actor(node).inventory.equipped_weapons) > 1


@attr.s(cmp=False)
class DamageSetter(Effect):
    dmg = attr.ib(None)
    trigger = attr.ib(Effect.Trigger.attacking)
    end = attr.ib(0)

    item_prefix = attr.ib(_('devastating'))
    item_suffix = attr.ib(_('of damage'))

    def __attrs_post_init__(self):
        self.name = _('Damage')

    @property
    def description(self):
        return _describe(self, _('Damage: %s') % self.dmg)

    def do(self, target):
        if not super().do(target):
            return False

        if target.stats.hp <= 0:
            return False

        owner = self.owner
        targets_pc = api.is_pc(target)
        pc_owner = owner and api.is_pc(owner)

        # owner might be destroyed before - in that case all we have is its
        # previous id, so we have to handle displaying its name somehow.
        owner_fullname = self._safe_fullname(owner)
        target_fullname = target.descr.fullname

        if targets_pc:
            log.info(_('You were hit by %s') % owner_fullname)
        elif pc_owner:
            log.info(_('You hit %s') % target_fullname)

        if targets_pc and config.godmode():
            return True

        dmg = _roll_or_get(self.dmg)

        log.debug('DMG: %s hits %s for %d' %
            (owner_fullname, target_fullname, dmg))

        target.stats.hp -= dmg

        if target.stats.hp <= 0 :
            t = _('%(att)s kills %(dfn)s!')
            if targets_pc:
                t = '[c=red]%s[/c]' % t
            elif pc_owner:
                t = '[c=yellow]%s[/c]' % t
            fmt = dict(att=capital(owner_fullname),
                       dfn=target_fullname)
            log.info(t % fmt)

            api.destroy(target)

        elif pc_owner and target.stats.hp < target.stats.max_hp * 0.6:
            log.info(_('[c=yellow]%s is wounded[/c]') % \
                     capital(target_fullname))

        elif pc_owner and target.stats.hp < target.stats.max_hp * 0.3:
            log.info(_('[c=yellow]%s is heavy wounded[/c]') % \
                     capital(target_fullname))

        return True

    def merge(self, other):
        if isinstance(other, self.__class__) and self.end == other.end:
            self.dmg += other.dmg
            self.__attrs_post_init__()
            return True
        return False

    def increase_level(self, lvl):
        super().increase_level(lvl)
        self.dmg += lvl

    def _safe_fullname(self, node):
        if node:
            return node.descr.fullname
        return _('someone')


@attr.s(cmp=False)
class Accuracy(Effect):
    acc = attr.ib(0)
    trigger = attr.ib(Effect.Trigger.equipped)

    item_prefix = attr.ib(_('accurate'))
    item_suffix = attr.ib(_('of focus'))

    def __attrs_post_init__(self):
        self.acc = _roll_or_get(self.acc)
        self.name = _('Accuracy')

    @property
    def description(self):
        return _describe(self, _('Accuracy: +%d') % self.acc)

    def do(self, target):
        if super().do(target):
            target.stats.accu_bonus += self.acc
            return True
        return False

    def undo(self, target):
        target.stats.accu_bonus -= self.acc
        super().undo(target)

    def increase_level(self, lvl):
        super().increase_level(lvl)
        self.acc += lvl

    def merge(self, other):
        if isinstance(other, self.__class__) and self.end == other.end:
            self.acc += other.acc
            self.__attrs_post_init__()
            return True
        return False


@attr.s(cmp=False)
class Armor(Effect):
    ac = attr.ib(0)
    trigger = attr.ib(Effect.Trigger.equipped)

    item_prefix = attr.ib(_('protective'))
    item_suffix = attr.ib(_('of defense'))

    def __attrs_post_init__(self):
        self.ac = _roll_or_get(self.ac)

        self.name = _('Armor')

    @property
    def description(self):
        return _describe(self, _('Armor: +%d') % self.ac)

    def do(self, target):
        if super().do(target):
            target.stats.armor_bonus += self.ac
            return True
        return False

    def undo(self, target):
        target.stats.armor_bonus -= self.ac
        super().undo(target)

    def increase_level(self, lvl):
        super().increase_level(lvl)
        self.ac += lvl

    def merge(self, other):
        if isinstance(other, self.__class__) and self.end == other.end:
            self.ac += other.ac
            self.__attrs_post_init__()
            return True
        return False


@attr.s(cmp=False)
class OxygenTank(Effect):
    storage = attr.ib(0)
    trigger = attr.ib(Effect.Trigger.attacking)

    item_prefix = attr.ib(None)
    item_suffix = attr.ib(_('of breath'))

    def __attrs_post_init__(self):
        self.storage = _roll_or_get(self.storage)

        self.name = _('Oxygen Tank')

    @property
    def description(self):
        return _describe(self, _('Oxygen storage: +%d') % self.storage)

    def do(self, target):
        if super().do(target):
            target.stats.o2_bonus += self.storage
            return True
        return False

    def undo(self, target):
        target.stats.o2_bonus -= self.storage
        if target.stats.o2 > target.stats.max_o2:
            target.stats.o2 = target.stats.max_o2
        super().undo(target)

    def increase_level(self, lvl):
        super().increase_level(lvl)
        self.storage += 5 * lvl

    def merge(self, other):
        if isinstance(other, self.__class__) and self.end == other.end:
            self.storage += other.storage
            self.__attrs_post_init__()
            return True
        return False


@attr.s(cmp=False)
class Regenerate(Effect):
    stat = attr.ib('hp')
    amount = attr.ib(None)
    chance = attr.ib(1)
    end = attr.ib(1)
    trigger = attr.ib(Effect.Trigger.activated)

    item_prefix = attr.ib(_('curing'))
    item_suffix = attr.ib(_('of life'))

    def __attrs_post_init__(self):
        self.name = _('Regeneration')

    @property
    def description(self):
        ch = int(self.chance * 100)
        descr = _('%(what)s regeneration: %(amount)s') % \
            dict(amount=self.amount, what=self.stat.upper())

        ann = None
        if self.chance < 1:
            ann = [_('%d%% chance') % ch]
        return _describe(self, descr, ann)

    def do(self, target):
        if not mech.chance(self.chance):
            # effect took place; prevents removing effect from effect queue
            return True

        amount = _roll_or_get(self.amount)
        regenerated = api.regenerate(target, self.stat, amount)
        if regenerated > 0 and api.is_pc(target):
            log.info(_('You regenerated %(amount)d %(what)s') %
                dict(amount=amount, what=self.stat.upper()))
        return True

    def increase_level(self, lvl):
        super().increase_level(lvl)
        self.amount += lvl * 2

    def merge(self, other):
        if isinstance(other, self.__class__) \
                and self.stat == other.stat \
                and self.end == other.end:
            self.amount += other.amount
            self.__attrs_post_init__()
            return True
        return False


@attr.s(cmp=False)
class RegenerateHealth(Regenerate):
    stat = attr.ib('hp')

    item_prefix = attr.ib(_('curing'))
    item_suffix = attr.ib(_('of life'))


@attr.s(cmp=False)
class RegenerateOxygen(Regenerate):
    stat = attr.ib('o2')

    item_prefix = attr.ib(None)
    item_suffix = attr.ib(_('of respiration'))


def _roll_or_get(val):
        try:
            return val.roll()
        except AttributeError:
            return val
