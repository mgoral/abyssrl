# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

'''Definitions of functions used for gettext's translation strings.'''

import os
import gettext

import abyssrl.version as version

domain = version.__appname__.lower()  # abyssrl

localedir = os.path.join(os.path.sep, "usr", "share", "locale")
gettext.bindtextdomain(domain, localedir)
gettext.textdomain(domain)

t = gettext.translation(
    domain=domain,
    localedir=localedir,
    fallback=True)

_ = t.gettext
P_ = t.ngettext
