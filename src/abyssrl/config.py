# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

# Copyright (C) 2017 Michał Góral.
#
# Feed Commas is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Feed Commas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Feed Commas. If not, see <http://www.gnu.org/licenses/>.

import os
import configparser

from collections import OrderedDict

import mgcomm.xdg

import abyssrl.version as version


_cfg = None
_path = None

_defaults = OrderedDict((
    ('video', OrderedDict((
        ('width', 800),
        ('height', 600),
        ('fullscreen', False),
    ))),

    # TODO: analyze, sort and re-add with modifiers.
    # e.g. alt-ctrl-a should translate to "CTRL-META-a"
    ('keys', OrderedDict((
        ('k', 'move-n'),
        ('j', 'move-s'),
        ('h', 'move-w'),
        ('l', 'move-e'),
        ('u', 'move-ne'),
        ('y', 'move-nw'),
        ('n', 'move-se'),
        ('b', 'move-sw'),

        # alternative keypad movement
        ('keypad8', 'move-n'),
        ('keypad2', 'move-s'),
        ('keypad4', 'move-w'),
        ('keypad6', 'move-e'),
        ('keypad9', 'move-ne'),
        ('keypad7', 'move-nw'),
        ('keypad3', 'move-se'),
        ('keypad1', 'move-sw'),

        # camera movement
        ('w', 'move-camera-n'),
        ('s', 'move-camera-s'),
        ('a', 'move-camera-w'),
        ('d', 'move-camera-e'),

        (',', 'interact'),
        ('i', 'inventory'),

        ('c', 'statistics'),

        ('t', 'target-hostile'),

        ('.', 'wait'),
        ('keypad5', 'wait'),

        ('enter', 'activate'),
        ('return', 'activate'),
        ('escape', 'cancel'),

        ('v', 'view'),

        ('tab', 'next'),
        ('s-tab', 'previous'),

        ('s-~', 'toggle-debug'),
    ))),

    ('dev', OrderedDict((
        ('debug', False),
        ('godmode', False),
    ))),
))


class _NoDefault:
    pass
_NoDefaultInstance = _NoDefault()


class _ConfigParser(configparser.ConfigParser):  # pylint: disable=too-many-ancestors
    '''ConfigParser subclass which tracks options modifications.'''
    def __init__(self, *args, **kwargs):
        self._default_data = _defaults
        self._modified = False
        super().__init__(*args, **kwargs)

    @property
    def modified(self):
        '''Tells whether current configuration was modified since last write.'''
        return self._modified

    def set(self, *args, **kwargs):
        '''Re-implementation of ConfigParser.set'''
        super().set(*args, **kwargs)
        self._modified = True

    def read(self, *args, **kwargs):
        '''Re-implementation of ConfigParser.read'''
        super().read(*args, **kwargs)
        self._remove_unused()
        self._populate()

    def _remove_unused(self):
        def _commit(argslist, func):
            for args in argslist:
                func(*args)

        remove_section, remove_option = [], []
        for section, opts in self.items():
            if section in self._default_data:
                for option_name in opts:
                    if option_name not in self._default_data[section]:
                        remove_option.append((section, option_name))
            else:
                remove_section.append((section, ))
        _commit(remove_option, self.remove_option)
        _commit(remove_section, self.remove_section)

    def _populate(self):
        # this preserves the order of configuration file
        for section, opts in self._default_data.items():
            if not self.has_section(section):
                self.add_section(section)
            for option_name, value in opts.items():
                if not self.has_option(section, option_name):
                    self.set(section, option_name, str(value))

    def write(self, *args, **kwargs):
        '''Re-implementation of ConfigParser.write'''
        super().write(*args, **kwargs)
        self._modified = False


def path():
    '''Returns a path to the configuration file, which is searched in a way
    specified by XDG Base Directory Specification'''
    if _path:
        return _path

    app_name = version.__appname__.lower()
    cfg_name = 'config.ini'
    return mgcomm.xdg.basedir('config', subdir=app_name, file=cfg_name)


def config():
    '''Returns a global (shared) configuration object. If it hasn't been created
    yet, it is read from the mix of default values and configuration file.'''
    global _cfg  # pylint: disable=global-statement
    if _cfg is None:
        _cfg = _ConfigParser()
        _cfg.read(path())
    return _cfg


def exists():
    '''Returns whether there was found any configuration file.'''
    return os.path.exists(path())


def get(spec, default=_NoDefaultInstance):
    '''Properly returns a value of the field, even if it isn't set or is set to
    an empty value. Uses a global configuration and field name is given as a
    string in form "section_name.field_name".'''
    section_name, _, field_name = spec.partition('.')

    assert section_name
    assert field_name

    if default is _NoDefaultInstance:
        default = _defaults[section_name][field_name]

    section = config()[section_name]

    try:
        if isinstance(default, bool):
            return section.getboolean(field_name, fallback=default)
        elif isinstance(default, int):
            return section.getint(field_name, fallback=default)
        elif isinstance(default, float):
            return section.getfloat(field_name, fallback=default)
    except ValueError:
        pass

    ret = section.get(field_name, fallback=default)
    if not ret:  # correct; handles a case when ret is e.g. empty string
        ret = default
    return type(default)(ret)  # cast to the correct type


def set(spec, value):
    '''Sets configuration value for a given `spec`. `spec` is given as a string
    in form "section_name.field_name".'''
    section_name, _, field_name = spec.partition('.')

    assert section_name
    assert field_name

    # Allows adding arbitrary settings, without the need for updating _defaults
    # over and over again. Useful e.g. for keys section where only default keys
    # are mapped and user is free to set any keys he/she wants.
    _defaults[section_name].setdefault(field_name, value)

    assert isinstance(value, type(_defaults[section_name][field_name]))

    section = config()[section_name]
    section[field_name] = str(value)


def godmode():
    '''A special function which can be used almost everywhere to check whether
    we're in a special dev mode'''
    return get('dev.godmode')


def debug():
    '''A special function which can be used almost everywhere to check whether
    we're in a special dev mode'''
    return get('dev.debug')
