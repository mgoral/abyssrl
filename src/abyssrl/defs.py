# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.


'''Various definitions (constants, enums etc.) used here and there, placed in a
one file for import simplicity.'''


import enum
import attr

from abyssrl.locale import _


DUAL_WIELD_PENALTY = -2


class Rarity(enum.IntEnum):
    common = 0
    uncommon = 55
    rare = 85
    unique = 95
    legendary = 100 

    PAST_THE_END = 101


class ItemKind(enum.Enum):
    money = 0
    weapon = 1
    shield = 2
    armor = 3
    trousers = 4
    boots = 5

    potion = 50


class ItemTarget(enum.IntEnum):
    # starts for 1 for simple boolean comparisons
    owner = 1,
    enemy = 2
    position = 3


class ItemSlots(enum.IntEnum):
    # Values of slots are used also as array indices, so make sure that they're
    # consecutive numbers!
    head = 0
    main_hand = 1
    off_hand = 2
    armor = 3
    trousers = 4
    boots = 5

    @staticmethod
    def names():
        return [
            _('Head'),
            _('Main Hand'),
            _('Off Hand'),
            _('Armor'),
            _('Trousers'),
            _('Boots'),
        ]

    @staticmethod
    def abbrevs():
        return [
            # Translators: Head abbrev
            _('H'),

            # Translators: Main Hand abbrev
            _('MH'),

            # Translators: Off Hand abbrev
            _('OH'),

            # Translators: Armor abbrev
            _('A'),

            # Translators: Trousers abbrev
            _('T'),

            # Translators: Boots abbrev
            _('B'),
        ]
