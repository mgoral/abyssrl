# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import attr

import abyssrl.screen as screen
import abyssrl.api as api
from abyssrl.ecs.components import Position
from abyssrl.ecs.system import System, Node
import abyssrl.version as version
from abyssrl.utils import GameError


@attr.s(slots=True)
class ActionNode(Node):
    pos = attr.ib(validator=Node.not_none)
    props = attr.ib(validator=Node.not_none)
    actions = attr.ib(validator=Node.not_none)

    # not mandatory
    stats = attr.ib()
    actor = attr.ib()

@attr.s
class Actions(System):
    def make_node(self, obj):
        try:
            return ActionNode(obj.id, obj.pos, obj.props, obj.actions,
                obj.stats, obj.ai or obj.pc)
        except ValueError as e:
            return None

    def _handle(self, node):
        node.actions.cooldown = max(0, node.actions.cooldown - 1)
        if node.actions.cooldown > 0:
            return

        if not node.actions.queue:
            return

        success = False
        while node.actions.queue and success is False:
            action = node.actions.queue[0]
            success = action(node)

            if action.repeat <= 0 or not success:
                node.actions.queue.popleft()
                if api.is_pc(node):
                    screen.autopause_unlock()

            if action.repeat > 0:
                action.repeat -= 1  # works, obviously, also for math.inf
