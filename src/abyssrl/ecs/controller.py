# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import time
import decimal
import contextlib

import attr

import abyssrl.screen as screen
import abyssrl.signals as signals
import abyssrl.utils as utils
import abyssrl.api as api
from abyssrl.ecs.actions import Actions
from abyssrl.ecs.render import Renderer
from abyssrl.ecs.input import Input
from abyssrl.ecs.map import Player, LevelAdmin
from abyssrl.ecs.script_executor import ScriptExecutor
from abyssrl.ecs.effres import EffectsResolution
from abyssrl.ecs.entity import Entity

class Controller:
    def __init__(self, world):
        self.world = world
        self.current_event = None
        self.meas = utils.Measurements()
        self._stopped = False
        self._turn = 0

        self.object_added = signals.signal('object_added')
        self.object_removed = signals.signal('object_removed')
        self.component_added = signals.signal('component_added')
        self.component_removed = signals.signal('component_removed')
        self.turn_changed = signals.signal('turn_changed')
        self.stopped = signals.signal('stopped')

        # The order is important - systems will be called during update() in
        # this order.
        self.systems = [
            Input(self),
            ScriptExecutor(self),
            Actions(self),
            EffectsResolution(self),
            Player(self),

            LevelAdmin(self),
            Renderer(self),
        ]
        self.system_updating = None
        self.objects = {}

        for s in self.systems:
            system_id = type(s).__name__.lower()
            setattr(self, system_id, s)

            self.object_added.connect(s.handle_object_added)
            self.object_removed.connect(s.handle_object_removed)
            self.turn_changed.connect(s.turn_passed)
            Entity.component_added.connect(s.handle_component_added)
            Entity.component_removed.connect(s.handle_component_removed)

        self.world.resetted.connect(self.player.world_reset)
        self.world.resetted.connect(self.renderer.needs_full_redraw)

        self.object_added.connect(self.world.handle_object_added)
        self.object_removed.connect(self.world.handle_object_removed)

        self.player.fov_changed.connect(self.renderer.fov_changed)
        self.player.fov_changed.connect(self.scriptexecutor.fov_changed)
        self.player.player_moved.connect(self.renderer.player_moved)

    def add_object(self, obj):
        self.objects[obj.id] = obj
        self.object_added.emit(obj)

    def remove_object(self, obj):
        try:
            del self.objects[obj.id]
        except KeyError:
            pass
        else:
            self.object_removed.emit(obj)

    def update(self):
        if self._stopped:
            return

        with self.meas.measure():
            for s in self.systems:
                self.system_updating = s
                s.update()
            self.system_updating = None

    def stop(self):
        '''Anybody can call stop to stop controller after current loop. We could
        stop it immediately, but we'll probably want renderer to do one last
        update.'''
        self._stopped = True
        self.stopped.emit()

    @property
    def turn(self):
        return self._turn

    @turn.setter
    def turn(self, val):
        self._turn = val
        self.turn_changed.emit(self._turn)

    def next_turn(self):
        self.turn += 1

    def clear(self):
        for id_ in self.objects:
            obj = self.objects[id_]
            self.object_removed.emit(obj)
        self.objects.clear()
