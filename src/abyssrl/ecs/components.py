# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import collections
import weakref
import decimal
import attr

import abyssrl.colors as colors
import abyssrl.signals as signals
import abyssrl.api as api

from abyssrl.defs import ItemSlots
from abyssrl.utils import Stack
from abyssrl.locale import _

@attr.s(slots=True)
class Position:
    x = attr.ib(converter=int)
    y = attr.ib(converter=int)

    def __add__(self, other):
        return Position(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Position(self.x - other.x, self.y - other.y)


@attr.s(slots=True)
class Actions:
    queue = attr.ib(attr.Factory(collections.deque))
    cooldown = attr.ib(0)


@attr.s(slots=True)
class Icon:
    icon = attr.ib(' ')
    fg = attr.ib(colors.white)
    bg = attr.ib(None)

    def shade(self, ratio):
        ic = Icon(self.icon, colors.shade(self.fg, ratio))
        if self.bg:
            ic.bg = colors.shade(self.bg, ratio)
        return ic

    def greyscale(self):
        ic = Icon(self.icon, colors.greyscale(self.fg))
        if self.bg:
            ic.bg = colors.greyscale(self.bg)
        return ic



@attr.s(slots=True)
class Description:
    name = attr.ib(None)
    descr = attr.ib()
    faction = attr.ib(None)  # TODO: should this be translatable?

    prefix = attr.ib('')
    suffix = attr.ib('')

    @descr.default
    def _default_descr(self):
        return _('It is [c=%(fg)s]%(fullname)s[/c].')

    def translate(self):
        if self.name:
            self.name = _(self.name)
        if self.prefix:
            self.prefix = _(self.prefix)
        if self.suffix:
            self.suffix = _(self.suffix)
        if self.descr:
            self.descr = _(self.descr)

    @property
    def fullname(self):
        fn = '%s %s %s' % (self.prefix, self.name, self.suffix)
        return fn.strip()


@attr.s(slots=True)
class Properties:
    transparent = attr.ib(True)
    state = attr.ib(None)

    # either a move cost of a map feature (in turns) or a movement modifier for
    # slow actors (total_cost = actor.move_cost * feature.move_cost)
    move_cost = attr.ib(1)

    # destroyable entities must be destroyed first before their cell can be
    # taken. So they're opaque for pathfinding algorithms, but they're not for
    # actual action handling (accessing such cell will typically take several
    # turns)
    destroyable = attr.ib(False)

    # some features are not destroyable, but can be triggered anyway. Think
    # about stairs. To use a feature, actor has to stand on it.
    usable = attr.ib(False)

    # permanent obstacles.
    obstacle = attr.ib(False)


@attr.s(slots=True)
class Effects:
    active = attr.ib(attr.Factory(dict))
    pending = attr.ib(attr.Factory(Stack))


@attr.s(slots=True)
class Inventory:
    slots = attr.ib(init=False)
    backpack = attr.ib(attr.Factory(list))
    money = attr.ib(0)

    weight = attr.ib(0, init=False)

    # manipulating this constant changes movement cost
    _slot_volume = attr.ib(decimal.Decimal(1.4))

    def __attrs_post_init__(self):
        for item in self.backpack:
            self.weight += item.item.weight
        for item in self.slots:
            if item:
                self.weight += item.item.weight

    @slots.default
    def _default_slots(self):
        return [None for i in range(len(ItemSlots))]

    def equipped(self, slot):
        return bool(self.slots[slot.value])

    def force(self, prec=2):
        '''Underwater we have to keep buoyancy in mind. So here's a little
        reminder from physics class:

            Buyonancy (B) is the weight of the fluid displaced by the object.

        In other words, it's fluid's volume * density.

        The total downwards force influencing an object is:

                    F = m * g - B

        We have weight of objects (gravitational force) stored as `self.weight`.
        We won't do any complex simulations to calculate objects volume -
        instead we'll assume that each inventory slot displaces a fixed volume
        of water. So the more items player has, the less downward force (which
        results in lesser movement cost), unless individual objects are quite
        heavy.

        Remember:

                    1 N = 102 g = 0.102 kg = 0.22 lbs
                    10 N = 1 kg
                    1 kg = 9.81 N
        '''
        decimal.getcontext().prec = prec

        g = decimal.Decimal(9.81)
        volume = self._slot_volume * len(self)
        gforce = self.weight
        buoyancy = g * volume

        # Disallow negative total force, resulting in floating
        return max(decimal.Decimal(0), gforce - buoyancy)

    def newtons(self, *a, **kw):
        return _('%s N') % self.force(*a, **kw)

    @property
    def equipped_weapons(self):
        weapons = []
        for s in (ItemSlots.main_hand, ItemSlots.off_hand):
            if self.slots[s]:
                weapons.append(self.slots[s])
        return weapons

    def __len__(self):
        empty_slots = self.slots.count(None)
        return len(self.backpack) + len(self.slots) - empty_slots


@attr.s(slots=True)
class Actor:
    inventory = attr.ib(attr.Factory(Inventory))


@attr.s(slots=True)
class Player(Actor):

    @attr.s(slots=True)
    class Highlight:
        pos  = attr.ib(None)
        icon = attr.ib(None)

    turns_since_attacked = attr.ib(0)
    highlight = attr.ib(None)
    camera_offset = attr.ib(Position(0, 0))
    target_id = attr.ib(None)


@attr.s(slots=True)
class AI(Actor):
    handled_turn = attr.ib(-1)
    path = attr.ib(attr.Factory(list))


@attr.s(slots=True)
class MapFeature:
    pass


@attr.s(slots=True, cmp=False)
class Item:
    _owner_id = attr.ib(None)

    kind = attr.ib(None)
    fits = attr.ib(attr.Factory(list))
    effects = attr.ib(attr.Factory(list))
    money = attr.ib(None)

    slot = attr.ib(None, init=False)

    # api functions will automatically discard this item when possible, when
    # there are no longer any effects to be activated
    autodiscard = attr.ib(False)

    weight = attr.ib(0)
    use_target = attr.ib(None)

    stackable = attr.ib(False)
    amount = attr.ib(1)

    autopick = attr.ib(False)

    def __attrs_post_init__(self):
        if self.money:
            try:
                # evaluate money in case it's initialized to a Dice
                self.money = self.money.roll()
            except AttributeError:
                pass

    @property
    def owner(self):
        return api.entity(self._owner_id)

    @owner.setter
    def owner(self, node):
        if node is None:
            self._owner_id = node
        else:
            self._owner_id = node.id

        for eff in self.effects:
            eff.owner = node

    def __eq__(self, other):
        if self.money and other.money:
            return self.money == other.money

        if len(self.effects) == len(other.effects):
            for i, lhs in enumerate(self.effects):
                rhs = other.effects[i]
                if lhs.name != rhs.name or lhs.level != rhs.level:
                    return False
            return True

        return False


@attr.s(slots=True)
class Scripts:
    created = attr.ib(lambda obj: None)
    destroyed = attr.ib(lambda obj: None)
    triggered = attr.ib(lambda obj, other: None)
    seen = attr.ib(lambda obj, other: None)
    every_turn = attr.ib(lambda obj: None)


@attr.s(slots=True)
class Randomize:
    '''A special component which, when present, causes entity randomization.
    Type of randomization will be determined by existance of other
    components.'''
    # 1 - 100, 1: common, 100: legendary
    rarity = attr.ib(None)
