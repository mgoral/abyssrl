# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

'''Stats is an ordinary component with some logic attached, because underlying
mechanics is a complicated beast to be honest. It's also the biggest one so it
deserves its own file.'''

import attr

from abyssrl.mechanics import *
from abyssrl.effect import DamageSetter

@attr.s(slots=True)
class Stats:
    dices = attr.ib()
    max_hp = attr.ib()
    hp = attr.ib()

    base_o2 = attr.ib(500)
    o2_bonus = attr.ib(0)
    o2 = attr.ib(500)

    base_strength = attr.ib(10) # strength
    base_dexterity = attr.ib(10) # dexterity
    base_mind = attr.ib(10) # mind

    base_accu = attr.ib(0)
    accu_bonus = attr.ib(0)

    base_ac = attr.ib(10)
    armor_bonus = attr.ib(0)

    base_dmg = attr.ib(Dice(1, 4, 0))
    dmg_bonus = attr.ib(0)

    level = attr.ib(1)

    view_rad = attr.ib(15) # FOV radius

    _distribuable_sdm = attr.ib(0, init=False)

    def __attrs_post_init__(self):
        if self.hp > self.max_hp:
            self.hp = self.max_hp

    @dices.default
    def default_dices(self):
        return [Dice(1,  6)]

    @max_hp.default
    def default_max_hp(self):
        return roll_dices(self.dices)

    @hp.default
    def default_hp(self):
        return self.max_hp

    @property
    def accuracy(self):
        return (self.base_accu + self.accu_bonus + self.level - 1 +
                self.dexterity_bonus)

    @property
    def ac(self):
        return self.base_ac + self.dexterity_bonus + self.armor_bonus

    @property
    def natural_damage(self):
        return DamageSetter(dmg=self.base_dmg)

    @property
    def next_level(self):
        return self.level * 10

    @property
    def strength(self):
        return self.base_strength

    @property
    def dexterity(self):
        return self.base_dexterity

    @property
    def mind(self):
        return self.base_mind

    @property
    def encounter_level(self):
        return sum(d.no for d in self.dices)

    @property
    def strg_bonus(self):
        return int((self.strength - 10) / 2)

    @property
    def dexterity_bonus(self):
        return int((self.dexterity - 10) / 2)

    @property
    def mind_bonus(self):
        return int((self.mind - 10) / 2)

    @property
    def max_o2(self):
        return self.base_o2 + self.o2_bonus

    def increase_threat(self, threat):
        self.level += threat
        self.dices += [self.dices[0]] * threat

        self.base_dmg += threat

        hp_prop = self.hp / self.max_hp
        self.max_hp = self.default_max_hp()
        self.hp = int(self.max_hp * hp_prop)
