# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import os
import logging
import math
import collections
import textwrap

import attr

import abyssrl.config as config
import abyssrl.api as api
import abyssrl.ecs.components as components
import abyssrl.colors as colors
import abyssrl.version as version
import abyssrl.screen as screen
from abyssrl.locale import _
from abyssrl.ecs.system import System, Node
from abyssrl.glyphs import tiles

log = logging.getLogger('%s.%s' % (version.__appname__, __name__))

_NO_ICON = components.Icon(' ', colors.black, colors.black)

class ConsoleHandler(logging.StreamHandler):
    '''Re-use standard Python logging module for logging into on-screen
    console.'''
    def __init__(self, renderer, *a, **kw):
        self.r = renderer
        super().__init__(*a, **kw)

    def emit(self, record):
        msg = self.format(record)
        self.r.message(msg, getattr(record, 'color', colors.white))


@attr.s
class Renderer(System):
    _layout = attr.ib(None)
    viewport = attr.ib(None)
    topbar = attr.ib(None)

    _lastcam = attr.ib(None, init=False)
    _fov_changed = attr.ib(False, init=False)
    _player_moved = attr.ib(False, init=False)
    _highlighted = attr.ib(False, init=False)

    # This isn't used by Renderer though as it implements its own update().
    # I leave it here so turn-agnostic systems are easily greppable and
    # distinguishable.
    turn_agnostic = attr.ib(True)

    @property
    def layout(self):
        return self._layout

    @layout.setter
    def layout(self, val):
        self._layout = val

        self.viewport = self.layout.find('viewport')
        self.console = self.layout.find('console')
        self.topbar = self.layout.find('topbar')

    def fov_changed(self):
        self._fov_changed = True

    def player_moved(self):
        self._player_moved = True

    def update(self):
        if not self.ctl.world.initialized:
            return

        world = self.ctl.world
        tl, br = self._camera()

        cam_moved = (tl != self._lastcam)

        # redraw black, unseen cells - seen cells will be drawn in a loop below
        if cam_moved:
            self.viewport.clear()

        self.viewport.offset = tl

        for x in range(tl.x, br.x):
            for y in range(tl.y, br.y):
                cell = world.at(x,y)
                visible = world.visible(x, y)

                if cam_moved or self._player_moved or self._fov_changed:
                    if visible:
                        cell.seen = True
                        self.draw_cell(cell, tl)

                    # redraw only on camera move and visibility change
                    elif cell.seen and (cam_moved or world.was_visible(x, y)):
                        self.draw_cell(cell, tl, shade=True)
                elif visible and cell.needs_update:
                    cell.seen = True
                    self.draw_cell(cell, tl)

        self._fov_changed = False
        self._player_moved = False
        self._lastcam = tl

        self.refresh_params()
        self.layout.draw()
        self.draw_highlight(tl, br)
        screen.refresh()

    def draw_highlight(self, offset, br):
        pc = self.ctl.player.pc
        hl = pc.pc.highlight
        vp = self.viewport

        t = self.ctl.objects.get(pc.pc.target_id)
        if t and self.ctl.world.visible(t.pos.x, t.pos.y):
            hl.pos = t.pos

        if hl:
            fw, fh = screen.font_spacing(self.viewport.font)
            ox = hl.pos.x - offset.x
            oy = hl.pos.y - offset.y
            x = min(max(0, ox), int(vp.width / fw) - 1)
            y = min(max(0, oy), int(vp.height / fh) - 1)

            # update stored position so it won't silently go out of a viewport
            # (which looks like unresponsive cursor)
            if ox != x or oy != y:
                hlx = hl.pos.x + x - ox
                hly = hl.pos.y + y - oy
                hl.pos = components.Position(hlx, hly)

            # decide cursor color
            world = self.ctl.world
            icon = components.Icon(hl.icon.icon, hl.icon.fg, hl.icon.bg)
            if api.hostile(pc, hl.pos) and world.visible(hl.pos.x, hl.pos.y):
                icon.fg = colors.red
            elif api.usable(hl.pos):
                icon.fg = colors.blue

            self.viewport.clear_highlights()
            self.viewport.highlight(icon, x, y)
            self._highlighted = True
        elif self._highlighted:
            self.viewport.clear_highlights()
            self._highlighted = False

    def refresh_params(self):
        pc = self.ctl.player.pc

        fmt = dict(
            hp_tile=tiles.t.vial,
            hp=pc.stats.hp,
            max_hp=pc.stats.max_hp,

            o2_tile=tiles.t.bulbs,
            o2=pc.stats.o2,
            max_o2=pc.stats.max_o2,

            weight_tile=tiles.t.weight,
            weight=pc.pc.inventory.newtons(),

            money_tile=tiles.t.money,
            money=pc.pc.inventory.money,

            stairs_down_tile=tiles.t.stairs_down,
            depth=self.ctl.world.properties.depth
        )

        text = _(
            '[font=tileset][c=red]{hp_tile:c}[/c][/font] {hp:}/{max_hp:} '
            '[font=tileset][c=blue]{o2_tile:c}[/c][/font] {o2:}/{max_o2:} '
            '[font=tileset]{weight_tile:c}[/font] {weight:} '
            '[font=tileset]{stairs_down_tile:c}[/font] {depth:} '
            '[font=tileset][c=gold]{money_tile:c}[/c][/font] {money:} '
        ).format(**fmt)

        if config.debug():
            dbg_fmt = dict(
                meas=str(self.ctl.meas),
                turn=str(self.ctl.turn),
            )
            text += '| {meas:} | {turn:}'.format(**dbg_fmt)

        self.topbar.text = text

    def message(self, msg, color=colors.white):
        self.console.add_message(msg)

    def draw_icon(self, icon, x, y):
        self.viewport.put_icon(icon, x, y)

    def draw_cell(self, cell, offset, shade=False):
        if not cell.terrain:
            return

        # optimization: substracting two objects is HEAVY and
        # this is a tight loop
        x = cell.pos.x - offset.x
        y = cell.pos.y - offset.y

        if cell.needs_update:
            self._update_icons(cell)
            cell.needs_update = False

        if shade:
            self.draw_icon(cell.shade_icon, x, y)
        else:
            self.draw_icon(cell.icon, x, y)

    def needs_full_redraw(self):
        # This will force redraw during the next update(). I'm too lazy to
        # introduce yet another flag just for this.
        self._lastcam = None

    def _update_icons(self, cell):
        if cell.actor:
            cell.icon = components.Icon(
                cell.actor.icon.icon,
                cell.actor.icon.fg,
                cell.actor.icon.bg or cell.terrain.bg)
        elif cell.feature:
            cell.icon = components.Icon(
                cell.feature.icon.icon,
                cell.feature.icon.fg,
                cell.feature.icon.bg or cell.terrain.bg)
        elif cell.objects:
            cell.icon = components.Icon(
                cell.objects[-1].icon.icon,
                cell.objects[-1].icon.fg,
                cell.objects[-1].icon.bg or cell.terrain.bg)
        else:
            cell.icon = components.Icon(
                cell.terrain.icon,
                cell.terrain.fg,
                cell.terrain.bg)

        # shade icon - draw e.g. walls, but not enemies
        if cell.feature:
            fi = cell.feature.icon
            cell.shade_icon = components.Icon(
                fi.icon,
                colors.shade(fi.fg, 0.5),
                colors.shade(fi.bg or cell.terrain.bg, 0.5))
        elif cell.objects:
            oi = cell.objects[-1].icon
            cell.shade_icon = components.Icon(
                oi.icon,
                colors.shade(oi.fg, 0.5),
                colors.shade(oi.bg or cell.terrain.bg, 0.5))
        else:
            cell.shade_icon = cell.terrain.shade(0.5)

    def _camera(self):
        pc = self.ctl.player.pc
        off = pc.pc.camera_offset

        fw, fh = screen.font_spacing(self.viewport.font)
        world = self.ctl.world
        center = pc.pos
        vp = components.Position(
            min(self.viewport.width / fw, world.width),
            min(self.viewport.height / fh, world.height))
        ms = components.Position(world.width, world.height)

        def _calc(c, size, m):
            hs = size / 2
            if c < hs:
                return 0
            elif c >= m - hs:
                return m - size
            else:
                return c - hs

        def _offset(coord, o, v, m):
            '''Returns correct delta by which tl should be moved and which is an
            offset fix'''
            if o == 0:
                return 0

            nc = max(min(coord + o, m - v), 0)
            return nc - coord


        tl = components.Position(
            _calc(center.x, vp.x, ms.x), _calc(center.y, vp.y, ms.y))

        delta_x = _offset(tl.x, off.x, vp.x, ms.x)
        delta_y = _offset(tl.y, off.y, vp.y, ms.y)

        tl.x += delta_x
        tl.y += delta_y
        off.x = delta_x
        off.y = delta_y

        br = tl + vp
        return tl, br
