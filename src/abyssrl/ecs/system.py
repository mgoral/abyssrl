# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import attr

'''This is a base for every system. Subclass it and implement _handle(node)
method.

The most important thing is update() function. It's REALLY discouraged to
re-implement it in a subclass, but if you do that, at least make sure to call
super().update() at some point. You can use `pre_update()` and `post_update()`
to customize its behaviour to some extent, but be very careful when changing its
body. Here's why...

Systems get updated about Entities changes continuously. Basically every
setattr() called on Entities component emits a signal and systems update their
node lists accordingly. But there's a catch: when system runs its update()
method, it is iterating over the list which is going to be updated. That's why
we introduce a set of clever hacks to prevent UB.

First of all, it's OK to update or even change element of list as long as len()
of list itself doesn't change. So we'll always perform component CHANGES
on-line. Adding and removing of nodes is more tricky. We'll store these
operations in a separate container from which we'll later simply load any last
requested state of a given entity. We don't want to use however removed entities
in any way (e.g. when refering to them from the outside). So we'll mark them as
dirty, skip them during current iteration and won't return them from
node-searching functions (they can still be accessed via `nodes` field). Further
update() will correctly skip these and querying them via a node() won't bring
any effect.

Queued changes are processed right after the list iteration.

So it boils down to:

    1. update views on-line whenever possible (when not in update() or not
       changing list's len())
    2. otherwise update right after nodes' iteration
    3. use only the latest state of entity
    4. be smart about adding/removing entries from a modification list
'''

@attr.s
class System:
    ctl = attr.ib()  # controller
    nodes = attr.ib(attr.Factory(list), init=False)
    _handled = attr.ib(attr.Factory(dict), init=False)  # {id : nodes index}
    _input_queue = attr.ib(attr.Factory(dict), init=False)  # {id: node}
    _updating = attr.ib(False, init=False)

    # how frequently system updates, redefine in concrete systems if needed
    turn_agnostic = attr.ib(False, init=False)
    _turn_passed = attr.ib(False, init=False)

    @property
    def updating(self):
        '''Tells whether system is currently being updated'''
        # TODO: blocks the whole update(), maybe block only iteration over
        # nodes?
        return self.ctl.system_updating is self

    def turn_passed(self, no):
        self._turn_passed = True

    def handle_object_added(self, obj):
        return self._sth_added(obj)


    def handle_object_removed(self, obj):
        idx = self._handled.get(obj.id)
        if idx is None:
            return
        return self._sth_removed(obj)

    def handle_component_added(self, obj, component_name):
        node = self.make_node(obj)
        if not node:
            return
        return self._sth_added(obj)

    def handle_component_removed(self, obj, component_name, val):
        return self._sth_removed(obj)

    def make_node(self, obj):
        '''Creates a node from an object. This node will be stored internally on
        node list. If object doesn't meet system's requirements, `make_node`
        should return None.'''
        return

    def node(self, id_):
        '''Fast access of a node with a given id_.'''
        try:
            node = self.nodes[self._handled[id_]]
            if not node.dirty:
                return node
        except (KeyError, IndexError):
            return None

    def real(self, node):
        '''Returns real Entity which node is a view for'''
        if node and not node.dirty:
            return self.ctl.objects[node.id]

    def update(self):
        '''Contains 'main loop' of a system. Gets called once every game
        frame.'''

        if not self._turn_passed and not self.turn_agnostic:
            return

        self.pre_update()
        for node in self.nodes:
            if not node.dirty:
                self._handle(node)
        self.handle_input_queue()
        self.post_update()

        self._turn_passed = False

    def pre_update(self):
        '''A function called directly before update loop.'''
        pass

    def post_update(self):
        '''A function called directly after update loop, but before executing
        component changes.'''
        pass

    def handle_input_queue(self):
        '''Does the bookkeeping of the nodes list.'''
        for id_ in self._input_queue:
            node = self._input_queue[id_]
            if node is None:
                self._remove_node(id_)
            else:
                self._add_node(node)
        self._input_queue.clear()

    def _handle(self, node):
        '''This is where the real work is done - it should be reimplemented by a
        subclass'''
        return

    def _remove_node(self, id_):
        idx = self._handled.get(id_)
        if idx is not None:
            del self.nodes[idx]
            del self._handled[id_]

            # re-assign indices
            for node in self.nodes[idx:]:
                self._handled[node.id] -= 1

    def _add_node(self, node):
        idx = self._handled.get(node.id)
        if idx is not None:
            self.nodes[idx] = node
        else:
            self._handled[node.id] = len(self.nodes)
            self.nodes.append(node)

    def _sth_added(self, obj):
        # FIXME: node is constructed second time here when this function is
        # called from component_added. Fix e.g. by passing a node here from
        # object_added or by passing the whole Entity from component_added.
        node = self.make_node(obj)
        if not node:
            return

        try:
            del self._input_queue[node.id]
        except KeyError:
            pass

        idx = self._handled.get(node.id)
        if idx is not None:
            self.nodes[idx] = node
        elif self.updating:
            self._input_queue[node.id] = node
        else:
            self._add_node(node)
        return node

    def _sth_removed(self, obj):
        idx = self._handled.get(obj.id)
        if idx is None:
            return

        node = self.nodes[idx]

        try:
            del self._input_queue[node.id]
        except KeyError:
            pass

        if self.updating:
            node.dirty = True
            self._input_queue[node.id] = None
        else:
            self._remove_node(node.id)
        return node


@attr.s(slots=True)
class Node:
    id = attr.ib()
    dirty = attr.ib(False, init=False)

    @staticmethod
    def not_none(instance, attrib, val):
        if val is None:
            raise ValueError('%s not initialized' % attrib)

    @classmethod
    def has_attr(cls, name):
        for attr in cls.__attrs_attrs__:
            if attr.name == name:
                return True
        return False
