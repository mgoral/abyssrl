# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import math
from enum import IntEnum
import functools
import attr
import logging

from abyssrl.ecs.system import System, Node
from abyssrl.ecs.components import Player, Position, Icon
from abyssrl.ecs.entity import dexpand
import abyssrl.screen as screen
import abyssrl.config as config
import abyssrl.signals as signals
import abyssrl.actions as actions
import abyssrl.api as api
import abyssrl.colors as colors
import abyssrl.gui.dialogs as dialogs
import abyssrl.gui.inventory as inv
import abyssrl.gui.character as character
import abyssrl.version as version

from abyssrl.glyphs import tiles
from abyssrl.locale import _

log = logging.getLogger('%s.%s' % (version.__appname__, __name__))

@attr.s(slots=True)
class InputNode(Node):
    pos = attr.ib(validator=Node.not_none)
    actions = attr.ib(validator=Node.not_none)
    pc = attr.ib(validator=Node.not_none)

class ControlMode(IntEnum):
    game = 0
    view = 1

@attr.s
class Input(System):
    turn_agnostic = attr.ib(True)

    _sm = attr.ib(None)
    _actions = attr.ib(None)
    _modes = attr.ib(None)
    _next_action = attr.ib(None)
    _mode = attr.ib(ControlMode.game)
    _keys = config.config()['keys']
    _record_mouse_pos = attr.ib(None)

    def __attrs_post_init__(self):
        self._actions = list(range(len(ControlMode.__members__)))

        # Game Mode
        self._actions[ControlMode.game] = {
            'cancel': self._reset,

            'move-n': functools.partial(self._move, 0, -1),
            'move-s': functools.partial(self._move, 0, 1),
            'move-w': functools.partial(self._move, -1, 0),
            'move-e': functools.partial(self._move, 1, 0),
            'move-ne': functools.partial(self._move, 1, -1),
            'move-nw': functools.partial(self._move, -1, -1),
            'move-se': functools.partial(self._move, 1, 1),
            'move-sw': functools.partial(self._move, -1, 1),

            'move-camera-n': functools.partial(self._move_camera_offset, 0, -1),
            'move-camera-s': functools.partial(self._move_camera_offset, 0, 1),
            'move-camera-w': functools.partial(self._move_camera_offset, -1, 0),
            'move-camera-e': functools.partial(self._move_camera_offset, 1, 0),

            'mouse-left-released': self._interact,
            'mouse-move': self._move_mouse,
            'mouse-right': self._record_mouse,
            'mouse-right-released': self._stop_record_mouse,

            'interact': self._interact_kb,
            'inventory': self._inventory,

            'statistics': self._show_stats,

            'wait': self._wait,

            'target-hostile': self._target_hostile,
            'view': functools.partial(self._set_mode, ControlMode.view),

            'toggle-debug': self._toggle_debug,
        }

        # View Mode
        self._actions[ControlMode.view] = \
            self._actions[ControlMode.game].copy()
        self._actions[ControlMode.view].update({
            'move-n': functools.partial(self._move_cursor, 0, -1),
            'move-s': functools.partial(self._move_cursor, 0, 1),
            'move-w': functools.partial(self._move_cursor, -1, 0),
            'move-e': functools.partial(self._move_cursor, 1, 0),
            'move-ne': functools.partial(self._move_cursor, 1, -1),
            'move-nw': functools.partial(self._move_cursor, -1, -1),
            'move-se': functools.partial(self._move_cursor, 1, 1),
            'move-sw': functools.partial(self._move_cursor, -1, 1),
            'activate': self._describe,
            'mouse-left-released': self._describe_m,
            'cancel': functools.partial(self._set_mode, ControlMode.game),
            'view': functools.partial(self._set_mode, ControlMode.game),
        })
        for k in ('interact', 'inventory', 'statistics', 'wait'):
            del self._actions[ControlMode.view][k]

    def handle_key(self, key):
        command = self._keys.get(key)

        action = self._actions[self._mode].get(command)
        if callable(action):
            self._next_action = action
            return True
        return False

    def handle_mouse(self, key, x, y):
        action = self._actions[self._mode].get(key)
        if callable(action):
            self._next_action = functools.partial(action, x, y)
            return True
        return False

    def make_node(self, obj):
        try:
            return InputNode(obj.id, obj.pos, obj.actions, obj.pc)
        except ValueError:
            return None

    def _handle(self, node):
        if self._needs_attention(node):
            self._clear_actions(node)

        if not node.pc.highlight or not node.pc.highlight.pos:
            self._set_mode(self._mode, node)

        if self._next_action is None:
            if node.actions.queue:
                self.ctl.turn += 1
            return

        action = self._next_action
        self._next_action = None

        return action(node)

    def _new_action(self, node, action):
        node.pc.camera_offset = Position(0, 0)
        self._clear_actions(node)
        self._queue_action(node, action)

    def _queue_action(self, node, action):
        if not node.actions.queue:
            self.ctl.turn += 1

        node.actions.queue.append(action)
        screen.autopause_lock()

    def _clear_actions(self, node):
        screen.autopause_unlock(len(node.actions.queue))
        node.actions.queue.clear()

    def _reset(self, node):
        node.pc.camera_offset = Position(0, 0)
        node.pc.target_id = None

        self._set_mode(ControlMode.game, node)
        self._clear_actions(node)

    def _move(self, x, y, node):
        newpos = Position(node.pos.x + x, node.pos.y + y)

        move_and_autopick_or_trigger = actions.If(
            actions.Move(newpos), actions.AutoPick(newpos),
            actions.Trigger(newpos))
        self._new_action(node, move_and_autopick_or_trigger)

    def _move_camera_offset(self, x, y, node):
        node.pc.camera_offset.x += x
        node.pc.camera_offset.y += y

    def _interact(self, x, y, node):
        newpos = Position(x, y)

        cell = self.ctl.world.atpos(newpos)
        if not cell.seen:
            return

        if newpos == node.pos:
            self._interact_own_cell(node, cell)
        else:
            self._interact_other_cell(node, cell)

    def _interact_own_cell(self, node, cell):
        '''Pick items or just wait.'''
        if not cell.feature and not cell.objects:
            self._wait(node)
        else:
            self._interact_kb(node)

    def _interact_other_cell(self, node, cell):
        '''Move to the new cell and trigger it if it's triggerable'''
        if cell.actor:
            self._new_action(node, actions.AttackMelee(cell.actor.id, repeat=0))
        else:
            move_and_autopick = actions.ForEach(
                [actions.MoveOnPath(cell.pos), actions.AutoPick()],
                repeat=math.inf)
            self._new_action(node, move_and_autopick)

            if api.triggerable(cell.pos):
                self._queue_action(node, actions.Trigger(cell.pos))


    def _interact_kb(self, node):
        if api.usable(node.pos):
            self._new_action(node, actions.UseCell(node.pos))
            return True

        objects = self.ctl.world.atpos(node.pos).objects
        if objects:
            dialog = dialogs.Picker(objects, title=_('Pick items'))
            dialog.run()
            if dialog.selected:
                picks = [
                    actions.And(
                        actions.Pick(obj),
                        actions.Equip(obj, replace=False))
                    for obj in dialog.selected
                ]
                self._new_action(node, actions.ForEach(picks))
            return True
        return False

    def _inventory(self, node):
        dialog = inv.InventoryView(self.real(node))
        dialog.run()
        if dialog.action:
            self._new_action(node, dialog.action)
        elif dialog.took_turn:
            self.ctl.turn += 1

    def _show_stats(self, node):
        dialog = character.CharacterView(self.real(node))
        dialog.run()

    def _wait(self, node):
        self._new_action(node, actions.Wait())

    def _move_mouse(self, x, y, node):
        pos = Position(x, y)
        if self._record_mouse_pos:
            delta = self._record_mouse_pos - pos
            node.pc.camera_offset += delta
        else:
            node.pc.highlight.pos = pos

    def _record_mouse(self, x, y, node):
        self._record_mouse_pos = Position(x, y)

    def _stop_record_mouse(self, x, y, node):
        self._record_mouse_pos = None


    def _move_cursor(self, dx, dy, node):
        node.pc.highlight.pos += Position(dx, dy)

    def _toggle_debug(self, node):
        now = config.get('dev.debug')
        new = not now
        config.set('dev.debug', new)

        s = 'on' if new else 'off'
        log.info('Debug mode %s' %s)

        glog = logging.getLogger(version.__appname__)
        if config.debug():
            glog.setLevel(logging.getLevelName('DEBUG'))
        else:
            glog.setLevel(logging.getLevelName('INFO'))

    def _needs_attention(self, node):
        '''Tells whether player's attention is needed (i.e. whether action queue
        should be cleared because of the new situation).'''
        if not node.actions.queue:
            return False

        node = self.real(node)
        if node.stats.hp < node.stats.max_hp * 0.1:
            return True

        if node.stats.o2 < node.stats.max_o2 * 0.1:
            return True

        for actor in api.iter_hostiles(node):
            return True

        return False

    def _set_mode(self, m, node):
        self._mode = m

        if m is ControlMode.game:
            cursor = Icon(tiles.t.cursor, colors.green)
        elif m is ControlMode.view:
            cursor = Icon(tiles.t.cursor_view, colors.grey)

        if not node.pc.highlight or not node.pc.highlight.pos:
            node.pc.highlight = node.pc.Highlight(node.pos, cursor)
        else:
            node.pc.highlight.icon = cursor

    def _target_hostile(self, node):
        hostiles = list(api.iter_hostiles(self.real(node)))
        if not hostiles:
            node.pc.target_id = None
            return

        if node.pc.target_id is None:
            node.pc.target_id = hostiles[0].id
        else:
            # t might be None. We don't care as hostiles list won't contain
            # None, so it doesn't need any special handling
            t = self.ctl.objects.get(node.pc.target_id)

            try:
                i = (hostiles.index(t) + 1) % len(hostiles)
                node.pc.target_id = hostiles[i].id
            except ValueError:
                node.pc.target_id = hostiles[0].id

    def _describe(self, node):
        # Currently it's only available in view mode
        world = self.ctl.world
        pos = node.pc.highlight.pos
        cell = world.atpos(pos)

        def _log(descr, fg, **fmt):
            if fg:
                fmt['fg'] = colors.name(fg)
            else:
                fmt['fg'] = colors.name(colors.white)

            log.info(descr % fmt)

        def _log_debug(obj):
            if not config.get('dev.debug'):
                return
            log.info('id=[c=red]%d[/c], otid=[c=yellow]%s[/c], '
                     'pos=[c=green]%s[/c]' % \
                (obj.id, obj.otid, (obj.pos.x, obj.pos.y)))

        def _fg_of(obj):
            if obj.icon:
                return obj.icon.fg
            return None

        if not cell.seen:
            return log.info(_("You haven't explored this place yet."))
        else:
            visible = world.visible(pos.x, pos.y)
            if visible and cell.actor:
                log.info(dexpand(cell.actor))
                _log_debug(cell.actor)
            elif len(cell.objects) > 1:
                log.info(_('A [c=yellow]pile of items[/c] lays on a ground.'))
            elif len(cell.objects) == 1:
                log.info(dexpand(cell.objects[0]))
                _log_debug(cell.objects[0])
            elif cell.feature:
                log.info(dexpand(cell.feature))
                _log_debug(cell.feature)

    def _describe_m(self, x, y, node):
        return self._describe(node)

