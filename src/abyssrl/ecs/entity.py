# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import copy

import attr

import abyssrl.colors as colors
import abyssrl.mechanics as mech

from abyssrl.ecs.components import *
from abyssrl.ecs.stats import Stats
from abyssrl.utils import cast
import abyssrl.generators as gen

def _gen_id():
    i = -1
    def _f():
        nonlocal i
        i += 1
        return i
    return _f


class _Notified:
    '''Descriptor of fields which should automatically notify some of their
    changes.'''
    def __init__(self, name, converter):
        self.name = name
        self.real = '_' + name
        self.conv = converter

    def __get__(self, obj, objtype):
        return getattr(obj, self.real)

    def __set__(self, obj, val):
        curr = getattr(obj, self.real)
        setattr(obj, self.real, self.conv(val))

        if val is None and curr is not None:
            obj.component_removed.emit(obj, self.name, curr)
        elif val != curr:
            obj.component_added.emit(obj, self.name)


def component(type_, **kw):
    kw.setdefault('metadata', {})['component'] = True
    kw.setdefault('converter', cast(type_))
    kw.setdefault('cmp', False)
    return attr.ib(None, **kw)


def component_descriptors(cls):
    for a in cls.__attrs_attrs__:
        if a.metadata and a.metadata.get('component'):
            dname = a.name.lstrip('_')
            setattr(cls, dname, _Notified(dname, a.converter))
    return cls


@component_descriptors
@attr.s(slots=True)
class Entity:
    otid = attr.ib()  # Object Type ID, like 'orc', 'player', 'wall' etc.
    id = attr.ib(attr.Factory(_gen_id()))

    # components
    _descr = component(Description)
    _icon = component(Icon)
    _pos = component(Position)
    _actions = component(Actions)
    _props = component(Properties)
    _stats = component(Stats)
    _effects = component(Effects)

    _pc = component(Player)
    _ai = component(AI)
    _mapfeature = component(MapFeature)
    _item = component(Item)
    _scripts = component(Scripts)

    _randomize = component(Randomize)

    # signals, class-wide
    component_added = signals.signal('component_added')
    component_removed = signals.signal('component_removed')

    def __attrs_post_init__(self):
        r = self.randomize
        if r:
            if r.rarity is None:
                r.rarity = mech.roll(1, 100)

            if self.item:
                gen.generate_effects(self)

        # prevent any further randomizations, even in case of explicit copy of
        # this entity
        self.randomize = None

    def update(self, comp_name, dct):
        comp = getattr(self, comp_name)
        for key in dct:
            setattr(comp, key, dct[key])

    @classmethod
    def FromJson(cls, dct):
        otid = dct['otid']
        comps = dct['components']
        e = cls(otid, **comps)
        e.translate()
        return e

    def translate(self):
        if self.descr:
            self.descr.translate()


def dexpand(e, color=True):
    '''Expands description of entity.'''
    t = e.descr.descr

    fmt = {
        'name': e.descr.name,
        'fullname': e.descr.fullname,
    }

    if color and e.icon and e.icon.fg:
        fmt['fg'] = colors.name(e.icon.fg)
    else:
        fmt['fg'] = ''

    return t % fmt
