# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import attr

from abyssrl.ecs.system import System, Node


@attr.s(slots=True)
class ScriptedNode(Node):
    pos = attr.ib(validator=Node.not_none)
    scripts = attr.ib(validator=Node.not_none)

    # Not mandatory, but will be set when they're available.
    # Generally scripts vary per-object-type so when you're creating an object,
    # you have to make sure that attached scripts will only use available
    # components.
    descr = attr.ib()
    props = attr.ib()
    stats = attr.ib()
    icon = attr.ib()
    actions = attr.ib()
    effects = attr.ib()
    pc = attr.ib()
    ai = attr.ib()
    item = attr.ib()


@attr.s
class ScriptExecutor(System):
    _seen_by = attr.ib(attr.Factory(dict), init=False)
    _visible_cells = attr.ib(attr.Factory(list), init=False)
    _fov_changed = attr.ib(False, init=False)

    def make_node(self, obj):
        try:
            return ScriptedNode(
                obj.id, obj.pos, obj.scripts, obj.descr, obj.props, obj.stats,
                obj.icon, obj.actions, obj.effects, obj.pc, obj.ai, obj.item)
        except ValueError as e:
            return None

    def handle_object_added(self, obj):
        node = super().handle_object_added(obj)
        if node:
            self.trigger_created(node)
            node.scripts.created = None  # TODO: set to default instead?
        return node

    def handle_object_removed(self, obj):
        node = super().handle_object_removed(obj)
        if node:
            self.trigger_destroyed(node)
            node.scripts.destroyed = None  # TODO: set to default instead?
        return node

    def fov_changed(self):
        self._fov_changed = True

    def pre_update(self):
        if self._fov_changed:
            self._visible_cells = list(
                c.pos for c in self.ctl.world.visible_cells())

    def _handle(self, node):
        if node.pos in self._visible_cells:
            self.trigger_seen(node, self.ctl.player.pc)

        # called once a turn, thanks to the default turn_agnostic set to true
        self._trigger_every_turn(node)

    def post_update(self):
        self._fov_changed = False

    def trigger_seen_cell(self, cell, by_who):
        all_objs = cell.objects[:]
        if cell.feature:
            all_objs.append(cell.feature)
        if cell.actor:
            all_objs.append(cell.actor)

        for obj in all_objs:
            self.trigger_seen(obj, by_who)

    def trigger_seen(self, obj, by_who):
        seen_by = self._seen_by.setdefault(obj.id, set())
        if by_who.id in seen_by:
            return

        lhs = self.node(obj.id)
        rhs = self.node(by_who.id)

        if lhs and rhs:
            seen_by.add(by_who.id)
            lhs.scripts.seen(lhs, rhs)

    def trigger_triggered(self, triggered, by_who):
        lhs = self.node(triggered.id)
        rhs = self.node(by_who.id)

        if lhs and rhs:
            lhs.scripts.triggered(lhs, rhs)

    def trigger_created(self, obj):
        if obj.scripts.created:
            obj.scripts.created(obj)

    def trigger_destroyed(self, obj):
        if obj.scripts.destroyed:
            obj.scripts.destroyed(obj)

    def _trigger_every_turn(self, obj):
        obj.scripts.every_turn(obj)
