# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import attr

import abyssrl.effect as effect

from abyssrl.ecs.system import System, Node

_auto_effects = [
    effect.DualWield(),
]

@attr.s(slots=True)
class EffectsNode(Node):
    effects = attr.ib(validator=Node.not_none)

@attr.s(slots=True)
class EffectsResolution(System):
    def make_node(self, obj):
        try:
            return EffectsNode(obj.id, obj.effects)
        except ValueError as e:
            return None

    def _handle(self, node):
        ent = self.real(node)
        for eff, end in list(node.effects.active.items()):
            end -= 1
            if end <= 0 or (eff.trigger == eff.Trigger.auto
                            and not eff.applicable(node)):
                eff.undo(ent)
                del node.effects.active[eff]
            else:
                eff.do(ent)

        for eff in _auto_effects:
            if eff.applicable(node) and eff not in node.effects.active:
                node.effects.pending.push(eff)

        while node.effects.pending:
            eff = node.effects.pending.pop()
            if eff.do(ent):
                if eff.end > 0:
                    node.effects.active[eff] = eff.end
                else:
                    eff.undo(ent)
