# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import functools
import logging
import copy
import attr

import abyssrl.level as level
import abyssrl.api as api
import abyssrl.version as version
import abyssrl.signals as signals

from abyssrl.ecs.entity import Entity
from abyssrl.ecs.system import System, Node
from abyssrl.ecs.components import Position, Icon
from abyssrl.level.defs import LevelType
from abyssrl.locale import _
from abyssrl.utils import cast


log = logging.getLogger('%s.%s' % (version.__appname__, __name__))

@attr.s(slots=True)
class PlayerNode(Node):
    pos = attr.ib(validator=Node.not_none)
    stats = attr.ib(validator=Node.not_none)
    pc = attr.ib(validator=Node.not_none)


@attr.s
class Player(System):
    '''This system is used to keep track of player and notify other systems
    about any needed changes.'''
    fov = attr.ib(None, repr=False)

    _lastpos = attr.ib(None, init=False, repr=False)

    fov_changed = attr.ib(attr.Factory(
        functools.partial(signals.signal, 'fov-changed')))
    player_moved = attr.ib(attr.Factory(
        functools.partial(signals.signal, 'player-moved')))

    def update(self):
        if not self.pc:
            self._reset()
            return

        if self.pc.stats.hp <= 0:
            log.info(_('[color=red]You died!'))
            self.ctl.stop()

        player_moved = (self.pc.pos != self._lastpos)
        self._lastpos = self.pc.pos

        if player_moved:
            self.player_moved.emit()

        if player_moved or self._recompute_fov:
            self._recompute_fov = False
            self._lastpos = self.pc.pos
            self.ctl.world.recompute_fov(self.pc)
            self.fov_changed.emit()

        # bookkeeping etc.
        super().update()

    def make_node(self, obj):
        try:
            return PlayerNode(obj.id, obj.pos, obj.stats, obj.pc)
        except ValueError:
            return None

    @property
    def pc(self):
        if self.nodes:
            return self.real(self.nodes[0])

    def recompute_fov(self):
        self._recompute_fov = True

    def world_reset(self):
        self._reset()

    def _reset(self):
        self._lastpos = None
        self.fov = None
        self._recompute_fov = False


@attr.s
class LevelAdmin(System):
    factory = attr.ib(attr.Factory(level.Factory))

    _levels = attr.ib(attr.Factory(dict), init=False)
    _depth = attr.ib(None, init=False)
    _pc = attr.ib(None, init=False)
    _new_level = attr.ib(False, init=False)

    def request_level_change(self, pc, depth=None):
        self._depth = depth
        self._pc = pc
        self._new_level = True

    def reset_request(self):
        self._depth = None
        self._pc = None
        self._new_level = False

    def update(self):
        '''Changes level to the one given by a depth. If there's no such level
        previously created, a new level will be created.'''

        if not self._new_level:
            return

        self._new_level = False

        if self._depth in self._levels:
            pass  # TODO; requires save-load system

        # TODO: check here if previous level has been loaded and save it to _levels

        self.ctl.clear()
        world = self.ctl.world

        l = self.factory(self._depth)
        world.new_map(l.width, l.height, l.properties)

        # threat level is increased every N levels or whenever level is
        # dangerous.
        threat_level = int(l.properties.depth / 2) + l.properties.danger

        for tile in l.tiles:
            x, y = tile.x, tile.y

            try:
                templ = l.objects[tile.otid]
            except KeyError:
                templ = self.factory.bases[tile.otid]

            type_ = templ['type']

            if type_ == 'terrain':
                world.at(x,y).terrain = cast(Icon)(templ['icon'])
            else:
                if templ['otid'] == 'player':
                    # special case: treat "player" tiles as player spawn points
                    obj = self._pc
                    api.destroy(self._pc)
                else:
                    # deepcopy: template might contain created objects, which
                    # should be local per-entity
                    obj = Entity.FromJson(copy.deepcopy(templ))
                    api.increase_threat(obj, threat_level)

                obj.pos = (x,y)
                self.ctl.add_object(obj)

            if l.properties.discovered:
                world.at(x,y).seen = True

        world.recompute_fov(self._pc)
        self.reset_request()
