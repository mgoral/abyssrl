# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

"""RGB to text"""

def shade(color, factor=0.5):
    if not color:
        return
    r, g, b = color
    return (round(r * factor), round(g * factor), round(b * factor))

def greyscale(color):
    if not color:
        return
    r, g, b = color
    avg = round((r + g + b) / 3)  # average all three colors
    return (avg, avg, avg)

def name(color):
    return '%s,%s,%s' % color


white = (255, 255, 255)
black = (0, 0, 0)

valhalla = (34, 32, 52)  # deep, deep purple
loulou = (69, 40, 60)  # violet
oiled_cedar = (102, 57, 49)  # dark brown
rope = (143, 86, 59)  # brown
tahiti_gold = (223, 113, 38)  # orange
twine = (217, 160, 102)  # light brown
pancho = (238, 195, 154)  # pink
golden_fizz = (251, 242, 54)  # yellow
atlantis = (153, 229, 80)  # light green
christi = (106, 190, 48)  # green
elf_green = (55, 148, 110)  # blueish-green (I'm not good at color names)
dell = (75, 105, 47)  # dark green
verdigris = (82, 75, 36)  # dead leafes
opal = (50, 60, 57)
deep_koamaru = (63, 63, 116)  # purple
venice_blue = (48, 96, 130)  # deep (navy) blue
royal_blue = (91, 110, 225)  # blue
cornflower = (99, 155, 255)  # blue
viking = (95, 205, 228)  # sky blue
light_steel_blue = (203, 219, 252)
heather = (155, 173, 183)  # steel
topaz = (132, 126, 135)
dim_gray = (105, 106, 106)
smokey_ash = (89, 86, 82)  # ash
clairvoyant = (118, 66, 138)  # purple
brown = (172, 50, 50)  # red
mandy = (217, 87, 99)  # pink
plum = (215, 123, 186)  # rose
rain_forest = (143, 151, 74)  # khaki
stinger = (138, 111, 48)  # darker version of khaki

#
# ALIASES, because what the hell is tahiti gold?!
grey = topaz
dark_grey = smokey_ash
light_grey = heather

red = brown
light_red = mandy

orange = tahiti_gold
yellow = golden_fizz
gold = tahiti_gold
brown = rope

green = christi
light_green = atlantis
dark_green = dell

blue = cornflower
violet = deep_koamaru

def iter():
    g = globals()
    for name in g:
        val = g[name]
        if not name.startswith('_') and isinstance(val, tuple) and len(val) == 3:
            yield name, g[name]
