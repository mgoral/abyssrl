# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.


import functools

import attr
import tcod.map
import tcod.path
import numpy

from abyssrl.colors import shade
from abyssrl.ecs.components import Position, Icon
from abyssrl.utils import expect
import abyssrl.signals as signals
import abyssrl.config as config


@attr.s(slots=True)
class Cell:
    pos = attr.ib()
    needs_update = attr.ib(False, init=False)
    seen = attr.ib(attr.Factory(config.godmode), init=False)
    icon = attr.ib(None, init=False)
    shade_icon = attr.ib(None, init=False)

    _world = attr.ib(cmp=False)
    _terrain = attr.ib(None, init=False, cmp=False)
    _objects = attr.ib(attr.Factory(list), init=False, cmp=False)
    _feature = attr.ib(None, init=False, cmp=False)
    _actor = attr.ib(None, init=False, cmp=False)

    def add(self, obj):
        expect(self._terrain, 'Cannot add object to cell without terrain')

        # actor
        if obj.props and (obj.pc or obj.ai):
            expect(not self._actor, 'Actor already set')
            expect(not self._feature or not self._feature.props.obstacle,
                'Cell\'s feature is an obstacle')

            self._actor = obj

            pos = self.pos
            self._world.tcod_map.transparent[pos.y, pos.x] = self.transparent
            self._world.walk_map[pos.y][pos.x] = 127  # max int8
            self._world.astar = None

        # feature
        elif obj.props and obj.mapfeature:
            expect(not self._feature, 'Feature already set')

            self._feature = obj

            pos = self.pos
            self._world.tcod_map.transparent[pos.y, pos.x] = self.transparent
            self._world.walk_map[pos.y][pos.x] = self.move_cost
            self._world.astar = None

        # other objects
        else:
            expect(all(obj.id != o.id for o in self._objects),
                'Object with id %d already stored' % obj.id)
            self._objects.append(obj)

        self.needs_update = True

    def remove(self, id_):
        # actor
        if self._actor and self._actor.id == id_:
            self._actor = None
            pos = self.pos

            self._world.tcod_map.transparent[pos.y, pos.x] = self.transparent
            self._world.walk_map[pos.y][pos.x] = self.move_cost
            self._world.astar = None
            self.needs_update = True
            return True

        # feature
        elif self._feature and self._feature.id == id_:
            self._feature = None
            pos = self.pos

            self._world.tcod_map.transparent[pos.y, pos.x] = self.transparent
            self._world.walk_map[pos.y][pos.x] = self.move_cost
            self._world.astar = None
            self.needs_update = True
            return True

        # other objects
        else:
            for i, o in enumerate(self._objects):
                if o.id == id_:
                    del self._objects[i]
                    self.needs_update = True
                    return True
        return False

    @property
    def actor(self):
        return self._actor

    @property
    def feature(self):
        return self._feature

    @property
    def terrain(self):
        return self._terrain

    @terrain.setter
    def terrain(self, icon):
        expect(not self.feature and not self.actor and not self.objects,
            'Terrain must be changed before/after any objects')

        self._terrain = icon
        self._world.tcod_map.transparent[self.pos.y, self.pos.x] = True
        self._world.walk_map[self.pos.y][self.pos.x] = 1
        self.needs_update = True

    @property
    def objects(self):
        return self._objects

    @property
    def transparent(self):
        check = [self.feature, self.actor]
        for obj in check:
            if obj and obj.props and not obj.props.transparent:
                return False
        return True

    @property
    def move_cost(self):
        if self.actor:
            return 127 # max int8
        if self.feature and self.feature.props.obstacle:
            return 0
        if self.feature:
            return self.feature.props.move_cost
        return 1

@attr.s
class World:
    _map = attr.ib(None, repr=False, init=False)
    width = attr.ib(0, init=False)
    height = attr.ib(0, init=False)
    properties = attr.ib(None, init=False)
    tcod_map = attr.ib(None, repr=False, init=False)

    fov_map = attr.ib(None, repr=False, init=False)
    fov_id = attr.ib(0, repr=False, init=False)

    resetted = attr.ib(attr.Factory(
        functools.partial(signals.signal, 'world-reset')))

    def new_map(self, w, h, props):
        self.clear()
        self._map = [[Cell(Position(x, y), self)
                    for y in range(h)] for x in range(w)]

        self.fov_id = 0
        self.fov_map = [[0 for y in range(h)] for x in range(w)]

        self.width = w
        self.height = h
        self.properties = props

        # Remember that tcod map has rows at the first position of xy
        # coordinates (i.e. map[x,y] == tcod_map[y,x]).
        self.tcod_map = tcod.map.Map(width=self.width, height=self.height)
        self.walk_map = numpy.zeros((h, w), dtype=numpy.int8)
        self.astar = None

        self.tcod_map.transparent[:] = False
        self.resetted.emit()

    @property
    def initialized(self):
        return bool(self._map)

    def clear(self):
        if not self._map:
            return

        self._map = None
        self.width = 0
        self.height = 0
        self.tcod_map = None

    def handle_object_added(self, obj):
        if self.initialized:
            self.atpos(obj.pos).add(obj)

    def handle_object_removed(self, obj):
        if self.initialized:
            return self.atpos(obj.pos).remove(obj.id)
        return False

    def recompute_fov(self, obj):
        self.tcod_map.compute_fov(obj.pos.x, obj.pos.y,
            radius=obj.stats.view_rad)

        self.fov_id += 1

        for y, x in numpy.argwhere(self.tcod_map.fov == True):
            self.fov_map[x][y] = self.fov_id

    def visible(self, x, y):
        return self.fov_map[x][y] == self.fov_id

    def was_visible(self, x, y):
        return self.fov_map[x][y] == self.fov_id - 1

    def visible_cells(self):
        for y, x in numpy.argwhere(self.tcod_map.fov == True):
            yield self.at(x, y)

    def path(self, src, dst):
        if not self.astar:
            self.astar = tcod.path.AStar(self.walk_map)
        path = self.astar.get_path(src.x, src.y, dst.x, dst.y)
        return [Position(x, y) for x, y in path]

    def at(self, x, y):
        return self._map[x][y]

    def atpos(self, pos):
        return self._map[pos.x][pos.y]

    def valid(self, pos):
        ww = self.width
        wh = self.height
        return pos.x >= 0 and pos.y >= 0 and pos.x < ww and pos.y < wh

    def __iter__(self):
        for y in range(self.height):
            for x in range(self.width):
                yield self[x, y]
