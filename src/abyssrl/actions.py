# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import math
import logging
import attr

import abyssrl.api as api
import abyssrl.colors as colors
import abyssrl.version as version
from abyssrl.defs import ItemTarget
from abyssrl.locale import _

log = logging.getLogger('%s.%s' % (version.__appname__, __name__))


@attr.s
class ForEach:
    '''Implements performing a list of actions. Returns whether any of them was
    successful.'''
    actions = attr.ib(attr.Factory(list))
    repeat = attr.ib(0)

    def __call__(self, node):
        rets = []
        reps = []
        for action in self.actions:
            rets.append(action(node))
            reps.append(action.repeat)

        # FIXME: hack, a proper way would be to somehow propagate repeats in
        # each complex action (ForEach, Or, And, ...) to and from underlying
        # actions.
        # This causes removing action from queue directly after it's called and
        # depleted.
        if all(r == 0 for r in reps):
            self.repeat = 0

        return any(rets)


@attr.s
class Or:
    '''Implements logical OR of actions. Second action is performed immediately
    after the first one fails.'''
    first = attr.ib()
    second = attr.ib()
    repeat = attr.ib(0)

    def __call__(self, node):
        return (self.first(node) or self.second(node))


@attr.s
class And:
    '''Implements logical AND of actions. Second action is performed immediately
    after the first one ends successfully.'''
    first = attr.ib()
    second = attr.ib()
    repeat = attr.ib(0)

    def __call__(self, node):
        return (self.first(node) and self.second(node))


@attr.s
class If:
    '''Implements logical condition of actions in form of ternary operator. If
    CONDITION action (first argument) is successfully fulfilled, then TRUE
    ACTION (second argument) is performed immediately after. Otherwise FALSE
    ACTION (third argument) is performed.

    Returns success status of either TRUE or FALSE ACTION.'''
    condition = attr.ib()
    on_true = attr.ib()
    on_false = attr.ib()
    repeat = attr.ib(0)

    def __call__(self, node):
        if self.condition(node):
            return self.on_true(node)
        return self.on_false(node)


@attr.s
class Move:
    pos = attr.ib()
    repeat = attr.ib(0)

    def __call__(self, node):
        if node.stats.o2 <= 0 and api.is_pc(node):
            api.take_o2(node, int(node.stats.max_hp / 10))
            return False

        if api.move(node, self.pos, api.move_cost(node)):
            # Placing this if her makes this print appear only once, when oxygen
            # is depleted.
            if node.stats.o2 <= 0 and api.is_pc(node):
                log.info(_('[c=blue]You cannot catch your breath![/c]'))
            return True
        return False


@attr.s
class Trigger:
    pos = attr.ib()
    repeat = attr.ib(0)

    def __call__(self, node):
        return api.trigger(self.pos, node, o2=1)


@attr.s
class UseCell:
    pos = attr.ib()
    repeat = attr.ib(0)

    def __call__(self, node):
        return api.use(self.pos, node, o2=1)


@attr.s
class AttackMelee:
    target_id = attr.ib()
    repeat = attr.ib(math.inf)
    _path = attr.ib(None, init=False)

    def __call__(self, node):
        target = api.system_node(self.target_id, 'actions')
        if api.neighbours(node, target):
            ret = api.trigger(target.pos, node, o2=1)
        else:
            if not self._path or target.pos != self._path[-1]:
                self._recompute_path(node, target)

            if not self._move_on_path(node):
                # something's changed. We didn't take any action yet, so just
                # recompute the path and try tom move again
                self._recompute_path(node, target)
                return self._move_on_path(node)

    def _move_on_path(self, node):
        if not self._path:
            return False
        my_idx = self._path.index(node.pos)
        next_step = self._path[my_idx + 1]
        return api.move(node, next_step, api.move_cost(node))

    def _recompute_path(self, start, end):
        self._path = api.path(start, end.pos)
        if len(self._path) < 2:
            self._path = None


@attr.s
class MoveOnPath:
    pos = attr.ib()
    repeat = attr.ib(math.inf)
    _path = attr.ib(None, init=False)

    def __call__(self, node):
        moved = True

        if not self._move_on_path(node):
            # something's changed. We didn't take any action yet, so just
            # recompute the path and try tom move again
            self._recompute_path(node)
            if not self._move_on_path(node):
                moved = False

        # Action failed or ended
        if not moved or self._reached_destination(node):
            self.repeat = 0

        return moved

    def _move_on_path(self, node):
        if not self._path:
            return False

        my_idx = self._path.index(node.pos)
        if my_idx + 1 >= len(self._path):
            return False

        next_step = self._path[my_idx + 1]
        return api.move(node, next_step, api.move_cost(node))

    def _recompute_path(self, start):
        self._path = api.path(start, self.pos)
        if len(self._path) < 2:
            self._path = None

    def _reached_destination(self, node):
        ent = api.entity(node.id)  # entity should have the latest position
        return self._path and ent.pos == self._path[-1]


@attr.s
class Wait:
    repeat = attr.ib(0)

    def __call__(self, node):
        return True


@attr.s
class Equip:
    item = attr.ib()
    replace = attr.ib(True)

    repeat = attr.ib(0)

    def __call__(self, node):
        if api.equip(node, self.item, self.replace):
            if api.is_pc(node):
                log.info(_("You equipped [c=%(fg)s]%(name)s[/c].") %
                    dict(name=self.item.descr.fullname,
                         fg=colors.name(self.item.icon.fg)))
            return True
        return False


@attr.s
class Pick:
    '''Picks an item from a ground.'''
    item = attr.ib()
    repeat = attr.ib(0)

    def __call__(self, node):
        if not api.pick_item(self.item, node):
            return False

        if api.is_pc(node):
            if self.item.item.money:
                log.info(_('You picked up [c=%(fg)s]%(amount)d[/c] %(name)s.') %
                    dict(amount=self.item.item.money,
                         name=self.item.descr.fullname,
                         fg=colors.name(self.item.icon.fg)))
            else:
                log.info(_('You picked up %s.') % self.item.descr.fullname)
        return True


@attr.s
class AutoPick:
    '''AutoPick selected items, for player's convienience.'''
    pos = attr.ib(None)
    repeat = attr.ib(0)

    def __call__(self, node):
        picked = False
        pos = self.pos or api.entity(node.id).pos
        for item in api.items(pos):
            if item.item.autopick:
                picked = Pick(item)(node) or picked
        return picked


@attr.s
class UseItem:
    '''Uses an item - either a full item or a specific effect from it'''
    item = attr.ib()
    repeat = attr.ib(0)

    def __call__(self, node):
        if self.item.item.use_target == ItemTarget.owner:
            return api.use_item(self.item, node)

        # TODO: implement targettable items (mgl, 2018-03-25)
        assert False, 'using targettable items not implemented'
