# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import gzip
import struct
import attr

import abyssrl.resources as resources
import abyssrl.ecs.components as components

@attr.s
class XPData:
    version = attr.ib(None)
    layers = attr.ib(None)
    width = attr.ib(None)
    height = attr.ib(None)
    tiles = attr.ib(attr.Factory(list))

    def coords(self, i):
        x = int(i / self.height)
        y = int(i % self.height)
        return x, y

    def layers_at(self, x, y):
        # REXPaint data is stored in column-major order.
        return self.tiles[x * self.height + y]

    def tile_at(self, x, y, layer=0):
        return self.layers_at(x, y)[layer]


@attr.s
class Sprite:
    width = attr.ib(0)
    height = attr.ib(0)
    tiles = attr.ib()

    @tiles.default
    def _default_tiles(self):
        return [[None for y in range(self.height)] for x in range(self.width)]


def load(name):
    data = XPData()

    black = (0, 0, 0)
    transparent = (255, 0, 255)
    invis = (black, transparent)

    # fix real image size.
    maxx = -1
    maxy = -1

    layers = []

    with gzip.open(resources.filename(name)) as f:
        data.version, data.layers = struct.unpack('ii', f.read(8))
        for lno in range(data.layers):
            data.width, data.height = struct.unpack('ii', f.read(8))

            ldata = []

            for i in range(data.width * data.height):
                glyph, fgr, fgg, fgb, bgr, bgg, bgb = \
                    struct.unpack('iBBBBBB', f.read(10))

                fg = (fgr, fgg, fgb)
                bg = (bgr, bgg, bgb)

                # Don't draw default cells and invisible ones (e.g. transparent
                # misclicks, which are super hard to find and fix)
                if glyph == ord(' ') and fg in invis and bg in invis:
                    ldata.append(None)
                # Transparent cells
                elif bg == transparent:
                    ldata.append(None)
                else:
                    ldata.append(components.Icon(chr(glyph), fg, bg))

            layers.append(ldata)

    data.tiles = list(zip(*layers))
    return data


def sprite(data, img_no=0, img_w=None, img_h=None, bordered=False):
    if img_w is None:
        img_w = data.width
    if img_h is None:
        img_h = data.height

    s = Sprite(img_w, img_h)

    # This simplifies calculations significantly. For images with borders, we'll
    # account left and top borders as part of images and remove them at the end.
    if bordered:
        total_w = data.width - 1
        img_w += 1
        img_h += 1
    else:
        total_w = data.width

    def _first_icon(icon_layers):
        for icon in reversed(icon_layers):
            if icon:
                return icon

    # if texture had only one row, this would be searched x
    norm_x = img_no * img_w
    row = int(norm_x / total_w)

    startx = norm_x % total_w
    starty = row * img_h

    if bordered:
        startx += 1
        starty += 1
        img_w -= 1
        img_h -= 1


    for x in range(img_w):
        for y in range(img_h):
            s.tiles[x][y] = _first_icon(data.layers_at(startx + x, starty + y))
    return s
