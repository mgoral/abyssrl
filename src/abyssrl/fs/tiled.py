# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.


# disable annoying pytmx logs about pygame
import logging
logging.getLogger('pytmx.util_pygame').setLevel(logging.CRITICAL)


import pytmx
import attr

import abyssrl.resources as resources

from abyssrl.level.defs import Tile

@attr.s
class Layer:
    '''Objects of this class act as iterators of Tile'''
    level = attr.ib(repr=False)
    no = attr.ib()

    @property
    def data(self):
        return self.level.layers[self.no]

    def __iter__(self):
        if isinstance(self.data, pytmx.TiledTileLayer):
            yield from self._iter_tiles()
        else:
            yield from self._iter_objects()

    def _iter_tiles(self):
        for x, y, gid in self.data.iter_data():
            if gid:
                props = self.level.get_tile_properties_by_gid(gid)
                yield Tile(x, y, props['otid'])

    def _iter_objects(self):
        for obj in self.data:
            x = int(obj.x / obj.width)
            y = int(obj.y / obj.height)
            props = obj.properties
            yield Tile(x, y, props['otid'])


class MapData:
    def __init__(self, level):
        self.level = level
        self.layers = [Layer(level, i) for i in range(len(level.layers))]


def load(name):
    return MapData(pytmx.TiledMap(resources.filename(name)))
