# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.


import json
import copy
import importlib
import functools
import attr

import abyssrl.mechanics as mech
import abyssrl.resources as resources
from abyssrl.ecs.entity import Entity
from abyssrl.effect import Effect
from abyssrl.defs import ItemTarget, ItemSlots, ItemKind
from abyssrl.glyphs import tiles

def _getattr(module, name):
    base, _, symbol = name.rpartition('.')
    if base:
        fullmodule = '%s.%s' % (module, base)
    else:
        fullmodule = module

    mod = importlib.import_module(fullmodule)
    return getattr(mod, symbol)


def _object(module, name, **kwargs):
    cls = _getattr(module, name)
    return cls(**kwargs)


def _dice(s):
    return mech.Dice.S(s)


def _roll(s):
    return mech.Dice.S(s).roll()


def _enum(en, s):
    return getattr(en, s)


_annotations = {
    'py/script': functools.partial(_getattr, 'abyssrl.scripts'),
    'py/color': functools.partial(_getattr, 'abyssrl.colors'),
    'py/effect': functools.partial(_object, 'abyssrl.effect'),
    'py/glyph': functools.partial(tiles.glyphid),
    'py/item.slot': functools.partial(_enum, ItemSlots),
    'py/item.target': functools.partial(_enum, ItemTarget),
    'py/item.kind': functools.partial(_enum, ItemKind),
    'py/effect.trigger': functools.partial(_enum, Effect.Trigger),
    'py/dice': _dice,
    'py/roll': _roll,
}


def _py_annotations(dct):
    for key, fn in _annotations.items():
        val = dct.get(key)
        if val:
            del dct[key]
            return fn(val, **dct)
    return dct


def _extend_entity(dct, templates):
    basename = dct.get('base')
    if not basename:
        return dct

    base = copy.deepcopy(templates[basename]['components'])

    for name, comp in dct['components'].items():
        base_comp = base.get(name)
        if isinstance(comp, dict) and isinstance(base_comp, dict):
            base_comp.update(comp)
        else:
            base[name] = comp

    dct['components'] = base

    return dct


def _extend(dct, templates):
    type_ = dct['type']

    if type_ in ('entity', 'monster', 'pc'):
        return _extend_entity(dct, templates)
    return dct


def load(bs, bases=None):
    '''Loads objects from a given file-like stream (e.g. a file). Accepts a
    dictionary of bases which can be used by serialized objects as bases for
    further extensions.'''
    if not bases:
        bases = {}

    s = bs.decode('utf-8')
    objects = json.loads(s, object_hook=_py_annotations)

    ret = {}

    for dct in objects:
        obj = _extend(dct, bases)

        ret[dct['otid']] = obj

    return ret


def loadr(name, bases=None):
    '''Loads objects from a given resource name'''
    return load(resources.string(name), bases)
