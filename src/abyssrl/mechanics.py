# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import attr


@attr.s(slots=True)
class Dice:
    no = attr.ib(0)
    dice = attr.ib(0)
    mod = attr.ib(0)

    @classmethod
    def S(cls, dice_str):
        '''Create a dice from a string, e.g. 2d6+3'''
        no, _, dice = dice_str.partition('d')

        # dice_str == 'd6'
        if not no and dice:
            no = 1

        dice, _, mod = dice.partition('+')
        if not mod:
            mod = 0
        return cls(int(no), int(dice), int(mod))

    def roll(self, sort=False, acc=True):
        return roll(self.no, self.dice, self.mod, sort, acc)

    @property
    def maxroll(self):
        return self.dice * self.no + self.mod

    @property
    def minroll(self):
        return 1 * self.no + self.mod

    def __bool__(self):
        return bool(self.no) or bool(self.dice) or bool(self.mod)

    def __str__(self):
        return '%d-%d' % (self.minroll, self.maxroll)

    def __add__(self, other):
        if isinstance(other, int):
            return Dice(self.no, self.dice, self.mod + other)
        else:
            min_ = self.minroll + other.minroll
            max_ = self.maxroll + other.maxroll
            delta = min_ - 1  # delta from new minroll
            return Dice(1, max_ - delta, delta)

    def __radd__(self, other):
        return self.__add__(other)


def roll(no, dice, mod=0, sort=False, acc=True):
    result = [max(0, np.random.randint(1, dice)) for i in range(no)]
    if sort:
        result.sort()
    if acc:
        result = sum(result) + mod
    return result


def roll_dices(dices, sort=False, acc=True):
    result = []
    for dice in dices:
        result.extend(dice.roll(sort, acc=False))

    if sort:
        result.sort()
    if acc:
        result = sum(result)
    return result


def chance(probability=0.5):
    return np.random.choice((True, False), p=(probability, 1-probability))
