# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import xml.etree.ElementTree as ET

import abyssrl.resources as resources

# Unicode Private Use Area
PUA = 0xE000

class _NS:
    pass

class Tileset:
    def __init__(self):
        self.path = 'fonts/tileset.tsx'

        tree = ET.parse(resources.filename(self.path))
        root = tree.getroot()

        self.tilewidth = root.attrib['tilewidth']
        self.tileheight = root.attrib['tileheight']
        
        self.t = _NS()
        for tile in root.iter('tile'):
            id_ = int(tile.attrib['id'])
            prop = tile.find('./properties/property[@name="name"]')
            name = prop.attrib['value']
            if name:
                setattr(self.t, name, PUA + id_)

    def glyphid(self, name):
        return getattr(self.t, name)

tiles = Tileset()
