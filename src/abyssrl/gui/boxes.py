# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import functools
import attr

from abyssrl.gui._widget import Widget

import abyssrl.screen as screen
import abyssrl.config as config
import abyssrl.ecs.components as components
import abyssrl.signals as signals
import abyssrl.colors as colors

@attr.s(cmp=False)
class _Box(Widget):
    # change default surface: boxes will fill the whole screen as it's
    # reasonable to have them as top-level widgets.
    _surface = attr.ib(attr.Factory(screen.screen))
    _scales = attr.ib(attr.Factory(list), repr=False)
    _initial = attr.ib(attr.Factory(list), repr=False)

    def __attrs_post_init__(self):
        for widget in self._initial:
            self.add(widget)

    def add(self, widget, scale=None):
        widget.parent = self
        self._scales.append(scale)
        widget.focus_changed.connect(self.set_dirty)
        self.set_dirty()

    def remove(self, widget):
        i = self.children.index(widget)

        widget.parent = None
        del self._scales[i]

        if i >= len(self.children):
            if self.children:
                self.children[-1].focus() or self.focus_next()
        else:
            self.children[i].focus() or self.focus_next()

        widget.focus_changed.disconnect(self.set_dirty)
        self.set_dirty()

    def clear(self):
        # TODO: clear surface when removing all widgets. can be tested e.g. on a
        # character screen when switching between statistics and effects views
        # (remember to remove adding columns' separators, which are a "temporary
        # workaround")
        if self.children:
            self.set_dirty()

        self._scales.clear()
        while self.children:
            self.children[-1].focus_changed.disconnect(self.set_dirty)
            self.children[-1].parent = None

    def draw(self):
        for widget in self.children:
            widget.draw()
        self.dirty = False

    def resized(self):
        chcopy = self.children.copy()
        sccopy = self._scales.copy()

        self.children.clear()
        self._scales.clear()

        for i, widget in enumerate(chcopy):
            scale = sccopy[i]
            self.add(widget, scale)
        self.set_dirty()

    def keypress(self, key):
        if not self.children:
            return False

        # This keypress is strange. It operates on a more-less global tab_order,
        # so it's sufficent that Box will be somewhere on path from top-level to
        # currently focused widget and bam! we'll have focus changing for all
        # widgets. This will be usually the case, because Box subclasses are
        # usually a top-level widgets.
        keys = config.config()['keys']
        if keys.get(key) == 'move-s':
            return self.focus_dir('s')
        elif keys.get(key) == 'move-n':
            return self.focus_dir('n')
        elif keys.get(key) == 'move-e':
            return self.focus_dir('e')
        elif keys.get(key) == 'move-w':
            return self.focus_dir('w')

        elif keys.get(key) == 'next':
            return self.focus_next()
        elif keys.get(key) == 'previous':
            return self.focus_prev()

        return False

    def mouse_event(self, key, x, y):
        if key == 'mouse-left':
            ch = self.child_at(x, y)
            if ch:
                ch.focus()
                return True
        return False

@attr.s(cmp=False)
class VBox(_Box):
    def size_hint(self, surface):
        w, h = 0, 0
        for widget in self.children:
            w = max(w, widget.width)
            h += widget.height
        return w, h

    def add(self, widget, scale=None):
        available_space = self.available_space()
        if available_space <= 0:
            raise RuntimeError('Not enough space for a new widget')

        if scale:
            maxh = min(available_space, round(scale * self.surface.h))
        else:
            maxh = available_space

        # use the smallest of hint's height or maxh (max scaled height)
        surface = screen.Surface(
            x=self.surface.x,
            y=self.surface.y + self.height - available_space,
            w=self.width,
            h=maxh)
        hint_w, hint_h = widget.size_hint(surface)

        if hint_h < maxh:
            surface.h = hint_h
        if hint_w < self.width:
            surface.w = hint_w

        widget.surface = surface
        super().add(widget, scale)

    def available_space(self):
        total_h = sum(w.surface.h for w in self.children)
        return self.height - total_h

@attr.s(cmp=False)
class HBox(_Box):
    def size_hint(self, surface):
        w, h = 0, 0
        for widget in self.children:
            w += widget.width
            h = max(h, widget.height)
        return w, h

    def add(self, widget, scale=None):
        available_space = self.available_space()
        if available_space <= 0:
            raise RuntimeError('Not enough space for a new widget')

        if scale:
            maxw = min(available_space, round(scale * self.surface.w))
        else:
            maxw = available_space

        # use the smallest of hint's height or maxh (max scaled height)
        surface = screen.Surface(
            x=self.surface.x + self.width - available_space,
            y=self.surface.y,
            w = maxw,
            h = self.height)
        hint_w, hint_h = widget.size_hint(surface)

        if hint_w < maxw:
            surface.w = hint_w
        if hint_h < self.height:
            surface.h = hint_h

        widget.surface = surface
        super().add(widget, scale)

    def available_space(self):
        total_w = sum(w.surface.w for w in self.children)
        return self.width - total_w


@attr.s(cmp=False)
class FlowBox(_Box):
    line_height = attr.ib(1)  # TODO: support any height
    _lines = attr.ib(init=False)  # TODO: reimplement without this?

    @_lines.default
    def _default_lines(self):
        return [[]]

    def add(self, widget, *a, **kw):
        # w, h show total width of last line and total height of all lines
        w = sum(w.width for w in self._lines[-1])
        h = self.line_height * len(self._lines)

        hint_surface = screen.Surface(
            self.surface.x + w, self.surface.y + h,
            self.width - w, self.line_height)
        hint_w, hint_h = widget.size_hint(hint_surface)

        if hint_w > (self.width - w):
            if h > self.height - self.line_height:
                raise RuntimeError('Not enough space for a new widget')

            # when last line is already empty, there's no use in adding another
            # empty line
            elif self._lines[-1]:
                self._lines.append([])
                w = 0
                h += self.line_height

        width = min(hint_w, self.width - w)
        height = min(self.line_height, hint_h)

        widget.surface = screen.Surface(
            self.surface.x + w,
            self.surface.y + h - self.line_height,
            width,
            height)
        self._lines[-1].append(widget)
        super().add(widget, *a, **kw)

    def resized(self):
        self._lines = self._default_lines()
        super().resized()

    def size_hint(self, surface):
        w, h = 0, self.line_height
        line_w = 0
        for ch in self.children:
            line_left_w = surface.w - line_w
            if line_left_w < ch.width:
                h += self.line_height
                line_w = 0
            line_w += ch.width
            w = max(line_w, w)
        return w, h

@attr.s(cmp=False)
class Align(Widget):
    w = attr.ib(None, repr=False)  # managed widget
    horizontal = attr.ib('left')
    vertical = attr.ib('top')

    # Changed default surface
    _surface = attr.ib(attr.Factory(screen.screen))

    def __attrs_post_init__(self):
        self.w.parent = self
        self._set_widget_surface()
        super().__attrs_post_init__()

    def draw(self):
        return self.w.draw()

    def handle_event(self, *a, **kw):
        return self.w.handle_event(*a, **kw)

    def size_hint(self, surface):
        w, h = self.w.size_hint(surface)

        if self.horizontal == 'center':
            w = surface.w
        if self.vertical == 'center':
            h = surface.h

        return (w, h)

    def resized(self):
        self._set_widget_surface()

    def _set_widget_surface(self):
        hint_w, hint_h = self.w.size_hint(self.surface)
        x, y = self.surface.x, self.surface.y

        w = min(hint_w, self.surface.w)
        h = min(hint_h, self.surface.h)

        if self.horizontal == 'right':
            x = self.surface.x + self.surface.w - w
        elif self.horizontal == 'center':
            x = round(self.surface.x + (self.surface.w - w) / 2)

        if self.vertical == 'bottom':
            y = self.surface.y + self.surface.h - h
        elif self.vertical == 'center':
            y = round((self.surface.y + self.surface.h - h) / 2)

        x = max(self.surface.x, x)
        y = max(self.surface.y, y)
        self.w.surface = screen.Surface(x, y, w, h)

@attr.s(cmp=False)
class Fill(Widget):
    w = attr.ib(None, repr=False)  # managed widget
    direction = attr.ib('both')

    # managed widget will be filled up to these values (if set).
    maxw = attr.ib(None)
    maxh = attr.ib(None)

    # Changed default surface
    _surface = attr.ib(attr.Factory(screen.screen))

    def __attrs_post_init__(self):
        self.w.parent = self
        self._set_widget_surface()
        super().__attrs_post_init__()

    def draw(self):
        return self.w.draw()

    def handle_event(self, *a, **kw):
        return self.w.handle_event(*a, **kw)

    def size_hint(self, surface):
        maxw = self.maxw if self.maxw else surface.w
        maxh = self.maxh if self.maxh else surface.h

        if self.direction == 'both':
            return maxw, maxh
        elif self.direction == 'vertical':
            return self.w.size_hint(surface)[0], maxh
        elif self.direction == 'horizontal':
            return maxw, self.w.size_hint(surface)[1]

    def resized(self):
        self._set_widget_surface()

    def _set_widget_surface(self):
        w = self.maxw if self.maxw else self.surface.w
        h = self.maxh if self.maxh else self.surface.h
        self.w.surface = screen.Surface(self.surface.x, self.surface.y, w, h)


@attr.s(cmp=False)
class Border(Widget):
    w = attr.ib(None, repr=False)
    title = attr.ib(None)
    color = attr.ib(colors.white)
    bg = attr.ib(None)

    # Changed default surface
    _surface = attr.ib(attr.Factory(screen.screen))

    def __attrs_post_init__(self):
        self.w.parent = self
        self._set_widget_surface()
        super().__attrs_post_init__()

    def size_hint(self, surface):
        w, h = self.w.size_hint(surface)
        return w + 2, h + 2

    def handle_event(self, *a, **kw):
        return self.w.handle_event(*a, **kw)

    def draw(self):
        with screen.next_layer():
            self.w.draw()

        if not self.dirty:
            return
        self.dirty = False

        if self.bg:
            with screen.colorize(fg=self.bg.fg):
                self.surface.fill(glyph=self.bg.icon)

        with screen.next_layer():
            maxx = self.surface.w - 1
            maxy = self.surface.h - 1
            with screen.colorize(self.color):
                self.surface.put(0, 0, '┌')
                self.surface.put(0, maxy, '└')
                self.surface.put(maxx, 0, '┐')
                self.surface.put(maxx, maxy, '┘')
                self.surface.fill(1, 0, maxx - 1, 1, '─',)
                self.surface.fill(1, maxy, maxx - 1, 1, '─',)

            if self.title:
                divisor = min(10, self.width)
                x = round(self.width / divisor)
                maxw = self.surface.w - x - 1
                title = ' %s ' % self.title
                self.surface.print(x, 0, title, width=maxw, height=1)

    def resized(self):
        self.set_dirty()
        self._set_widget_surface()

    def shrink(self, align=None):
        w, h = self.size_hint(self.surface)
        s = self.surface
        self.surface = screen.Surface(s.x, s.y, w, h)

    def _set_widget_surface(self):
        x = self.surface.x + 1
        y = self.surface.y + 1
        w = self.surface.w - 2
        h = self.surface.h - 2
        self.w.surface = screen.Surface(x, y, w, h)


@attr.s(cmp=False)
class ScrollableList(_Box):
    _current = attr.ib(None)
    _start = attr.ib(-1, init=False)
    _end = attr.ib(-1, init=False)

    def draw(self):
        if not self.children or not self.dirty:
            return

        start_idx, end_idx = self._drawn_range()

        self._start = start_idx
        self._end = end_idx

        total_h = 0
        self.surface.clear()

        # Draws all-but-the-last-one widgets, which will get the remaining space
        for ch in self.children[start_idx:end_idx]:
            surf = screen.Surface(
                ch.surface.x,
                self.surface.y + total_h,
                ch.surface.w,
                ch.surface.h)

            total_h += ch.surface.h

            ch.surface = surf
            ch.draw()

        last = self.children[end_idx]

        last_surface = screen.Surface(
            self.surface.x,
            self.surface.y + total_h,
            self.surface.w,
            min(last.surface.h, self.height - total_h))
        last.surface = last_surface
        last.draw()

        self.dirty = False

    def keypress(self, key):
        focus_changed = super().keypress(key)
        if focus_changed:
            self._current = self.focused_child_index()
            self.set_dirty()
        return focus_changed

    def add(self, widget, *a, **kw):
        hint_w, hint_h = widget.size_hint(self.surface)
        surface = screen.Surface(
            x=self.surface.x,
            y=self.surface.y,
            w=self.width,
            h=hint_h)

        widget.surface = surface
        super().add(widget)

        # XXX HACK: this forces recalculating start and end indices when list
        # isn't long enough to scroll and new item should appear at the end of
        # it
        self._end = -1

        if self._current is None:
            self._current = 0

    def remove(self, widget):
        super().remove(widget)
        self._current = self.focused_child_index()
        self.set_dirty()

    def focused_child_index(self):
        for i, ch in enumerate(self.children):
            if ch.focused:
                return i
        return None

    def _children_heights(self):
        return [ch.surface.h for ch in self.children]

    def _drawn_range(self):
        if (self._current is None or
                (self._current >= self._start and self._current <= self._end)):
            return self._start, min(len(self.children) - 1, self._end)

        # start_idx and end_idx denote indices of widget which will be
        # displayed. This if-else block handles focus position when list is
        # scrolled. Focus will remain on top when scrolling up and at the bottom
        # when scrolling down
        if self._current > self._end:
            start_idx = 0
            end_idx = self._current
        elif self._current < self._start:
            start_idx = self._current
            end_idx = self._current

        heights = self._children_heights()

        # display additional widgets as long as there's still some space below
        while (end_idx < len(self.children) - 1 and
                sum(heights[start_idx:end_idx+1]) < self.height):
            end_idx += 1

        # hide top widgets as long as current list doesn't fit the surface
        while (start_idx < end_idx and
                sum(heights[start_idx:end_idx+1]) > self.height):
            start_idx += 1

        return start_idx, end_idx


@attr.s(cmp=False)
class ActionScrollableList(ScrollableList):
    '''Subclass of ScrollableList which additionally handles user actions (like
    activating a widget etc.), without needing to have specific signals/handlers
    inside widgets.

    Keep in mind that usually widgets should be focusable to perform an action
    on it.'''
    activate_key = attr.ib(None)
    activate_all_key = attr.ib(None)

    activated = attr.ib(attr.Factory(
        functools.partial(signals.signal, 'activated')))
    activated_all = attr.ib(attr.Factory(
        functools.partial(signals.signal, 'activated-all')))

    def keypress(self, key):
        keys = config.config()['keys']

        i = self.focused_child_index()
        if i is not None:
            k = keys.get(key)
            if self.activate_key and k == self.activate_key:
                self.activated.emit(self.children[i])
                return True
            elif self.activate_all_key and k == self.activate_all_key:
                self.activated_all.emit()
                return True

        return super().keypress(key)

    def mouse_event(self, key, x, y):
        if key == 'mouse-left':
            ch = self.child_at(x, y)
            if not ch:
                return
            if ch.focused:
                self.activated.emit(ch)
            else:
                ch.focus()
