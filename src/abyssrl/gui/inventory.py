# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import string
import attr

import abyssrl.screen as screen
import abyssrl.gui.dialogs as dialogs
import abyssrl.colors as colors
import abyssrl.api as api
import abyssrl.actions as actions

from abyssrl.glyphs import tiles
from abyssrl.defs import ItemSlots
from abyssrl.gui.boxes import ActionScrollableList, Border, HBox, Align
from abyssrl.gui.widgets import Label, IconView, Separator
from abyssrl.ecs.components import Icon
from abyssrl.ecs.entity import dexpand
from abyssrl.locale import _

def item_descr(item):
    effects = item.item.effects
    descrs = '\n'.join('\N{BULLET} %s' % eff.description for eff in effects)

    if item.item.weight:
        descrs += '\n\n\N{BULLET} ' + _('Weight: %d N') % item.item.weight

    return '%s\n\n[c=grey]%s[/c]' % (descrs, dexpand(item, color=False))

@attr.s
class InventoryView(dialogs.Modal):
    node = attr.ib()

    _equipment = attr.ib(init=False)
    _backpack = attr.ib(init=False)
    _layout = attr.ib(init=False)

    _weight_iv = attr.ib(init=False)

    # output
    action = attr.ib(None)
    took_turn = attr.ib(False)

    @_equipment.default
    def _default_equipment(self):
        eq = ActionScrollableList(activate_key='activate', name='equipment')
        eq.activated.connect(self._equipment_activated)
        return eq

    @_backpack.default
    def _default_backpack(self):
        bp = ActionScrollableList(activate_key='activate', name='backpack')
        bp.activated.connect(self._backpack_activated)
        return bp

    @_weight_iv.default
    def _default_weight_iv(self):
        return IconView(
            icon=Icon(tiles.t.weight, colors.white), label=Label(text='0'))

    @_layout.default
    def _default_layout(self):
        layout = HBox(surface=screen.center(0.9))

        b_left = Border(w=self._equipment, title=_('Equipment'),
            bg=Icon("\N{dark shade}", colors.shade(colors.brown)))

        b_right = Border(w=self._backpack, title=_('Backpack'),
            bg=Icon("\N{dark shade}", colors.shade(colors.brown)))

        layout.add(b_left, scale=0.4)
        layout.add(b_right, scale=0.6)

        return layout

    def __attrs_post_init__(self):
        self._update_equipment()
        self._update_backpack()
        self._equipment.focus_next()

    def _update_equipment(self):
        self._equipment.clear()

        slot_names = ItemSlots.names()

        actor = api.actor(self.node)
        inventory = actor.inventory

        for i, slot in enumerate(inventory.slots):
            self._equipment.add(Separator(v=1))

            l = Label(text=slot_names[i])
            self._equipment.add(Align(w=l, horizontal='center'))

            if slot:
                iv = IconView.Entity(slot, focusable=True)
                iv.metadata = slot
                self._equipment.add(iv)

    def _update_backpack(self):
        self._backpack.clear()

        actor = api.actor(self.node)
        inventory = actor.inventory

        self._backpack.add(Separator(v=1))

        # TODO: set text directly, without changing the background
        self._weight_iv.label = Label(text=inventory.newtons())
        self._backpack.add(self._weight_iv)

        iv = IconView(
            icon=Icon(tiles.t.money, colors.gold),
            label=Label(text=_('%d shinies') % inventory.money))
        self._backpack.add(iv)
        self._backpack.add(Separator(v=1))

        for item in inventory.backpack:
            iv = IconView.Entity(item, focusable=True)
            iv.metadata = item
            self._backpack.add(iv)

    def update(self, event):
        self._layout.handle_event(event)
        self._layout.draw()

    def _backpack_activated(self, icon_view):
        item = icon_view.metadata
        citem = item.item
        choices = []

        if citem.use_target:
            # translators: shortcut for use
            choices.append((_('u'), _('[c=blue]U[/c]se')))

        if citem.fits:
            # translators: shortcut for equip
            choices.append((_('e'), _('[c=blue]E[/c]quip')))

        # translators: shortcut for discard
        choices.append((_('d'), _('[c=blue]D[/c]iscard')))

        # translators: shortcut for cancel
        choices.append((_('c'), _('[c=blue]C[/c]ancel')))

        chbox = dialogs.ChooseBox(
            title=string.capwords(item.descr.fullname),
            text=item_descr(item),
            choices=choices,
            bg=Icon("\N{full block}", colors.shade(colors.blue)))
        chbox.run()

        if chbox.selected == 'u':
            self.action = actions.UseItem(item)
            self.took_turn = True
            self.stop()
        elif chbox.selected == 'e':
            # TODO: select item slot
            if api.equip(self.node, item, replace=False):
                self._backpack.remove(icon_view)
                self._update_equipment()
                self.took_turn = True
        elif chbox.selected == 'd':
            api.discard_item(item)
            self._backpack.remove(icon_view)

            inventory = api.actor(self.node).inventory
            # TODO: set text directly, without changing the background
            self._weight_iv.label = Label(text=inventory.newtons())


    def _equipment_activated(self, icon_view):
        item = icon_view.metadata
        citem = item.item
        choices = []

        if citem.use_target:
            # translators: shortcut for use
            choices.append((_('u'), _('[c=blue]U[/c]se')))

        # translators: shortcut for unequip
        choices.append((_('n'), _('U[c=blue]n[/c]equip')))

        # translators: shortcut for cancel
        choices.append((_('c'), _('[c=blue]C[/c]ancel')))

        chbox = dialogs.ChooseBox(
            title=string.capwords(item.descr.fullname),
            text=item_descr(item),
            choices=choices,
            bg=Icon("\N{full block}", colors.shade(colors.green)))
        chbox.run()

        if chbox.selected == 'u':
            self.action = actions.UseItem(item)
            self.took_turn = True
            self.stop()
        elif chbox.selected == 'n':
            if api.unequip(self.node, item.item.slot):
                self._equipment.remove(icon_view)
                self._backpack.add(icon_view)
                self._update_backpack()
                self._equipment.focus_next()
                self.took_turn = True
