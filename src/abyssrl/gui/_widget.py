# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import functools
import itertools
from collections import deque
import contextlib
import weakref

import attr

import abyssrl.screen as screen
import abyssrl.signals as signals

from abyssrl.utils import cast

def _true(w):
    return True

def _has_coords(w, x, y):
    return w.surface and w.surface.coords_inside(x, y)

@attr.s(cmp=False)
class Widget:
    name = attr.ib(None)
    _surface = attr.ib(None, repr=False)

    _focusable = attr.ib(False)
    focused = attr.ib(False, repr=False)

    _parent = attr.ib(None, repr=False, converter=cast(weakref.ref))
    children = attr.ib(attr.Factory(list), repr=False)

    # only top-level widget will have tab_order active
    _tab_order = attr.ib(attr.Factory(list), repr=False)

    shortcut = attr.ib(None)

    # some widgets might implement some kind of optimizations for less frequent
    # drawing. Setting a dirty flag should usually force them to redraw.
    dirty = attr.ib(True, init=False, repr=False)

    focus_changed = attr.ib(
        attr.Factory(functools.partial(signals.signal, 'focus-changed')),
        init=False, repr=False)

    def __attrs_post_init__(self):
        for ch in self.children:
            ch.parent = self

        if self.parent:
            self.parent.children.append(self)

        if self.focusable:
            self.tab_order.append(self)

        # notice focused widget that focus changes.
        if self.focused:
            self.focus()

    def draw(self):
        '''Reimplement in a subclass. This method contains drawing routines and
        should draw on a widget's surface.'''
        self.dirty = False

    def keypress(self, key):
        '''Reimplement in a subclass. This method reacts for a user input and
        returns True if it was successfully handled'''
        return False

    def mouse_event(self, key, x, y):
        '''Reimplement in a subclass. This method reacts for a user mouse input
        and returns True if it was successfully handled.'''
        return False

    def handle_event(self, ev):
        '''Passes the event to the first focused widget in a subtree with this
        widget being a root. If focused widget doesn't want to handle that
        event, tries to handle it in a branch made of focused widget parents.'''
        key = screen.event_repr(ev)

        # TODO: avoid string comparision; implement event struct
        # (mgl, 2018-04-02)
        if 'mouse' in key:
            mp = screen.mouse_pos()
            has_coords = functools.partial(_has_coords, x=mp.x, y=mp.y)
            widgets = [w for w in self.dfs(has_coords)]
            for w in reversed(widgets):
                if w.mouse_event(key, mp.x, mp.y):
                    return True
            else:
                return False

        focused = None
        handled = False
        for w in self.dfs():
            if not focused and w.focused:
                focused = w
                handled = w.keypress(key)
                if handled:
                    break
            # Unless a key is handled by a focused widget, we'll also test it
            # against any widget's shortuct (which, by default, focuses a
            # widget).
            elif w.shortcut == key:
                w.activated_shortcut()
        if handled:
            return True

        # There's no focus. In this situation the only reasonable choice is to
        # check whether the current (usually top-level) widget can handle a
        # keypress (usually set a focus)
        if not focused:
            return self.keypress(key)

        # go back and check if there's a parent which would like to handle a key
        # (maybe the parent will e.g. change focused widget?)
        parent = focused.parent
        while parent:
            if parent.keypress(key):
                return True
            parent = parent.parent

        return False

    def set_dirty(self, *a, **kw):
        '''Marks this widget as dirty. Accepts arbitrary arguments because this
        function will frequently be connected as signal's slot.'''
        self.dirty = True

    @property
    def surface(self):
        return self._surface

    @surface.setter
    def surface(self, val):
        self._surface = val
        self.resized()

    @property
    def width(self):
        return self._surface.w

    @property
    def height(self):
        return self._surface.h

    @property
    def parent(self):
        if self._parent:
            return self._parent()
        return None

    def child_at(self, x, y):
        '''Returns a direct children at given (local) coordinates.'''
        for ch in self.children:
            if ch.surface and ch.surface.coords_inside(x, y):
                return ch
        return None

    @parent.setter
    def parent(self, newparent):
        parent = self.parent
        if parent:
            try:
                parent.children.remove(self)
            except ValueError:
                pass

        # XXX: order of calls is important. Don't call _move_tab_order after new
        # parent is set, as it checks toproots all over the place!
        if newparent is None:  # orphan widget
            self._move_tab_order(self)
            self._parent = None
        else:  # adopt widget
            self._move_tab_order(newparent.toproot)
            self._parent = weakref.ref(newparent)
            newparent.children.append(self)

            # Fix focus - old top root's tab_order could have a different widget
            # focused and only one widget per tab_order is allowed to be
            # focused.
            for w in self.tab_order:
                if w.focused:
                    w.focus()
                    break


    def _move_tab_order(self, newtoproot):
        # Boxes (for example) like to re-add their own children. In that case
        # changing tab order is unnecessary.
        if newtoproot == self.toproot:
            return

        # We're moving a toproot into a subtree. Speed up things and just move
        # the whole tab order to the new one
        if self.toproot == self:
            newtoproot.tab_order.extend(self.tab_order)
            self.tab_order.clear()
            return

        # The remaining part of this function cherry-picks subtree widgets from
        # toproot's tab order and moves them to newtoproot's tab order

        subtree_tab_order = []

        # widgets are unhashable... Or are they? ;)
        widgets = set([id(w) for w in self.bfs()])
        rm_indices = []

        # move the whole subtree, keeping its relative tab order
        for i, w in enumerate(self.tab_order):
            if id(w) in widgets:
                subtree_tab_order.append(w)
                rm_indices.append(i)
        for i in reversed(rm_indices):
            del self.tab_order[i]

        # modifies directly newtoproot's tab order. If we're orphaning a widget,
        # it'd point to the oldtoproot tab_order.
        newtoproot._tab_order.extend(subtree_tab_order)

    @property
    def tab_order(self):
        if not self.parent:
            return self._tab_order
        return self.toproot.tab_order

    @property
    def toproot(self):
        '''Returns a top widget which has no parent (a tree root). Note that
        this might return the current widget.'''
        obj = self
        while obj.parent:
            obj = obj.parent
        return obj

    def size_hint(self, surface):
        '''Called primarily by a widget's parent when it's resizing to decide
        how much space a widget will be given. Returns a tuple (width, height).
        By default widget wants to fill the whole given surface.'''
        return (surface.w, surface.h)

    def bfs(self, p=_true):
        '''Traverse a tree starting from this node: breadth-first search
        variant.'''
        queue = deque([self])
        while len(queue) > 0:
            parent = queue.popleft()
            if p(parent):
                yield parent
                queue.extend(parent.children)

    def dfs(self, p=_true):
        '''Traverse a tree starting from this node: depth-first search
        variant.'''

        stack = deque([self])
        while len(stack) > 0:
            node = stack.pop()
            if p(node):
                yield node
                stack.extend(reversed(node.children))

    def find(self, name):
        '''Finds the first widget in a subtree with a given name'''
        for w in self.bfs():
            if w.name == name:
                return w

    def resized(self):
        '''Called whenever widget's surface was changed.'''
        self.set_dirty()

    def activated_shortcut(self):
        '''Called whenever widget has set up a shortcut and that shortcut is
        pressed by a user.'''
        self.focus()

    def focus(self):
        if self.focusable:
            for w in self.tab_order:
                if w.focused:
                    w.focused = False
                    w.set_dirty()
                    w.focus_changed.emit()
            self.focused = True
            self.set_dirty()  # widget might want to indicate its focus
            self.focus_changed.emit()
            return True
        return False

    @property
    def focusable(self):
        return self._focusable

    @focusable.setter
    def focusable(self, val):
        # widget is added to tab_order once for all and not removed. Widgets can
        # typically change their's focusable property and we want to preserve
        # relative tab_order.
        if val and self not in self.tab_order:
            self.tab_order.append(self)
        self._focusable = val

    def focus_next(self):
        return self._focus_nextprev(1)

    def focus_prev(self):
        return self._focus_nextprev(-1)

    def focus_dir(self, direction):
        '''Changes focus in any given direction ('n,s,e,w'). This function
        doesn't rotate focus.'''
        assert direction in 'nsew'

        if not self.tab_order:
            return False

        focused = self._focused_tab_order_index()

        if focused is None:
            return self.focus_next()

        fw = self.tab_order[focused]

        # This defines functions for sort key and additional appliable check,
        # depending on a direction in which user wants to move focus.
        #
        # The list of widgets is sorted by a tuple of:
        #    `(distance in direction (either x or y), offset from direction)`
        #
        # If `distance in direction` is negative, it means that widget is
        # located on the other side of currently focused widget and should not
        # be focused.
        # Offset is absolute value of other widget's misalignment from the
        # second, non-direction coordinate. If it's too high (when widget lays
        # entirely in a different columnt), widget won't be focused as well
        # (because we prefer widgets in the same column as focused widget).
        if direction == 'e':
            k = lambda lhs: (lhs.surface.x - fw.surface.x,
                             abs(lhs.surface.y - fw.surface.y))
            appliable = lambda _: True
        elif direction == 'w':
            k = lambda lhs: (fw.surface.x - lhs.surface.x,
                             abs(lhs.surface.y - fw.surface.y))
            appliable = lambda _: True
        elif direction == 'n':
            k = lambda lhs: (fw.surface.y - lhs.surface.y,
                             abs(lhs.surface.x - fw.surface.x))
            appliable = lambda lhs: lhs.surface.x >= fw.surface.x and \
                                    lhs.surface.x < fw.surface.x + fw.surface.w
        elif direction == 's':
            k = lambda lhs: (lhs.surface.y - fw.surface.y,
                             abs(lhs.surface.x - fw.surface.x))
            appliable = lambda lhs: lhs.surface.x >= fw.surface.x and \
                                    lhs.surface.x < fw.surface.x + fw.surface.w

        focus_order = sorted(self.tab_order, key=k)
        for w in focus_order:
            distance = k(w)[0]
            if distance > 0 and appliable(w) and w.focus():
                return True
        return False

    def _focused_tab_order_index(self):
        for i, w in enumerate(self.tab_order):
            if w.focused:
                return i
        return None

    def _focus_nextprev(self, direction=1):
        assert direction in (1, -1), 'direction must be either 1 or -1'

        if not self.tab_order:
            return False

        focused = self._focused_tab_order_index()

        if focused is None:
            # start from the first/last element, depending on direction
            next_ = 0 if direction > 0 else len(self.tab_order) - 1
        else:
            next_ = focused + direction

        # Split tab_order to max. 2 iterators, which will ultimately iterate
        # through all elements of tab_order in a chosen direction, possibly
        # cycling at either start or end of tab_order. This iterator is used
        # later in a single-pass for.
        if direction < 0:
            ch1 = range(next_, -1, -1)
            ch2 = range(len(self.tab_order) - 1, next_, -1)
        else:
            ch1 = range(next_, len(self.tab_order))
            ch2 = range(0, next_)

        self.set_dirty()
        for i in itertools.chain(ch1, ch2):
            w = self.tab_order[i]
            if w.focus():
                return True
        return False
