# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

from abyssrl.gui.boxes import FlowBox, Align
from abyssrl.gui.widgets import Button, Separator, Label

def button_list(label_conn_shortcut_tuples, align=None, sep=0):
    box = FlowBox()
    for i, req in enumerate(label_conn_shortcut_tuples):
        lbl, conn, shortcut = req

        btn = Button(label=Label(text=lbl))
        if conn:
            btn.clicked.connect(conn)
        if shortcut:
            btn.shortcut = shortcut

        box.add(btn)

        if sep and i < len(label_conn_shortcut_tuples) - 1:
            box.add(Separator(h=sep))

    if align:
        return Align(horizontal=align, w=box)
    return box

