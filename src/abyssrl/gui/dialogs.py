# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import abc
import attr

import abyssrl.screen as screen
import abyssrl.colors as colors

from abyssrl.gui.boxes import (
    ScrollableList, ActionScrollableList, Border, VBox, HBox, FlowBox)
from abyssrl.gui.widgets import Checkbox, IconView, Label, Button, Separator
from abyssrl.gui.helpers import button_list
from abyssrl.ecs.components import Icon
from abyssrl.locale import _

def _default_modal_bg():
    return Icon("\N{dark shade}", colors.shade(colors.brown))

@attr.s
class Modal(metaclass=abc.ABCMeta):
    def __attrs_post_init__(self):
        # Setting attributes specific only to the base class is a hack for attr
        # library. It allows having nice constructors inside derived dialogs.
        self._running = False

    def run(self):
        event_queue = screen.dispatch_events()
        self._running = True

        with screen.next_layer_group():
            while self._running:
                ev = next(event_queue)
                if screen.event_name(ev) in ('close', 'escape'):
                    self.stop()
                    self.clear_state()
                    break

                self.update(ev)
                screen.refresh()

            screen.clear_layer_group()
        screen.refresh()

    def stop(self):
        self._running = False

    def clear_state(self):
        '''Can be implemented by a subclass. Is called when user forcefully
        closes modal window.'''
        pass

    @abc.abstractmethod
    def update(self, event):
        pass


@attr.s
class Select(Modal):
    _input = attr.ib()
    title = attr.ib('')

    bg = attr.ib(attr.Factory(_default_modal_bg))

    _layout = attr.ib(init=False)

    # output
    selected = attr.ib(attr.Factory(list), init=False)

    @_layout.default
    def _default_layout(self):
        layout = ScrollableList()

        # initializing border here is faster because we don't have to e.g. move
        # tab_order later
        b = Border(w=layout,
            title=self.title, bg=self.bg, surface=screen.center())

        for in_ in self._input:
            chb = Checkbox(label=Label(text=in_))
            chb.metadata = in_
            chb.check_changed.connect(layout.set_dirty)
            chb.check_changed.connect(self._check_changed, widget=chb)
            layout.add(chb)

        return b

    def update(self, event):
        if len(self._input) == 0:
            self.stop()
        else:
            self._layout.handle_event(event)
            self._layout.draw()

    def clear_state(self):
        self.selected = []

    def _check_changed(self, change, widget):
        if change:
            self.selected.append(widget.metadata)
        else:
            self.selected.remove(widget.metadata)

@attr.s
class Picker(Modal):
    _input = attr.ib()
    title = attr.ib(_('Pick / Pick All'))
    bg = attr.ib(attr.Factory(_default_modal_bg))

    _layout = attr.ib(init=False)

    # output
    selected = attr.ib(attr.Factory(list), init=False)

    @_layout.default
    def _default_layout(self):
        layout = VBox()
        self.picklist = ActionScrollableList(
            activate_key='activate', activate_all_key='pick')

        # HACK: scale
        layout.add(self.picklist, scale=0.95)

        self.picklist.activated.connect(self._picked)
        self.picklist.activated_all.connect(self._picked_all)

        # initializing border here is faster because we don't have to e.g. move
        # tab_order later
        b = Border(w=layout,
            title=self.title, bg=self.bg, surface=screen.center())

        for obj in self._input:
            w = IconView.Entity(obj, focusable=True)
            w.metadata = obj
            self.picklist.add(w)

        self.picklist.focus_next()

        layout.add(button_list(
            [
                (_('Pick all'), self._picked_all, None),
                (_('Cancel'), self.stop, 'esc')
            ],
            align='center', sep=1))

        return b

    def update(self, event):
        if len(self.picklist.children) == 0:
            self.stop()
        elif len(self._input) == 1:
            self.selected = [self._input[0]]
            self.stop()
        else:
            self._layout.handle_event(event)
            self._layout.draw()

    def _picked(self, widget):
        self.selected.append(widget.metadata)
        self.picklist.remove(widget)

    def _picked_all(self):
        for w in self.picklist.children:
            self.selected.append(w.metadata)
        self.stop()

@attr.s
class ChooseBox(Modal):
    title = attr.ib(None)
    text = attr.ib(None)
    choices = attr.ib(None)
    bg = attr.ib(attr.Factory(_default_modal_bg))

    _layout = attr.ib(init=False)

    # output
    selected = attr.ib(None, init=False)

    @_layout.default
    def _default_layout(self):
        layout = VBox()
        b = Border(w=layout,
            title=self.title,
            bg=self.bg, surface=screen.center())

        if self.text:
            layout.add(Separator(v=1))
            layout.add(Label(text=self.text, wrap=True))

        buttons = FlowBox()

        for i, ch in enumerate(self.choices):
            choice, text = ch
            button = Button(
                label=Label(text='%s' % text),
                shortcut=choice)
            button.metadata = choice
            button.clicked.connect(self._chosen, choice=choice)
            buttons.add(button)
            if i < len(self.choices) - 1:
                buttons.add(Separator(h=1))

        buttons.focus_prev()

        layout.add(Separator(v=2))
        layout.add(buttons)

        b.shrink()
        return b

    def update(self, event):
        self._layout.handle_event(event)
        self._layout.draw()

    def _chosen(self, choice):
        self.selected = choice
        self.stop()
