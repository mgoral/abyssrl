# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import attr
import functools

import abyssrl.screen as screen
import abyssrl.gui.dialogs as dialogs
import abyssrl.colors as colors
import abyssrl.api as api
import abyssrl.mechanics as mech

from abyssrl.gui.boxes import (ScrollableList, Border, VBox, HBox, Align, Fill,
                               FlowBox)
from abyssrl.defs import ItemSlots
from abyssrl.gui.widgets import Label, Separator, Button
from abyssrl.ecs.components import Icon
from abyssrl.utils import capital
from abyssrl.locale import _

@attr.s
class CharacterView(dialogs.Modal):
    node = attr.ib()

    _layout = attr.ib(init=False)

    @_layout.default
    def _default_layout(self):
        layout = VBox()
        border= Border(w=layout, surface=screen.center(0.9),
                       bg=Icon("\N{dark shade}", colors.shade(colors.brown)))

        self._window = HBox()

        stats_label = _('[c=violet]C[/c]haracter')
        self.stats_btn = Button(label=Label(text=stats_label), shortcut=_('c'))
        self.stats_btn.clicked.connect(self._show_stats)

        effects_label = _('[c=violet]E[/c]ffects')
        self.effects_btn = Button(label=Label(text=effects_label),
                                  shortcut=_('e'))
        self.effects_btn.clicked.connect(self._show_effects)

        buttons = FlowBox(
            initial=[self.stats_btn, Separator(h=1), self.effects_btn])
        buttons.focus_next()

        layout.add(Label(text=self.node.descr.name, font='heading',
                         align='center'))
        layout.add(Separator(v=1))
        layout.add(buttons)
        layout.add(Separator(v=1))
        layout.add(Fill(w=self._window))

        return border

    def __attrs_post_init__(self):
        self._show_stats()

    def _show_stats(self):
        self._window.clear()

        left = ScrollableList()
        center = ScrollableList()
        right = ScrollableList()

        self._window.add(left, scale=0.33)
        self._window.add(center, scale=0.33)
        self._window.add(right, scale=0.33)

        stats = self.node.stats

        # left
        left.add(_params(_('Level'), stats.level))

        left.add(Separator(v=1))
        left.add(_h(_('Attack')))
        for dmg_type, dmg in damage_sources(self.node):
            left.add(_params(dmg_type, str(dmg)))
        # TODO: report OFF_HAND_PENALTY when player equips 2 weapons. This would
        # be best represented by stats.accuracy dynamic property, but components
        # don't know about each other...
        left.add(_params(
            _('Accuracy'),
            _s(stats.base_accu, stats.accuracy - stats.base_accu)))

        left.add(Separator(v=1))
        left.add(_h(_('Defense')))
        left.add(_params(
            _('Armor Class'), _s(stats.base_ac, stats.ac - stats.base_ac)))

        left.add(Separator(v=1))
        left.add(_h(_('Statistics')))
        left.add(_params(
            _('Strength'),
            _s(stats.base_strength, stats.strength - stats.base_strength)))
        left.add(_params(
            _('Dexterity'),
            _s(stats.base_dexterity, stats.dexterity - stats.base_dexterity)))
        left.add(_params(
            _('Mind'),
            _s(stats.base_mind, stats.mind - stats.base_mind)))

        # center
        center.add(_h(_('Resources')))
        center.add(_params(
            _('Oxygen'),
            _s(stats.base_o2, stats.max_o2 - stats.base_o2)))

        # right
        right.add(_h(_('Other')))
        right.add(_params(_('View radius'), stats.view_rad))

    def _show_effects(self):
        self._window.clear()

        list_ = ScrollableList()
        self._window.add(list_)


        effects = self.node.effects

        list_.add(_h(_('Active effects')))
        if not effects.active:
            t = _('[c=grey]None[/c]')
            list_.add(Label(text=t, align='center'))

        for eff in effects.active:
            list_.add(_params(eff.description, eff.end))

    def update(self, event):
        self._layout.handle_event(event)
        self._layout.draw()


def damage_sources(node):
    actor = api.actor(node)
    sources = []
    for slot in (ItemSlots.main_hand, ItemSlots.off_hand):
        item = actor.inventory.slots[slot]
        if not item:
            continue

        eff_dices = [eff.dmg for eff in item.item.effects
                     if eff.trigger == eff.Trigger.attacking 
                        and hasattr(eff, 'dmg')]
        att_effs = sum(eff_dices, mech.Dice())
        if att_effs:
            sources.append((capital(item.descr.name), att_effs))

    if not sources:
        sources.append((_('Natural damage'), node.stats.natural_damage.dmg))

    return sources


def _h(text):
    '''Heading'''
    return Label(text='[c=viking]%s[/c]' % text, align='center')

def _s(base, mod=0):
    '''Colorful stats display'''
    if mod == 0:
        return base
    if mod < 0:
        color = colors.name(colors.red)
    else:
        color = colors.name(colors.green)
    return '%s [c=%s](%s)[/c]' % (base + mod, color, abs(mod))


def _params(name, value):
    '''Key-value presentation'''
    box = HBox()
    box.add(Label(text='%s: ' % name, align='right'), scale=0.6)
    box.add(Label(text=str(value)), scale=0.4)
    return Fill(w=box, maxh=1)
