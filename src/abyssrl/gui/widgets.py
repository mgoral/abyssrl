# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import functools
from collections import deque

import attr

import abyssrl.screen as screen
import abyssrl.config as config
import abyssrl.colors as colors
import abyssrl.signals as signals
import abyssrl.resources as resources

from abyssrl.glyphs import tiles
from abyssrl.gui._widget import Widget
from abyssrl.utils import capital


@attr.s(cmp=False)
class ViewPort(Widget):
    input_handler = attr.ib(None, repr=False)
    icons = attr.ib(attr.Factory(list), init=False, repr=False)
    font = attr.ib('tileset', repr=False)
    offset = attr.ib(None)

    _focusable = attr.ib(True)

    def draw(self):
        with screen.font(self.font):
            with screen.layer(0):
                for x, y, icon in self.icons:
                    screen.color(icon.fg, icon.bg)
                    self.surface.put(x, y, icon.icon)
                self.icons.clear()
        screen.reset_colors()

    def keypress(self, key):
        return self.input_handler.handle_key(key)

    def mouse_event(self, key, x, y):
        with screen.font(self.font):
            lx, ly = self.surface.abs2loc(x, y)

            if self.offset:
                lx += self.offset.x
                ly += self.offset.y
            return self.input_handler.handle_mouse(key, lx, ly)

    def put_icon(self, icon, x, y):
        self.icons.append((x, y, icon))

    def highlight(self, icon, x, y):
        with screen.font(self.font):
            with screen.layer(screen.max_layer_in_group()):
                screen.color(icon.fg, icon.bg)
                self.surface.put(x, y, icon.icon)
                screen.reset_colors()

    def clear_highlights(self):
        with screen.layer(screen.max_layer_in_group()):
            self.surface.clear()

    def clear(self):
        with screen.layer(0):
            screen.color(bg=colors.black)
            self.surface.clear()


@attr.s(cmp=False)
class Console(Widget):
    maxlen = attr.ib(50, repr=False)
    minwidth = attr.ib(0)
    messages = attr.ib(attr.Factory(lambda self: deque(maxlen=self.maxlen),
                                    takes_self=True))
    _updated = attr.ib(False, init=False, repr=False)

    _focusable = attr.ib(True)

    def add_message(self, msg):
        self.messages.appendleft(msg)
        self._updated = True

    def draw(self):
        if not self._updated:
            return

        with screen.layer(1):
            self._updated = False
            self.surface.clear()
            maxidx = min(len(self.messages), self.surface.h - 1)

            y = self.height - 1
            i = 0
            while y > 0 and i < len(self.messages):
                msg = self.messages[i]
                i += 1

                meas_h = screen.measure_text(msg, width=self.minwidth)[1]
                h = min(meas_h, y)
                y -= h

                self.surface.print(0, y, msg,
                    width=self.minwidth, height=h,
                    align=screen.Align.bottom.value)

    def resized(self):
        super().resized()
        self.minwidth = self.surface.w


@attr.s(cmp=False)
class Label(Widget):
    _text = attr.ib('')
    font = attr.ib('')
    align = attr.ib('default')
    wrap = attr.ib(False)

    def draw(self):
        if self.text:
            self._print(self.text)


    def size_hint(self, surface):
        font_w, font_h = screen.font_spacing(self.font)
        if self.text and not self.align in ('center', 'right'):
            if self.wrap:
                meas = screen.measure_text(self.text, width=surface.w)
            else:
                meas = screen.measure_text(self.text)
            return meas[0], meas[1]
        return (surface.w, font_h)

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, val):
        self._clear_text(self._text)
        self._text = val

    def _print(self, text):
        self.surface.clear()
        pre = ''
        if self.font:
            pre = '[font=%s]' % self.font

        if self.focused:
            screen.color(colors.yellow)

        print_kw = {
            'align': screen.Align[self.align].value,
        }
        if self.wrap or self.align in ('center', 'right'):
            print_kw['width'] = self.surface.w

        self.surface.print(0, 0, '%s%s' % (pre, text), **print_kw)

    def _clear_text(self, text):
        if not text or not self.surface:
            return
        w, h = screen.measure_text(text, width=self.surface.w)
        self._print(' ' * w * h)


@attr.s(cmp=False)
class ProgressBar(Widget):
    maximum = attr.ib(100)
    current = attr.ib(0)
    text = attr.ib(attr.Factory(Label))

    bg = attr.ib(None, repr=False)
    fg = attr.ib(colors.blue, repr=False)

    def size_hint(self, surface):
        label_h = self.text.size_hint(surface)[1]
        return (surface.w, label_h)

    def draw(self):
        with screen.next_layer():
            self.text.draw()

        fg_width = round(max(0, self.current) / self.maximum * self.width)

        screen.color(self.fg)
        self.surface.fill(0, 0, fg_width, self.height)

        screen.color(self.bg)
        self.surface.fill(fg_width, 0, self.width - fg_width, self.height)

    def resized(self):
        self.text.surface = self.surface


@attr.s(cmp=False)
class Button(Widget):
    label = attr.ib(attr.Factory(Label))
    fg = attr.ib(colors.black, repr=False)
    bg = attr.ib(colors.light_grey, repr=False)

    _focusable = attr.ib(True)
    clicked = attr.ib(attr.Factory(
        functools.partial(signals.signal, 'clicked')),
        repr=False)

    def size_hint(self, surface):
        hint_w, hint_h = self.label.size_hint(self._label_surface(surface))
        return hint_w + 2, hint_h

    def draw(self):
        if self.focused:
            bg = colors.shade(self.bg, 0.2)
            fg = colors.white
        else:
            bg = self.bg
            fg = self.fg

        screen.color(bg)
        self.surface.fill()

        with screen.next_layer():
            screen.color(fg)
            self.label.draw()

            with screen.next_layer():
                if not self.focusable:
                    with screen.colorize(colors.black):
                        self.surface.fill(glyph="\N{medium shade}")
                else:
                    # Clear possible previous button overlay
                    self.surface.clear()

    def keypress(self, key):
        keys = config.config()['keys']
        if keys.get(key) == 'activate':
            self.clicked.emit()
            return True

    def mouse_event(self, key, x, y):
        if not self.focusable:  # TODO: implement and change to self.enabled
            return False

        if key == 'mouse-left-released':
            self.focus()
            self.clicked.emit()

    def resized(self):
        self.label.surface = self._label_surface()

    def activated_shortcut(self):
        if self.focusable:  # TODO: implement and change to self.enabled
            super().activated_shortcut()
            self.clicked.emit()

    def _label_surface(self, surface=None):
        if surface is None:
            surface = self.surface
        return screen.Surface(
            surface.x + 1, surface.y, surface.w - 2, surface.h)


@attr.s(cmp=False)
class Separator(Widget):
    h = attr.ib(0) # horizontal
    v = attr.ib(0) # vertical

    def size_hint(self, surface):
        if self.h== 'max':
            w = surface.w
        else:
            w = self.h

        if self.v== 'max':
            h = surface.h
        else:
            h = self.v
        return (w, h)


@attr.s(cmp=False)
class AsciiArt(Label):
    _sprite = attr.ib(None)
    minwidth = attr.ib(0)
    minheight = attr.ib(0)

    def __attrs_post_init__(self):
        if self._sprite:
            self.sprite = self._sprite

    @property
    def sprite(self):
        return self._sprite

    @sprite.setter
    def sprite(self, val):
        self._sprite = val
        fw, fh = screen.font_spacing('tilefont')
        self.minwidth = val.width * fw
        self.minheight = val.height * fh

    def draw(self):
        with screen.font('tilefont'):
            fw, fh = screen.font_spacing()
            for x in range(int(self.minwidth / fw)):
                for y in range(int(self.minheight / fh)):
                    icon = self.sprite.tiles[x][y]
                    if icon:
                        screen.color(icon.fg, icon.bg)
                        self.surface.put(x, y, icon.icon)

    def size_hint(self, surface):
        return (self.minwidth, self.minheight)


@attr.s(cmp=False)
class Checkbox(Widget):
    label = attr.ib(attr.Factory(Label))
    checked = attr.ib(False)

    _focusable = attr.ib(True)
    check_changed = attr.ib(attr.Factory(
        functools.partial(signals.signal, 'check-changed')))

    def size_hint(self, surface):
        hint_w, hint_h = self.label.size_hint(self._label_surface(surface))
        return hint_w + 1, hint_h

    def draw(self):
        if self.focused:
            screen.color(colors.yellow)
        elif not self.focusable:
            screen.color(colors.light_grey)
        else:
            screen.color(colors.white)

        empty = "\N{ballot box}"
        ticked = "\N{ballot box with check}"

        icon = ticked if self.checked else empty
        self.surface.put(0, 0, icon)

        self.label.draw()

    def keypress(self, key):
        keys = config.config()['keys']
        if keys.get(key) == 'activate':
            self.checked = not self.checked
            self.check_changed.emit(self.checked)
            return True

    def resized(self):
        super().resized()
        self.label.surface = self._label_surface()

    def _label_surface(self, surface=None):
        if surface is None:
            surface = self.surface
        return screen.Surface(
            surface.x + 1, surface.y, surface.w - 1, surface.h)


@attr.s(cmp=False)
class IconView(Widget):
    icon = attr.ib(None)
    label = attr.ib(attr.Factory(Label))

    @classmethod
    def Entity(cls, e, *a, **kw):
        '''Alternative constructor which creates IconView for a given entity'''
        txt = capital(e.descr.fullname)
        if e.item and e.item.stackable and e.item.amount > 1:
            txt += ' (%d)' % e.item.amount
        return cls(icon=e.icon, label=Label(text=txt), *a, **kw)

    def size_hint(self, surface):
        hint_w, hint_h = self.label.size_hint(self._label_surface(surface))
        fw = screen.font_spacing('tileset')[0]
        return hint_w + fw + 1, hint_h

    def draw(self):
        with screen.font('tileset'):
            with screen.colorize(self.icon.fg, self.icon.bg):
                self.surface.put(0, 0, self.icon.icon)

        if self.focused:
            screen.color(colors.yellow)

        self.label.draw()

    def resized(self):
        super().resized()
        self.label.surface = self._label_surface()

    def _label_surface(self, surface=None):
        if surface is None:
            surface = self.surface
        fw = screen.font_spacing('tileset')[0]
        return screen.Surface(
            surface.x + fw + 1, surface.y, surface.w - fw, surface.h)


@attr.s(cmp=False)
class InputBox(Widget):
    text = attr.ib('')

    fg = attr.ib(colors.white, repr=False)
    underline = attr.ib(colors.dark_grey, repr=False)
    highlight_focused = attr.ib(True, repr=False)

    _old_text = attr.ib('', init=False)

    _focusable = attr.ib(True)

    _cursor = attr.ib('_', init=False)
    _cpos = attr.ib(0, init=False)

    finished = attr.ib(attr.Factory(
        functools.partial(signals.signal, 'text-changed')),
        repr=False, init=False)

    def size_hint(self, surface):
        return surface.w, 1

    def draw(self):
        if not self.dirty:
            return
        self.dirty = False
        
        screen.color(self.underline)
        self.surface.fill(glyph='_')

        if self.focused and self.highlight_focused:
            self.surface.fill()

        with screen.next_layer():
            screen.color(self.fg)
            self.surface.clear()

            maxlen = self.surface.w - 1
            cpos = 0

            if self.text:
                from_ = 0
                to = maxlen
                cpos = self._cpos

                # move display window to the right
                if self._cpos > maxlen:
                    delta = self._cpos - maxlen
                    from_ += delta
                    to += delta
                    cpos -= delta

                text = self.text[from_:to]
                self.surface.print(0, 0, text)

            with screen.next_layer():
                self.surface.clear()
                if self.focused:
                    self.surface.put(max(0, cpos), 0, '_')

    def focus(self):
        self._oldtext = self.text
        return super().focus()

    def keypress(self, key):
        keys = config.config()['keys']

        # XXX: Disregard user mappings: users might accidentaly disable
        # themselves from unfocusing input box, or from typing some letters
        # (e.g. by mapping a letter to "activate").
        if key in ('enter', 'return'):
            self.finished.emit(self.text)
            self.focus_next()
            return True
        elif key == 'escape':
            self.text = self._oldtext
            self._cpos = len(self.text)
            self.focus_prev()
            return False
        elif key == 'tab':
            self.focus_next()
            return True
        elif key == 's-tab':
            self.focus_prev()
            return True
        elif key == 'right':
            self._cpos = min(len(self.text), self._cpos + 1)
            self.set_dirty()
            return True
        elif key == 'left':
            self._cpos = max(0, self._cpos - 1)
            self.set_dirty()
            return True
        elif key == 'home':
            self._cpos = 0
            self.set_dirty()
            return True
        elif key == 'end':
            self._cpos = len(self.text)
            self.set_dirty()
            return True
        elif key == 'backspace':
            self.text = self.text[:self._cpos - 1] + self.text[self._cpos:]
            self._cpos = max(0, self._cpos - 1)
            self.set_dirty()
            return True
        elif key == 'delete':
            self.text = self.text[:self._cpos] + self.text[self._cpos + 1:]
            if self._cpos > len(self.text):
                self._cpos = len(self.text)
            self.set_dirty()
            return True

        # letters with shift are reported e.g. as "s-A, s-B, ..."
        if key.startswith('s-'):
            key = key[2:]
        if key.startswith('keypad'):
            key = key[6:]

        # TODO: unicode double-width chars
        if len(key) != 1:
            return False

        self.set_dirty()
        self.text = self.text[:self._cpos] + key + self.text[self._cpos:]
        self._cpos += len(key)  # TODO: len of unicode double-width char
        return True

    def mouse_event(self, key, x, y):
        if key == 'mouse-left':
            lx, _ = self.surface.abs2loc(x, y)
            self._cpos = min(lx, len(self.text))
            self.set_dirty()

    def clear(self):
        self.text = ''
        self._cpos = 0
        self._oldtext = ''
