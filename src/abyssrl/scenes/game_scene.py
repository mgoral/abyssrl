# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import logging
import copy

import attr

import abyssrl.screen as screen
import abyssrl.world as world_m
import abyssrl.api as api
import abyssrl.ecs.controller as controller
import abyssrl.ecs.render as render
import abyssrl.colors as colors
import abyssrl.level as level
import abyssrl.version as version
from abyssrl.locale import _

from abyssrl.scenes import GameOver
from abyssrl.ecs.entity import Entity
from abyssrl.ecs.components import Icon
from abyssrl.utils import cast
from abyssrl.gui.boxes import VBox, HBox, Border, FlowBox
from abyssrl.gui.widgets import ProgressBar, Label, Console, ViewPort, Separator

log = logging.getLogger(version.__appname__)


@attr.s
class GameScene:
    manager = attr.ib()
    init_pc_stats = attr.ib()

    ctl = attr.ib()
    pc = attr.ib()

    console_handler = attr.ib()
    layout = attr.ib()
    _close = attr.ib(False)


    def __attrs_post_init__(self):
        api.init(self.ctl, self.ctl.world)

        self.ctl.stopped.connect(self.game_over)
        self.ctl.renderer.layout = self.layout
        api.schedule_change_level(self.pc, 0)

    @ctl.default
    def _default_ctl(self):
        world = world_m.World()
        return controller.Controller(world)

    @pc.default
    def _new_pc(self):
        templ = self.ctl.leveladmin.factory.bases['player']
        pc = Entity.FromJson(copy.deepcopy(templ))
        for name in self.init_pc_stats:
            pc.update(name, self.init_pc_stats[name])
        return pc

    @layout.default
    def _build_layout(self):
        layout = VBox('top-level-layout')
        top = HBox('top')
        bottom = HBox('bottom')

        viewport = ViewPort(
            name='viewport',
            input_handler=self.ctl.input,
            focused=True)

        console = Console(name='console')

        topbar = Label(name='topbar')

        top.add(topbar)

        bottom.add(Border(w=console, title=_('Message log')))

        layout.add(top)
        layout.add(viewport, 0.7)
        layout.add(bottom, 0.3)

        return layout

    @console_handler.default
    def _default_console_handler(self):
        h = render.ConsoleHandler(self.ctl.renderer)
        log.addHandler(h)
        return h

    def game_over(self):
        self._close = True

    def update(self, ev):
        if screen.event_name(ev) == 'close':
            self.manager.clear()
            return

        if self._close:
            self.manager.pop()
            self.manager.push(GameOver(self.manager))
            return

        if ev:
            self.layout.handle_event(ev)
        self.ctl.update()

    def start(self):
        screen.clear()

    def stop(self):
        log.removeHandler(self.console_handler)
