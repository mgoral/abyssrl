# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import attr

import abyssrl.screen as screen
import abyssrl.fs.rexpaint as rexpaint

from abyssrl.scenes import NewCharacter
from abyssrl.gui.boxes import VBox, Align, Fill
from abyssrl.gui.widgets import AsciiArt, Label, Separator, Button
from abyssrl.locale import _

@attr.s
class MainMenu:
    manager = attr.ib()
    layout = attr.ib()

    _started = attr.ib(False, init=False)

    @layout.default
    def _build_layout(self):
        fw, fh = screen.font_spacing('tileset')

        sprite = rexpaint.sprite(rexpaint.load('art/logo.xp'))

        maxw = sprite.width * fw
        layout = VBox('container', surface=screen.center(maxw=maxw))

        continue_last = Button(
            label=Label(text=_('Continue last game'), align='center'),
            focusable=False)
        continue_last.clicked.connect(self._continue)

        new_char = Button(label=Label(text=_('New Character'), align='center'))
        new_char.clicked.connect(self._new_char)

        load_game = Button(
            label=Label(text=_('Load Game'), align='center'), focusable=False)
        load_game.clicked.connect(self._load_game)

        options = Button(
            label=Label(text=_('Options'), align='center'), focusable=False)
        options.clicked.connect(self._options)

        quit = Button(label=Label(text=_('Quit'), align='center'))
        quit.clicked.connect(self._quit)

        logo = AsciiArt(sprite=sprite)

        layout.add(Align(w=logo, horizontal='center'))

        menu_maxw = 25

        layout.add(Separator(v=5))
        layout.add(Align(
            w=Fill(w=continue_last, direction='horizontal', maxw=menu_maxw),
            horizontal='center'))

        layout.add(Separator(v=1))
        layout.add(Align(
            w=Fill(w=new_char, direction='horizontal', maxw=menu_maxw),
            horizontal='center'))

        layout.add(Separator(v=1))
        layout.add(Align(
            w=Fill(w=load_game, direction='horizontal', maxw=menu_maxw),
            horizontal='center'))

        layout.add(Separator(v=1))
        layout.add(Align(
            w=Fill(w=options, direction='horizontal', maxw=menu_maxw),
            horizontal='center'))

        layout.add(Separator(v=1))
        layout.add(Align(
            w=Fill(w=quit, direction='horizontal', maxw=menu_maxw),
            horizontal='center'))

        layout.focus_next()

        return Align(w=layout, horizontal='center', vertical='center')

    def start(self):
        self._started = True
        screen.clear()

    def stop(self):
        self._started = False

    def update(self, ev):
        if not self._started:
            return

        if screen.event_name(ev) == 'close':
            return self._quit()

        if ev:
            self.layout.handle_event(ev)

        if self._started:
            self.layout.draw()
            screen.refresh()

    def _continue(self):
        pass

    def _new_char(self):
        self.manager.push(NewCharacter(self.manager))

    def _load_game(self):
        pass

    def _options(self):
        pass

    def _quit(self):
        self.manager.clear()
