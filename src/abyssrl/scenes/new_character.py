# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import time
import attr

import abyssrl.screen as screen
import abyssrl.mechanics as mech
import abyssrl.ecs.entity as entity
import abyssrl.fs.rexpaint as rexpaint
from abyssrl.gui.boxes import VBox, HBox, Align, FlowBox
from abyssrl.gui.widgets import Label, Separator, Button, AsciiArt, InputBox
from abyssrl.gui.helpers import button_list
from abyssrl.scenes import GameScene
from abyssrl.locale import _

@attr.s
class NewCharacter:
    manager = attr.ib()
    layout = attr.ib()
    _started = attr.ib(False)
    _rolled = attr.ib(attr.Factory(list))

    @layout.default
    def _build_layout(self):
        layout = VBox('container', surface=screen.center())

        self.inputbox = InputBox(focused=True)
        self.inputbox.finished.connect(self.finished)

        layout.add(Label(text='New character', font='heading', align='center'))
        layout.add(Separator(v=5))
        layout.add(Label(text=_("What's your name?")))
        layout.add(self.inputbox)

        layout.add(Separator(v=1))

        layout.add(button_list(
            [
                (_('Begin new journey'), self.finished, None),
                (_('Cancel'), self.cancelled, 'esc')
            ],
            align='center', sep=1))

        return layout

    def update(self, ev):
        if screen.event_name(ev) == 'close':
            self.manager.clear()
            return

        if ev:
            self.layout.handle_event(ev)

        if self._started:
            self.layout.draw()
            screen.refresh()

    def start(self):
        self._started = True
        screen.clear()

    def stop(self):
        self._started = False

    def finished(self, *a, **kw):
        name = self.inputbox.text
        if not name:
            return

        pc_stats = {
            'descr': {
                'name': name,
            }
        }

        self.manager.pop()
        self.manager.push(GameScene(self.manager, pc_stats))

    def cancelled(self):
        self.manager.pop()
