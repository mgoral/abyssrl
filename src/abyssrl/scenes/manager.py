# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import time
import attr

import abyssrl.screen as screen
import abyssrl.config as config
import abyssrl.colors as colors
import abyssrl.resources as resources
import abyssrl.version as version
from abyssrl.utils import GameError


@attr.s
class SceneManager:
    scenes = attr.ib(attr.Factory(list))

    def __attrs_post_init__(self):
        global sm
        sm = self

    def __enter__(self, *a, **kw):
        screen.start()

        res_w = config.get('video.width')
        res_h = config.get('video.height')

        # TODO: don't hardcode (mgl, 2018-01-24)
        font_w, font_h = 8, 16

        width = int(res_w / font_w)
        height = int(res_h / font_h)

        params = dict(
            w=width, h=height,
            fw=font_w, fh=font_h,
            title=version.__fullname__,
            fullscreen=str(config.get('video.fullscreen')).lower())

        ok = screen.change_options(
            "window.title='%(title)s'; "
            "window.size=%(w)dx%(h)d; "
            "window.cellsize=%(fw)dx%(fh)d; "
            "window.fullscreen=%(fullscreen)s;"
            "input.filter=[keyboard,mouse+];" % params)

        ok = ok and screen.change_options(
            "palette.red=%s" % colors.name(colors.red)
        )

        for name, col in colors.iter():
            ok = ok and screen.change_options(
                "palette.%s=%s" % (name, colors.name(col)))

        ok = ok and screen.configure_font(
            '', resources.filename('fonts/DejaVuSans.ttf'),
            size='10x16')

        ok = ok and screen.configure_font(
            'tileset', resources.filename('fonts/tileset.png'),
            size='16x16', spacing='2x1', offset=0xE000)

        ok = ok and screen.configure_font(
            'tilefont', resources.filename('fonts/font_16x16.png'),
            size='16x16', spacing='2x1')

        ok = ok and screen.configure_font(
            'heading', resources.filename('fonts/DejaVuSans.ttf'),
            size='24', spacing='3x2')

        if not ok:
            raise GameError('Unable to set options')

        return self

    def __exit__(self, *a, **kw):
        self.clear()
        screen.stop()

    def push(self, scene):
        screen.resume()
        if self.scenes:
            self.scenes[-1].stop()
        self.scenes.append(scene)
        self.scenes[-1].start()

    def pop(self):
        screen.resume()
        scene = self.scenes.pop()
        scene.stop()
        if self.scenes:
            self.scenes[-1].start()
        return scene

    def clear(self):
        while self.scenes:
            self.pop()

    @property
    def scene(self):
        if self.scenes:
            return self.scenes[-1]
        return None

    def default_handler(self, ev):
        pass

    def run(self):
        event_queue = screen.dispatch_events()
        while self.scenes:
            self.scenes[-1].update(next(event_queue))
