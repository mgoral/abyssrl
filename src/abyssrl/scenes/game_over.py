# Copyright (C) 2018 Michał Góral.
#
# This file is part of Abyssal Plain
#
# Abyssal Plain is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Abyssal Plain is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Abyssal Plain. If not, see <http://www.gnu.org/licenses/>.

import attr

import abyssrl.screen as screen
import abyssrl.fs.rexpaint as rexpaint

from abyssrl.gui.boxes import VBox, Align
from abyssrl.gui.widgets import Label, Separator, AsciiArt
from abyssrl.locale import _

@attr.s
class GameOver:
    manager = attr.ib()
    layout = attr.ib()

    @layout.default
    def _build_layout(self):
        sprite = rexpaint.sprite(rexpaint.load('art/game-over.xp'))
        art = AsciiArt(sprite=sprite)

        layout = VBox('container')
        layout.add(art)
        return Align(w=layout, vertical='center', horizontal='center')

    def start(self):
        pass

    def stop(self):
        pass

    def update(self, ev):
        if screen.event_name(ev) == 'close':
            self.manager.clear()
            return

        if ev and screen.event_repr(ev) in ('escape', 'enter', 'return'):
            self.manager.pop()
            return

        self.layout.draw()
        screen.refresh()
